const config= require('./config')
const tester = require('graphql-tester').tester
const fields=`
id
descripcion
codigo_subasta
vin
year
color{
    id
    descripcion
}
sucursal{
    id
    nombre
    empresa{
        id
        nombre
    }
}
status_inventario{
    id
    descripcion
}
gastos{
    id
    descripcion
    tipo_gasto{
        id
        descripcion
    }
    monto
    tasa_cambio{
        id
        valor
        moneda_predeterminada{
            id
            descripcion
        nomemclatura

        }
        moneda_alterna{
            id
            descripcion
        nomemclatura
        }

    }
}
vehiculo{
    id
    modelo{
        id
        descripcion
        marca{
            id
            descripcion
        }
    }
    tipo_combustible{
        id
        descripcion
    }
    tipo_motor{
        id
        descripcion
    }
    cilindro{
        id
        descripcion
    }
    tipo_transmision{
        id
        descripcion
    }
    tipo_vehiculo{
        id
        descripcion
    }
    tipo_train{
        id
        descripcion
    }
}

`
describe('A Inventario', function() {
    const self = this;
    beforeAll(() => {
      self.test = tester({
        url: `http://127.0.0.1:${config.APP_PORT}/${config.GQL_URL_DIR}`,
        contentType: 'application/json'
      });
    });
    it('it should get one data', done => {
      self
          .test(
              
              JSON.stringify({
                  query: `query data($id:Int!) {
                    inventario( id:$id) {
                      ${fields}
                   }
                  }
                `,
                  variables: {
                      id: 2
                  }
              })
              //,
              //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
          )
          .then(res => {
              self.inventario = res.data.inventario;
              console.log(self.inventario) // adding userid to the test suite object
              expect(res.status).toBe(200);
              expect(res.success).toBe(true);
              done();
          })
          .catch(err => {

              expect(err).toBe(null);
              done();
          });
  });

  it('it should get all data data with no param', done => {
      self
          .test(
              JSON.stringify({
                  query: `{
                      inventarios {
                          ${fields}
                          }
                         }
                     
                `
              }
                  //,
                  //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
              ))
          .then(res => {
              self.inventarios = res.data.inventarios;
              console.log(self.inventarios) // adding userid to the test suite object
              expect(res.status).toBe(200);
              expect(res.success).toBe(true);
              done();
          })
          .catch(err => {

              expect(err).toBe(null);
              done();
          });
  });

  it('it should get all data that meets the parameter criteria withDetails', done => {
      self
          .test(
              JSON.stringify({
                  query: `query data($param:String!,$withDetails:Boolean){
                    inventarios(param:$param,withDetails:$withDetails) {
                          ${fields}
                          }
                         }
                     
                `,
                  variables: {
                      param: 'honda',
                      withDetails:true
                  }
              })
              //,
              //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
          )
          .then(res => {
              self.inventarios = res.data.inventarios;
              console.log(self.inventarios) // adding userid to the test suite object
              expect(res.status).toBe(200);
              expect(res.success).toBe(true);
              done();
          })
          .catch(err => {

              expect(err).toBe(null);
              done();
          });
  });

  
  it('it should get all data that meets the parameter criteria withoutdetials', done => {
    self
        .test(
            JSON.stringify({
                query: `query data($param:String!,$withDetails:Boolean){
                    inventarios(param:$param,withDetails:$withDetails) {
                        ${fields}
                        }
                       }
                   
              `,
                variables: {
                    param: 'honda',
                    withDetails:false
                }
            })
            //,
            //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
        )
        .then(res => {
            self.inventarios = res.data.inventarios;
            console.log(self.inventarios) // adding userid to the test suite object
            expect(res.status).toBe(200);
            expect(res.success).toBe(true);
            done();
        })
        .catch(err => {

            expect(err).toBe(null);
            done();
        });
});
    
})