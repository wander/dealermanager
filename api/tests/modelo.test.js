const config = require('./config')
const tester = require('graphql-tester').tester

describe('A Modelo', function () {
    const self = this;
    beforeAll(() => {
        self.test = tester({
            url: `http://127.0.0.1:${config.APP_PORT}/${config.GQL_URL_DIR}`,
            contentType: 'application/json'
        });
    });

    it('it should get one data', done => {
        self
            .test(

                JSON.stringify({
                    query: `query data($id:Int!) {
                      modelo( id:$id) {
                    id
                   descripcion
                   marca{
                       id
                       descripcion
                   }
                     }
                    }
                  `,
                    variables: {
                        id: 7
                    }
                })
                //,
                //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
            )
            .then(res => {
                self.modelo = res.data.modelo;
                console.log(self.modelo) // adding userid to the test suite object
                expect(res.status).toBe(200);
                expect(res.success).toBe(true);
                done();
            })
            .catch(err => {

                expect(err).toBe(null);
                done();
            });
    });

    it('it should get all data data with no param', done => {
        self
            .test(
                JSON.stringify({
                    query: `{
                        modelos {
                           id
                          descripcion
                          marca{
                              id
                              descripcion
                              }
                            }
                        }`
                }
                    //,
                    //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
                ))
            .then(res => {
                self.modelos = res.data.modelos;
                console.log(self.modelos) // adding userid to the test suite object
                expect(res.status).toBe(200);
                expect(res.success).toBe(true);
                done();
            })
            .catch(err => {

                expect(err).toBe(null);
                done();
            });
    });

    it('it should get all data that meets the parameter criteria', done => {
        self
            .test(
                JSON.stringify({
                    query: `query data($param:String!){
                        modelos(param:$param) {
                           id
                          descripcion
                          marca{
                            id
                            descripcion
                        }
                            }
                           }
                       
                  `,
                    variables: {
                        param: 'crv'
                    }
                })
                //,
                //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
            )
            .then(res => {
                self.modelos = res.data.modelos;
                console.log(self.modelos) // adding userid to the test suite object
                expect(res.status).toBe(200);
                expect(res.success).toBe(true);
                done();
            })
            .catch(err => {

                expect(err).toBe(null);
                done();
            });
    });
})