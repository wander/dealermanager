const config = require('./config')
const tester = require('graphql-tester').tester
import gpl from 'graphql-tag'
const fields=`id
descripcion
modelos{
    id
    descripcion
}`
describe('A Marca', function () {
    const self = this;
    beforeAll(() => {
        self.test = tester({
            url: `http://127.0.0.1:${config.APP_PORT}/${config.GQL_URL_DIR}`,
            contentType: 'application/json'
        });
    });

    it('it should get one data', done => {
        self
            .test(
                
                JSON.stringify({
                    query: `query data($id:Int!) {
                      marca( id:$id) {
                        ${fields}
                     }
                    }
                  `,
                    variables: {
                        id: 2
                    }
                })
                //,
                //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
            )
            .then(res => {
                self.marca = res.data.marca;
                console.log(self.marca) // adding userid to the test suite object
                expect(res.status).toBe(200);
                expect(res.success).toBe(true);
                done();
            })
            .catch(err => {

                expect(err).toBe(null);
                done();
            });
    });

    it('it should get all data data with no param', done => {
        self
            .test(
                JSON.stringify({
                    query: `{
                        marcas {
                            ${fields}
                            }
                           }
                       
                  `
                }
                    //,
                    //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
                ))
            .then(res => {
                self.marcas = res.data.marcas;
                console.log(self.marcas) // adding userid to the test suite object
                expect(res.status).toBe(200);
                expect(res.success).toBe(true);
                done();
            })
            .catch(err => {

                expect(err).toBe(null);
                done();
            });
    });

    it('it should get all data that meets the parameter criteria', done => {
        self
            .test(
                JSON.stringify({
                    query: `query data($param:String!){
                        marcas(param:$param) {
                            ${fields}
                            }
                           }
                       
                  `,
                    variables: {
                        param: 'honda'
                    }
                })
                //,
                //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
            )
            .then(res => {
                self.marcas = res.data.marcas;
                console.log(self.marcas) // adding userid to the test suite object
                expect(res.status).toBe(200);
                expect(res.success).toBe(true);
                done();
            })
            .catch(err => {

                expect(err).toBe(null);
                done();
            });
    });
})