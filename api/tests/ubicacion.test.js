const config = require('./config')
const tester = require('graphql-tester').tester

describe('A ubicacion', function () {
    const self = this;
    beforeAll(() => {
        self.test = tester({
            url: `http://127.0.0.1:${config.APP_PORT}/${config.GQL_URL_DIR}`,
            contentType: 'application/json'
        });
    });

    it('it should get one data', done => {
        self
            .test(
                JSON.stringify({
                    query: `query data($id:Int!) {
                      ubicacion( id:$id) {
                    id
                   descripcion
                     }
                    }
                  `,
                    variables: {
                        id: 2
                    }
                })
                //,
                //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
            )
            .then(res => {
                self.ubicacion = res.data.ubicacion;
                console.log(self.ubicacion) // adding userid to the test suite object
                expect(res.status).toBe(200);
                expect(res.success).toBe(true);
                done();
            })
            .catch(err => {
                console.log(err)
                expect(err).toBe(null);
                done();
            });
    });

    it('it should get all data data with no param', done => {
        self
            .test(
                JSON.stringify({
                    query: `{
                        ubicaciones {
                           id
                          descripcion
                            }
                           }
                       
                  `
                        }
                //,
                //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
            ))
            .then(res => {
                self.ubicacions = res.data.ubicaciones;
                console.log(self.ubicaciones) // adding userid to the test suite object
                expect(res.status).toBe(200);
                expect(res.success).toBe(true);
                done();
            })
            .catch(err => {
                console.log(err)
                expect(err).toBe(null);
                done();
            });
    });

    it('it should get all data that meets the parameter criteria', done => {
        self
            .test(
                JSON.stringify({
                    query: `query data($param:String!){
                        ubicaciones(param:$param) {
                           id
                          descripcion
                            }
                           }
                       
                  `,
                    variables: {
                        param: 'MIAMI'
                    }
                })
                //,
                //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
            )
            .then(res => {
                self.ubicacions = res.data.ubicaciones;
                console.log(self.ubicaciones) // adding userid to the test suite object
                expect(res.status).toBe(200);
                expect(res.success).toBe(true);
                done();
            })
            .catch(err => {
                console.log(err)
                expect(err).toBe(null);

                done();
            });
    });
})