const config = require('./config')
const tester = require('graphql-tester').tester
const fields=`id
              descripcion`
describe('A Tipo Combustible', function () {
    const self = this;
    beforeAll(() => {
        self.test = tester({
            url: `http://127.0.0.1:${config.APP_PORT}/${config.GQL_URL_DIR}`,
            contentType: 'application/json'
        });
    });

    it('it should get one data', done => {
        self
            .test(
                JSON.stringify({
                    query: `query data($id:Int!) {
                      tipo_comb( id:$id) {
                         ${fields}
                     }
                    }
                  `,
                    variables: {
                        id: 2
                    }
                })
                //,
                //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
            )
            .then(res => {
                self.tipo_comb = res.data.tipo_comb;
                console.log(self.tipo_comb) // adding userid to the test suite object
                expect(res.status).toBe(200);
                expect(res.success).toBe(true);
                done();
            })
            .catch(err => {

                expect(err).toBe(null);
                done();
            });
    });

    it('it should get all data data with no param', done => {
        self
            .test(
                JSON.stringify({
                    query: `{
                        tipo_combs {
                            ${fields}
                            }
                           }
                       
                  `
                        }
                //,
                //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
            ))
            .then(res => {
                self.tipo_combs = res.data.tipo_combs;
                console.log(self.tipo_combs) // adding userid to the test suite object
                expect(res.status).toBe(200);
                expect(res.success).toBe(true);
                done();
            })
            .catch(err => {

                expect(err).toBe(null);
                done();
            });
    });

    it('it should get all data that meets the parameter criteria', done => {
        self
            .test(
                JSON.stringify({
                    query: `query data($param:String!){
                        tipo_combs(param:$param) {
                            ${fields}
                            }
                           }
                       
                  `,
                    variables: {
                        param: '4'
                    }
                })
                //,
                //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
            )
            .then(res => {
                self.tipo_combs = res.data.tipo_combs;
                console.log(self.tipo_combs) // adding userid to the test suite object
                expect(res.status).toBe(200);
                expect(res.success).toBe(true);
                done();
            })
            .catch(err => {

                expect(err).toBe(null);
                done();
            });
    });
})