const config = require('./config')
const tester = require('graphql-tester').tester
const fields = `id
              modelo{
                  id
                  descripcion
              }
              tipo_combustible @include(if:$withDetails){
                  id
                  descripcion
              }
              tipo_motor @include(if:$withDetails){
                id
                descripcion
            }
            cilindro @include(if:$withDetails){
                id
                descripcion
            }
            tipo_transmision @include(if:$withDetails){
                id
                descripcion
            }
            tipo_vehiculo @include(if:$withDetails){
                id
                descripcion
            }
            tipo_train @include(if:$withDetails){
                id
                descripcion
            }`
describe('A vehiculo', function () {
    const self = this;
    beforeAll(() => {
        self.test = tester({
            url: `http://127.0.0.1:${config.APP_PORT}/${config.GQL_URL_DIR}`,
            contentType: 'application/json'
        });
    });

    it('it should get one data', done => {
        self
            .test(
                JSON.stringify({
                    query: `query data($id:Int!) {
                      vehiculo( id:$id) {
                   ${fields}
                     }
                    }
                  `,
                    variables: {
                        id: 2
                    }
                })
                //,
                //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
            )
            .then(res => {
                self.vehiculo = res.data.vehiculo;
                console.log(self.vehiculo) // adding userid to the test suite object
                expect(res.status).toBe(200);
                expect(res.success).toBe(true);
                done();
            })
            .catch(err => {

                expect(err).toBe(null);
                done();
            });
    });

    it('it should get all data data with no param', done => {
        self
            .test(
                JSON.stringify({
                    query: `{
                        vehiculos {
                        ${fields}
                            }
                           }
                       
                  `
                }
                    //,
                    //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
                ))
            .then(res => {
                self.vehiculos = res.data.vehiculos;
                console.log(self.vehiculos) // adding userid to the test suite object
                expect(res.status).toBe(200);
                expect(res.success).toBe(true);
                done();
            })
            .catch(err => {

                expect(err).toBe(null);
                done();
            });
    });

    it('it should get all detail of vehiculos', done => {
        self
            .test(
                JSON.stringify({
                    query: `query data($param:String!,$withDetails:Boolean){
                        vehiculos(param:$param,withDetails:$withDetails) {
                            ${fields}
                            }
                           }
                       
                  `,
                    variables: {
                        param: 'honda',
                        withDetails: true
                    }
                })
                //,
                //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
            )
            .then(res => {
                self.vehiculos = res.data.vehiculos;
                console.log(self.vehiculos) // adding userid to the test suite object
                expect(res.status).toBe(200);
                expect(res.success).toBe(true);
                done();
            })
            .catch(err => {

                expect(err).toBe(null);
                done();
            });
    });

    it('it should vehicle with model information only', done => {
        self
            .test(
                JSON.stringify({
                    query: `query data($param:String!,$withDetails:Boolean){
                        vehiculos(param:$param,withDetails:$withDetails) {
                            ${fields}
                            }
                           }
                       
                  `,
                    variables: {
                        param: 'honda',
                        withDetails: false
                    }
                })
                //,
                //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
            )
            .then(res => {
                self.vehiculos = res.data.vehiculos;
                console.log(self.vehiculos) // adding userid to the test suite object
                expect(res.status).toBe(200);
                expect(res.success).toBe(true);
                done();
            })
            .catch(err => {

                expect(err).toBe(null);
                done();
            });
    });
})