const config = require('./config')
const tester = require('graphql-tester').tester
const fields=`id
              descripcion`
describe('A status', function () {
    const self = this;
    beforeAll(() => {
        self.test = tester({
            url: `http://127.0.0.1:${config.APP_PORT}/${config.GQL_URL_DIR}`,
            contentType: 'application/json'
        });
    });

    it('it should get one data', done => {
        self
            .test(
                JSON.stringify({
                    query: `query data($id:Int!) {
                      status( id:$id) {
                 ${fields}
                     }
                    }
                  `,
                    variables: {
                        id: 2
                    }
                })
                //,
                //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
            )
            .then(res => {
                self.estatus = res.data.status;
                console.log(self.estatus) // adding userid to the test suite object
                expect(res.status).toBe(200);
                expect(res.success).toBe(true);
                done();
            })
            .catch(err => {

                expect(err).toBe(null);
                done();
            });
    });

    it('it should get all data data with no param', done => {
        self
            .test(
                JSON.stringify({
                    query: `{
                        statuses {
                            ${fields}
                            }
                           }
                       
                  `
                        }
                //,
                //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
            ))
            .then(res => {
                self.statuses = res.data.statuses;
                console.log(self.statuses) // adding userid to the test suite object
                expect(res.status).toBe(200);
                expect(res.success).toBe(true);
                done();
            })
            .catch(err => {

                expect(err).toBe(null);
                done();
            });
    });

    it('it should get all data that meets the parameter criteria', done => {
        self
            .test(
                JSON.stringify({
                    query: `query data($param:String!){
                        statuses(param:$param) {
                            ${fields}
                            }
                           }
                       
                  `,
                    variables: {
                        param: '4'
                    }
                })
                //,
                //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
            )
            .then(res => {
                self.statuses = res.data.statuses;
                console.log(self.statuses) // adding userid to the test suite object
                expect(res.status).toBe(200);
                expect(res.success).toBe(true);
                done();
            })
            .catch(err => {

                expect(err).toBe(null);
                done();
            });
    });
})