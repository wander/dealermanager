const config= require('./config')
const tester = require('graphql-tester').tester
const fields=`
id
descripcion
tipo_gasto{
  id
  descripcion
}
monto
inventario{
  id
}

tasa_cambio{
  id
  valor
  moneda_predeterminada{
    id
    descripcion
    nomenclatura
  }
  moneda_alterna{
    id
    descripcion
    nomenclatura
  }
}
`
describe('A Gasto', function() {
    const self = this;
    beforeAll(() => {
      self.test = tester({
        url: `http://127.0.0.1:${config.APP_PORT}/${config.GQL_URL_DIR}`,
        contentType: 'application/json'
      });
    });
    it('it should get one data', done => {
      self
          .test(
              
              JSON.stringify({
                  query: `query data($id:Int!) {
                    gasto( id:$id) {
                      ${fields}
                   }
                  }
                `,
                  variables: {
                      id: 2
                  }
              })
              //,
              //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
          )
          .then(res => {
              self.gasto = res.data.gasto;
              console.log(self.gasto) // adding userid to the test suite object
              expect(res.status).toBe(200);
              expect(res.success).toBe(true);
              done();
          })
          .catch(err => {

              expect(err).toBe(null);
              done();
          });
  });

  it('it should get all data data with no param', done => {
      self
          .test(
              JSON.stringify({
                  query: `{
                      gastos {
                          ${fields}
                          }
                         }
                     
                `
              }
                  //,
                  //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
              ))
          .then(res => {
              self.gastos = res.data.gastos;
              console.log(self.gastos) // adding userid to the test suite object
              expect(res.status).toBe(200);
              expect(res.success).toBe(true);
              done();
          })
          .catch(err => {

              expect(err).toBe(null);
              done();
          });
  });

  it('it should get all data that meets the parameter criteria withDetails', done => {
      self
          .test(
              JSON.stringify({
                  query: `query data($param:String!,$withDetails:Boolean){
                      gastos(param:$param,withDetails:$withDetails) {
                          ${fields}
                          }
                         }
                     
                `,
                  variables: {
                      param: 'honda',
                      withDetails:true
                  }
              })
              //,
              //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
          )
          .then(res => {
              self.gastos = res.data.gastos;
              console.log(self.gastos) // adding userid to the test suite object
              expect(res.status).toBe(200);
              expect(res.success).toBe(true);
              done();
          })
          .catch(err => {

              expect(err).toBe(null);
              done();
          });
  });

  
  it('it should get all data that meets the parameter criteria withoutdetials', done => {
    self
        .test(
            JSON.stringify({
                query: `query data($param:String!,$withDetails:Boolean){
                    gastos(param:$param,withDetails:$withDetails) {
                        ${fields}
                        }
                       }
                   
              `,
                variables: {
                    param: 'honda',
                    withDetails:false
                }
            })
            //,
            //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
        )
        .then(res => {
            self.gastos = res.data.gastos;
            console.log(self.gastos) // adding userid to the test suite object
            expect(res.status).toBe(200);
            expect(res.success).toBe(true);
            done();
        })
        .catch(err => {

            expect(err).toBe(null);
            done();
        });
});
    
})