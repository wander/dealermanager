const config = require('./config')
const tester = require('graphql-tester').tester
const fields=`id
              descripcion`
describe('A status Inventario', function () {
    const self = this;
    beforeAll(() => {
        self.test = tester({
            url: `http://127.0.0.1:${config.APP_PORT}/${config.GQL_URL_DIR}`,
            contentType: 'application/json'
        });
    });

    it('it should get one data', done => {
        self
            .test(
                JSON.stringify({
                    query: `query data($id:Int!) {
                      status_inv( id:$id) {
                         ${fields}
                     }
                    }
                  `,
                    variables: {
                        id: 2
                    }
                })
                //,
                //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
            )
            .then(res => {
                self.status_inv = res.data.status_inv;
                console.log(self.status_inv) // adding userid to the test suite object
                expect(res.status).toBe(200);
                expect(res.success).toBe(true);
                done();
            })
            .catch(err => {

                expect(err).toBe(null);
                done();
            });
    });

    it('it should get all data data with no param', done => {
        self
            .test(
                JSON.stringify({
                    query: `{
                        statuses_inv {
                            ${fields}
                            }
                           }
                       
                  `
                        }
                //,
                //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
            ))
            .then(res => {
                self.statuses_inv = res.data.statuses_inv;
                console.log(self.statuses_inv) // adding userid to the test suite object
                expect(res.status).toBe(200);
                expect(res.success).toBe(true);
                done();
            })
            .catch(err => {

                expect(err).toBe(null);
                done();
            });
    });

    it('it should get all data that meets the parameter criteria', done => {
        self
            .test(
                JSON.stringify({
                    query: `query data($param:String!){
                        statuses_inv(param:$param) {
                            ${fields}
                            }
                           }
                       
                  `,
                    variables: {
                        param: '4'
                    }
                })
                //,
                //{ jar: true } // using my fork shalkam/graphql-tester to be able to add this option to the node request
            )
            .then(res => {
                self.statuses_inv = res.data.statuses_inv;
                console.log(self.statuses_inv) // adding userid to the test suite object
                expect(res.status).toBe(200);
                expect(res.success).toBe(true);
                done();
            })
            .catch(err => {

                expect(err).toBe(null);
                done();
            });
    });
})