// Update with your config settings.

module.exports = {

   development: {
    client: 'mysql',
    connection: {
        user: "root",
        password: "naty2307",
        database: "dealerdb",
        host: "127.0.0.1",
    },
    migrations:{
      directory:__dirname+'/src/knex/migrations',
      tableName: 'knex_migrations'
    },
    seeds:{
      directory:__dirname+'/src/knex/seeders'
    }
}, 

cloud: {
  client: 'mysql',
  connection: {
      user: "sbuser@sbservermariadb",
      password: "N@ty2307",
      database: "dealerdb",
      port:3306,
      host: "sbservermariadb.mariadb.database.azure.com",
      ssl:true
  },
  migrations:{
    directory:__dirname+'/src/knex/migrations',
    tableName: 'knex_migrations'
  },
  seeds:{
    directory:__dirname+'/src/knex/seeders'
  }
},
test: {
  client: 'mysql',
  connection: {
      user: "root",
      password: "naty2307",
      database: "dealerdb_test",
      host: "127.0.0.1",
  },
  migrations:{
    directory:__dirname+'/src/knex/migrations',
    tableName: 'knex_migrations'
  },
  seeds:{
    directory:__dirname+'/src/knex/seeders'
  }
},

  staging: {
    client: 'postgresql',
    connection: {
      database: 'my_db',
      user:     'username',
      password: 'password'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: 'postgresql',
    connection: {
      database: 'my_db',
      user:     'username',
      password: 'password'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }

};
