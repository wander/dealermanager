 //const { Model } = require('objection');
 const {BaseModel} = require('./baseModel')
class tasa_cambio extends BaseModel {
  static get tableName() {
    return 'tasa_cambio';
  }
  static get relationMappings() {
    return {
      moneda_predeterminada: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass:this.modelPaths+'/moneda',
        join:{
          to:'moneda.id',
          from :'tasa_cambio.default_moneda_id'
        }
      },
      moneda_alterna: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass:this.modelPaths+'/moneda',
        join:{
          to:'moneda.id',
          from :'tasa_cambio.moneda_id'
        }
      }
    }
  }
 
}
module.exports = tasa_cambio; 