 //const { Model } = require('objection');
 const {BaseModel} = require('./baseModel')
class inventario extends BaseModel {
  static get tableName() {
    return 'inventario';
  }
  static get virtualAttributes(){
    return ['total_gastos']
}
total_gastos(){
    return this.$relatedQuery('gastos')
             .sum('monto')
             .as('total_gastos')
    

}
  static get relationMappings(){
    return{
      vehiculo:{
        relation:BaseModel.BelongsToOneRelation,
        modelClass: this.modelPaths+'/vehiculo',
        join:{
          to:'vehiculo.id',
          from:'inventario.vehiculoid'
        }
      },
      status_inventario:{
        relation:BaseModel.BelongsToOneRelation,
        modelClass:this.modelPaths+'/status_inventario',
        join:{
          to:'status_inventario.id',
          from:'inventario.statusinvid'
        }
      },
      color:{
        relation:BaseModel.BelongsToOneRelation,
        modelClass:this.modelPaths+'/color',
        join:{
          to:'color.id',
          from:'inventario.colorid'
        }
      },
      gastos:{
        relation:BaseModel.HasManyRelation,
        modelClass:this.modelPaths+'/gasto',
        join:{
          to:'gasto.inventarioid',
          from:'inventario.id'
        }
      },
      sucursal:{
        relation:BaseModel.BelongsToOneRelation,
        modelClass:this.modelPaths+'/sucursal',
        join:{
          to:'sucursal.id',
          from:'inventario.sucursalid'
        }
      },
    }
}
}
module.exports = inventario; 