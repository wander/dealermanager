//TODO: AGREGAR COLOR AL INVENTARIO
 //const { Model } = require('objection');
 const {BaseModel} = require('./baseModel')
class usuario extends BaseModel {
  static get tableName() {
    return 'usuario';
  }

  static get relationMappings() {
    return {
      roles: {
        relation: BaseModel.ManyToManyRelation,
        modelClass:this.modelPaths+'/rol',
        join: {
          to: 'rol.id',
          through: {
            from: 'usuario_rol_mapping.usuarioid',
            to: 'usuario_rol_mapping.rolid'
          },
          from: 'usuario.id'
        }
      },

      sucursales: {
        relation: BaseModel.ManyToManyRelation,
        modelClass:this.modelPaths+'/sucursal',
        join: {
          to: 'sucursal.id',
          through: {
            from: 'usuario_sucursal_mapping.usuarioid',
            to: 'usuario_sucursal_mapping.sucursalid'
          },
          from: 'usuario.id'
        }
      }
    }
  }
}
module.exports = usuario; 