 //const { Model } = require('objection');
 const {BaseModel} = require('./baseModel')
class vehiculo extends BaseModel {
  static get tableName() {
    return 'vehiculo';
  }

  static get relationMappings(){
    return{
      modelo:{
        relation:BaseModel.BelongsToOneRelation,
        modelClass: this.modelPaths+'/modelo',
        join:{
          to:'modelo.id',
          from:'vehiculo.modeloid'
        }
      },
      tipo_combustible:{
        relation:BaseModel.BelongsToOneRelation,
        modelClass: this.modelPaths+'/tipo_combustible',
        join:{
          to:'tipo_combustible.id',
          from:'vehiculo.tipocombustibleid'
        }
      },
      tipo_motor:{
        relation:BaseModel.BelongsToOneRelation,
        modelClass: this.modelPaths+'/tipo_motor',
        join:{
          to:'tipo_motor.id',
          from:'vehiculo.tipomotorid'
        }
      },
      cilindro:{
        relation:BaseModel.BelongsToOneRelation,
        modelClass: this.modelPaths+'/cilindro',
        join:{
          to:'cilindro.id',
          from:'vehiculo.cilindroid'
        }
      },
      tipo_transmision:{
        relation:BaseModel.BelongsToOneRelation,
        modelClass: this.modelPaths+'/tipo_transmision',
        join:{
          to:'tipo_transmision.id',
          from:'vehiculo.tipotransmisionid'
        }
      },
      tipo_vehiculo:{
        relation:BaseModel.BelongsToOneRelation,
        modelClass: this.modelPaths+'/tipo_vehiculo',
        join:{
          to:'tipo_vehiculo.id',
          from:'vehiculo.tipovehiculoid'
        }
      },
      tipo_train:{
        relation:BaseModel.BelongsToOneRelation,
        modelClass: this.modelPaths+'/tipo_train',
        join:{
          to:'tipo_train.id',
          from:'vehiculo.tipotrainid'
        }
      },
    }
}
}
module.exports = vehiculo; 