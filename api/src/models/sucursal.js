 //const { Model } = require('objection');
 const {BaseModel} = require('./baseModel')
class sucursal extends BaseModel {
  static get tableName() {
    return 'sucursal';
  }

  static get relationMappings(){
    return{
      dealer:{
        relation:BaseModel.BelongsToOneRelation,
        modelClass: this.modelPaths+'/dealer',
        join:{
          to:'dealer.id',
          from:'sucursal.dealer_id'
        }
      },
    }
}
}
module.exports = sucursal; 