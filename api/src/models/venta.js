const { BaseModel } = require('./baseModel')

class venta extends BaseModel {
    static get tableName() {
        return 'venta'
    }
   

    static get relationMappings(){
        return{
            inventario:{
                    relation:BaseModel.HasOneRelation,
                    modelClass:`${this.modelPaths}/inventario`,
                    join:{
                        from:'inventario.id',
                        to:'venta.inventario_id'
                    }
            },
            sucursal:{
                relation:BaseModel.HasOneRelation,
                modelClass:`${this.modelPaths}/sucursal`,
                join:{
                    from:'sucursal.id',
                    to:'venta.sucursal_id'
                }
            },
            moneda:{
                relation:BaseModel.HasOneRelation,
                modelClass:`${this.modelPaths}/moneda`,
                join:{
                    from:'moneda.id',
                    to:'venta.moneda_id'
                }
            },
            usuario:{
                relation:BaseModel.HasOneRelation,
                modelClass:`${this.modelPaths}/usuario`,
                join:{
                    from:'usuario.id',
                    to:'venta.usuario_id'
                }

            },
            tasa_cambio:{
                relation:BaseModel.HasOneRelation,
                modelClass:`${this.modelPaths}/tasa_cambio`,
                join:{
                    from:'tasa_cambio.id',
                    to:'venta.tasacambio_id'
                }

            },
            moneda:{
                relation:BaseModel.HasOneRelation,
                modelClass:`${this.modelPaths}/moneda`,
                join:{
                    from:'moneda.id',
                    to:'venta.moneda_id'
                }
            }
        }
    }
}

module.exports=venta