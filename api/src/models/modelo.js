 //const { Model } = require('objection');
 const {BaseModel} = require('./baseModel')
class modelo extends BaseModel {
  static get tableName() {
    return 'modelo';
  }
  static get relationMappings(){
    return{
      marca:{
        relation:BaseModel.BelongsToOneRelation,
        modelClass: this.modelPaths+'/marca',
        join:{
          to:'marca.id',
          from:'modelo.marcaid'
        }
      },
    }
}
 
}
module.exports = modelo; 