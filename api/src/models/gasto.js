 //const { Model } = require('objection');
 const {BaseModel} = require('./baseModel')
class gasto extends BaseModel {
  static get tableName() {
    return 'gasto';
  }
  static get relationMappings(){
    return{
      tipo_gasto:{
        relation:BaseModel.BelongsToOneRelation,
        modelClass: this.modelPaths+'/tipo_gasto',
        join:{
          to:'tipo_gasto.id',
          from:'gasto.tipogastoid'
        }
      },
      inventario:{
        relation:BaseModel.BelongsToOneRelation,
        modelClass:this.modelPaths+'/inventario',
        join:{
          to:'inventario.id',
          from:'gasto.inventarioid'
        }
      },
      moneda:{
        relation:BaseModel.BelongsToOneRelation,
        modelClass:this.modelPaths+'/moneda',
        join:{
          to:'moneda.id',
          from:'gasto.monedaid'
        }
      },
      tasa_cambio:{
        relation:BaseModel.BelongsToOneRelation,
        modelClass:this.modelPaths+'/tasa_cambio',
        join:{
          to:'tasa_cambio.id',
          from:'gasto.tasacambioid'
        }
      },
      ubicacion:{
        relation:BaseModel.BelongsToOneRelation,
        modelClass:this.modelPaths+'/ubicacion',
        join:{
          to:'ubicacion.id',
          from:'gasto.ubicacionid'
        }
      }
    }
}
}
module.exports = gasto; 