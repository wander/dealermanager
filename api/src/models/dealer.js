//const { Model } = require('objection');
const { BaseModel } = require('./baseModel')
class dealer extends BaseModel {
  static get tableName() {
    return 'dealer';
  }
  static get relationMappings() {
    return {
      sucursales: {
        relation: BaseModel.HasManyRelation,
        modelClass: this.modelPaths + '/sucursal',
        join: {
          from: 'dealer.id',
          to: 'sucursal.dealer_id'
        }
      }
    }
  }
}
module.exports = dealer; 