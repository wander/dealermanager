 //const { Model } = require('objection');
 const {BaseModel} = require('./baseModel')
class marca extends BaseModel {
  static get tableName() {
    return 'marca';
  }
  static get relationMappings() {
    return {
      modelos: {
        relation: BaseModel.HasManyRelation,
        modelClass: this.modelPaths + '/modelo',
        join: {
          from: 'marca.id',
          to: 'modelo.marcaid'
        }
      }
    }
  }
 
}
module.exports = marca; 