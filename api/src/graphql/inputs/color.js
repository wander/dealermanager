import { GraphQLInputObjectType, GraphQLString, GraphQLInt } from "graphql";
export default new GraphQLInputObjectType({
  name: "colorinput",
  fields: () => ({
    id: { type: GraphQLInt },
    descripcion: { type: GraphQLString },
    class:{type:GraphQLString}
  })
});
//test