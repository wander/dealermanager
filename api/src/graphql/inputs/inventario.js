import { GraphQLInputObjectType, GraphQLString, GraphQLInt,GraphQLList, GraphQLFloat } from "graphql";
import status_inventario from './status_inventario'
import vehiculo from './vehiculo'
import sucursal from "./sucursal";
import color from "./color";
import gasto from "./gasto";
export default new GraphQLInputObjectType({
  name: "inventarioinput",
  fields: () => ({
    id: { type: GraphQLInt },
    descripcion: { type: GraphQLString },
    codigosubasta: { type: GraphQLString },
    vin: { type: GraphQLString },
    year:{type:GraphQLString},
    precio_venta:{type:GraphQLFloat,defaultValue:0},
    status_inventario: { type: status_inventario },
    vehiculo: { type: vehiculo },
    sucursal:{type:sucursal},
    color:{type:color},
    gastos:{type:new GraphQLList(gasto)},
    gastosToAdd:{type:new GraphQLList(gasto)},
    gastosToRemove:{type:new GraphQLList(gasto)}

  })
})
//test
 
 //const { Model } = require('objection');
 