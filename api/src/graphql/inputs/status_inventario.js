import { GraphQLInputObjectType, GraphQLString, GraphQLInt } from "graphql";
export default new GraphQLInputObjectType({
  name: "statusinventarioinput",
  fields: () => ({
    id: { type: GraphQLInt },
    descripcion: { type: GraphQLString },
  })
});
//test