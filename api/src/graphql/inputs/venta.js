import { GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLFloat } from "graphql";
import inventario from './inventario'
import usuario from './usuario'
import sucursal from './sucursal'
import tasa_cambio from './tasa_cambio'
import moneda from './moneda'
export default new GraphQLInputObjectType({
  name: "ventainput",
  fields: () => ({
    id: { type: GraphQLInt },
    codigofactura:{type:GraphQLString},
    cliente:{type:GraphQLString},
    cedula:{type:GraphQLString},
    monto:{type:GraphQLFloat},
    sucursal:{type:sucursal},
    inventario:{type:inventario},
    usuario:{type:usuario},
    tasa_cambio:{type:tasa_cambio},
    moneda:{type:moneda}
      
  })
});
//test