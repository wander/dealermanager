import { GraphQLInputObjectType, GraphQLInt, GraphQLFloat } from "graphql";
import Moneda from './moneda'
export default new GraphQLInputObjectType({
  name: "tasacambioinput",
  fields: () => ({
    id: { type: GraphQLInt },
    moneda_predeterminada: { type: Moneda },
    moneda_alterna:{type:Moneda},
    valor:{type:GraphQLFloat}
  })
});
//test