import { GraphQLInputObjectType, GraphQLString, GraphQLInt,GraphQLList, GraphQLBoolean } from "graphql";
export default new GraphQLInputObjectType({
  name: "rolinput",
  fields: () => ({
    id: { type: GraphQLInt },
    descripcion: { type: GraphQLString },
    removefromuser:{type:GraphQLBoolean,defaultValue:false}
  })
});
//test