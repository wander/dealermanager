import { GraphQLInputObjectType, GraphQLString, GraphQLInt,GraphQLBoolean } from "graphql";
import moneda from './moneda'
export default new GraphQLInputObjectType({
  name: "monedainput",
  fields: () => ({
    id: { type: GraphQLInt },
    descripcion: { type: GraphQLString },
    predeterminada:{type: GraphQLBoolean},
    nomenclatura:{type:GraphQLString}
   
  })
});
//test