import { GraphQLInputObjectType, GraphQLString, GraphQLInt } from "graphql";
export default new GraphQLInputObjectType({
  name: "tipogastoinput",
  fields: () => ({
    id: { type: GraphQLInt },
    descripcion: { type: GraphQLString },
  })
});
//test