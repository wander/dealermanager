import { GraphQLInputObjectType, GraphQLString, GraphQLInt,GraphQLList } from "graphql";
import modelo from './modelo'
export default new GraphQLInputObjectType({
  name: "marcainput",
  fields: () => ({
    id: { type: GraphQLInt },
    descripcion: { type: GraphQLString },
    modelo:{type:new GraphQLList(modelo)}
  })
});
//test