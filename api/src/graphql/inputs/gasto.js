import { GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLFloat } from "graphql";
import moneda from "./moneda";
import inventario from './inventario'
import tasa_cambio from './tasa_cambio'
import tipo_gasto from './tipo_gasto'
import ubicacion from './ubicacion'
var GraphQLDate= require('graphql-date')
export default new GraphQLInputObjectType({
  name: "gastoinput",
  fields: () => ({
    id: { type: GraphQLInt },
    descripcion: { type: GraphQLString },
    inventario: { type: inventario },
    moneda: { type: moneda },
    monto:{type:GraphQLFloat},
    tasa_cambio:{type:tasa_cambio},
    tipo_gasto:{type:tipo_gasto},
    ubicacion:{type:ubicacion},
    createdat:{type:GraphQLDate}

  })
})
//test
 
 //const { Model } = require('objection');
 