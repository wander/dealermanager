import { GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLList } from "graphql";
import sucursal from "./sucursal";
export default new GraphQLInputObjectType({
  name: "dealerinput",
  fields: () => ({
    id: { type: GraphQLInt },
    rnc: { type: GraphQLString },
    nombre:{type:GraphQLString},
    pathlogo:{type:GraphQLString},
    email:{type:GraphQLString},
    website:{type:GraphQLString},
    sucursales:{type: new GraphQLList(sucursal)},
  
  })
});
//test