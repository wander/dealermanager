import { GraphQLInputObjectType, GraphQLString, GraphQLInt,GraphQLList } from "graphql";
import sucursal from './sucursal'
import rol from './rol'
import { GraphQLBoolean } from "graphql";
export default new GraphQLInputObjectType({
  name: "usuarioinput",
  fields: () => ({
    id: { type: GraphQLInt },
    nombreusuario: { type: GraphQLString },
    clave: { type: GraphQLString },
    nombre: { type: GraphQLString },
    email: { type: GraphQLString },
    activo:{type:GraphQLBoolean},
    roles:{type:new GraphQLList(rol)},
    sucursales:{type: new GraphQLList(sucursal)},
    sucursalesToAdd:{type: new GraphQLList(sucursal)},
    sucursalesToRemove:{type: new GraphQLList(sucursal)},
    rolesToAdd:{type:new GraphQLList(rol)},
    rolesToRemove:{type: new GraphQLList(sucursal)},
  })
});
//test