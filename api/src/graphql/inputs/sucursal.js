import { GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLBoolean } from "graphql";
import dealer from "./dealer";
export default new GraphQLInputObjectType({
  name: "sucursalinput",
  fields: () => ({
    id: { type: GraphQLInt },
    descripcion: { type: GraphQLString },
    direccion: { type: GraphQLString },
    telefonos: { type: GraphQLString },
    dealer:{type:dealer},
    removefromuser:{type:GraphQLBoolean,defaultValue:false},
  })
});
//test