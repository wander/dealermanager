import { GraphQLInputObjectType, GraphQLString, GraphQLInt,GraphQLList } from "graphql";
import marca from './marca'
export default new GraphQLInputObjectType({
  name: "modeloinput",
  fields: () => ({
    id: { type: GraphQLInt },
    descripcion: { type: GraphQLString },
    marca:{type:marca}
  })
});
//test