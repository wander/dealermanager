/* const fs =require('fs');
const path = require('path');
const basename = path.basename(__filename); */
var requireDir = require('require-dir');

/* var models={};

models=
  fs.readdirSync(__dirname)
.filter(file => {
  return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
})
.forEach(file => {
require((path.join(__dirname, file)));
const model =(path.join(__dirname, file));
   require(model)
});
 */
import cilindro from './cilindro'
import color from './color'
import dealer from './dealer'
import gasto from './gasto'
import inventario from './inventario'
import marca from './marca'
import modelo from './modelo'
import moneda from './moneda'
import rol from './rol'
import status from './status'
import status_inventario from './status_inventario'
import sucursal from './sucursal'
import tipo_combustible from './tipo_combustible'
import tipo_gasto from './tipo_gasto'
import tipo_motor from './tipo_motor'
import tipo_train from './tipo_train'
import tipo_transmision from './tipo_transmision'
import usuario from './usuario'
import vehiculo from './vehiculo'


export default {
  cilindro,
  color,
  dealer,
  gasto,
  inventario,
  marca,
  modelo,
  moneda,
  rol,
  status,
  status_inventario,
  sucursal,
  tipo_combustible,
  tipo_gasto,
  tipo_motor,
  tipo_train,
  tipo_transmision,
  usuario,
  vehiculo

}