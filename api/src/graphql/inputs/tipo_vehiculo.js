import { GraphQLInputObjectType, GraphQLString, GraphQLInt } from "graphql";
export default new GraphQLInputObjectType({
  name: "tipovehiculoinput",
  fields: () => ({
    id: { type: GraphQLInt },
    descripcion: { type: GraphQLString },
  })
});
//test