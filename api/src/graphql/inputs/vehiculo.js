import { GraphQLInputObjectType, GraphQLString, GraphQLInt } from "graphql";
import modelo from './modelo'
import tipo_combustible from './tipo_combustible'
import tipo_motor from './tipo_motor'
import cilindro from './cilindro'
import tipo_tranmision from './tipo_transmision'
import tipo_train from './tipo_train'
import tipo_vehiculo from './tipo_vehiculo'
export default new GraphQLInputObjectType({
  name: "vehiculoinput",
  fields: () => ({
    id: { type: GraphQLInt },
    modelo:{type:modelo},
    tipo_combustible:{type:tipo_combustible},
    tipo_motor:{type:tipo_motor},
    cilindro:{type:cilindro},
    tipo_transmision:{type:tipo_tranmision},
    tipo_vehiculo:{type:tipo_vehiculo},
    tipo_train:{type:tipo_train}
      
  })
});
//test