import models from "../../../models";
import Sucursal from "../../types/sucursal";
import sucursalInput from "../../inputs/sucursal";

export default {
  type: Sucursal,
  args: {
    sucursal: {
      type: sucursalInput
    }
  },
  async resolve(source, args) {  
          const updated_sucursal = await models.sucursal.query().patchAndFetchById(args.sucursal.id,{
            dealer_id:args.sucursal.dealer.id,
            descripcion: args.sucursal.descripcion,
            direccion:args. sucursal.direccion,
            telefonos:args. sucursal.telefonos})         
            return updated_sucursal;

  }
}

