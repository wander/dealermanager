import models from "../../../models";
import usuarioInput from "../../inputs/usuario";
const bcrypt= require('bcrypt')
const jsonwebtoken = require('jsonwebtoken')
require('dotenv').config()
import usersignedin from "../../types/usersignedin";
export default {
    type: usersignedin,
    args: {
        usuario: {
            type: usuarioInput
        }
    },
    async resolve(source, args) {
        const user = await models.usuario.query().findOne('nombreusuario','=',args.usuario.nombreusuario).withGraphFetched('[roles,sucursales]')

        if (!user) throw new Error('Usuario o Clave invalida.')        

        const valid = await bcrypt.compare(args.usuario.clave, user.clave)

        if (!valid) throw new Error('Usuario o Clave invalida.')
        
        return {
            usuario:user,
            token:jsonwebtoken.sign(
                { id: user.id,nombreusuario:user.nombreusuario, email: user.email,roles:user.roles,sucursales:user.sucursales },
                process.env.JWT_SECRET,
                { expiresIn: '1d' }
            )
        }
      
    }
}


