import models from "../../../models";
import usuarioInput from "../../inputs/usuario";
import { GraphQLString } from "graphql";
const bcrypt = require('bcrypt')
const jsonwebtoken = require('jsonwebtoken')
import _ from "lodash"
require('dotenv').config()
export default {
    type: GraphQLString,
    args: {
        usuario: {
            type: usuarioInput
        }
    },
    async resolve(source, args) {



        try {

            const user = await models.usuario.transaction(async trx => {
                var created_user = await models.usuario.query(trx).insertAndFetch({
                    usuario: args.usuario.nombreusuario,
                    clave: await bcrypt.hash(args.usuario.clave, 10)
                })

                for (const rol of args.usuario.roles || []) {
                    await created_user.$relatedQuery('roles', trx).relate({
                        id: rol.id,
                        createdAt: new Date(),
                        updatedAt: new Date()
                    })
                }

                for (const sucursal of args.usuario.sucursales || []) {
                    await created_user.$relatedQuery('sucursales', trx).relate({
                        id: sucursal.id,
                        createdAt: new Date(),
                        updatedAt: new Date()
                    })
                }

                return created_user.$query(trx).withGraphFetched('[roles,sucursales]')

            })
            console.log('Great success! Transaction Succeded')
            // return json web token
            //TODO: REMOVER TOKEN NO ES NECESARIO AQUI
            return jsonwebtoken.sign(
                { id: user.id, nombreusuario: user.nombreusuario, roles: user.roles, sucursales: user.sucursales },
                process.env.JWT_SECRET,
                { expiresIn: '1d' }
            )
        } catch (error) {
            console.log(error)
            return error
        }

    }
}


