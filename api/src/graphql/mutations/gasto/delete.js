import models from "../../../models";
import gastoInput from "../../inputs/gasto";
import Gasto from "../../types/gasto";
import { GraphQLList} from "graphql";
export default {
    type: new GraphQLList(Gasto),
    args: {
        gasto: {
            type: gastoInput
        }
    },
    async resolve(source, args) {
        const gasto = args.gasto
        await models.gasto.query().deleteById(gasto.id);
        return await models.gasto.query().withGraphFetched('[tipo_gasto,inventario,moneda,tasa_cambio,ubicacion]').where('inventarioid', gasto.inventario.id)
    }
}