import models from "../../../models";
import Gasto from "../../types/gasto";
import gastoInput from "../../inputs/gasto";


export default {
    type:Gasto,
    args:{
        gasto:{
            type:gastoInput
        }
    },
    async resolve(source,args){
        const gasto=args.gasto
        const tasa_cambio=(gasto.tasa_cambio===null)? await models.tasa_cambio.query().findOne('predeterminada','=',true):
        (gasto.moneda.predeterminada===true) ? await models.tasa_cambio.query().findOne('predeterminada','=',true): gasto.tasa_cambio
        const updated_gasto= await models.gasto.query().updateAndFetchById(gasto.id,{
            
            descripcion:gasto.descripcion,
            inventarioid:gasto.inventario.id,
            monedaid:gasto.moneda.id,
            monto:gasto.monto,
            tasacambioid:tasa_cambio.id,
            tipogastoid:gasto.tipo_gasto.id,
            ubicacionid:gasto.ubicacion.id,
            updatedAt:new Date()

        });

        return updated_gasto.$query().withGraphFetched('[tipo_gasto,inventario,moneda,tasa_cambio,ubicacion]');
    }
}