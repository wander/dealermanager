import createDealer from './dealer/create'
import updateDealer from './dealer/update'

import createSucursal from './sucursal/create'
import updateSucursal from './sucursal/update'

import createVehiculo from './vehiculo/create'
import updateVehiculo from './vehiculo/update'

import updateInventario from './inventario/update'
import createInventario from './inventario/create'

import updateGasto from './gasto/update'
import createGasto from './gasto/create'
import deleteGasto from './gasto/delete'

import login from './security/login'
import signup from './security/signup'

import createUsuario from './usuario/create'
import updateUsuario from './usuario/update'
import updateUsuarioPerfil from './usuario/updateprofile'
import updateUsuarioPass from './usuario/updatepass'

import createVenta from './venta/create'
import updateVenta from './venta/update'
export default{
    createDealer,
    updateDealer,

    createSucursal,
    updateSucursal,

    createVehiculo,
    updateVehiculo,

    createInventario,
    updateInventario,

    updateGasto,
    createGasto,
    deleteGasto,

    login,
    signup,

    createUsuario,
    updateUsuario,
    updateUsuarioPerfil,
    updateUsuarioPass,

    createVenta,
    updateVenta

}