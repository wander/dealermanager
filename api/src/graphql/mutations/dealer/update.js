import models from "../../../models";
import Dealer from "../../types/dealer";
import dealerInput from "../../inputs/dealer";


export default {
  type: Dealer,
  args: {
    dealer: {
      type: dealerInput
    }
  },
  async resolve(source, args) {
    var dealer= await models.dealer.query().patchAndFetchById(args.dealer.id,{
    
      nombre: args.dealer.nombre,
      rnc: args.dealer.rnc,
      email: args.dealer.email,
      website: args.dealer.website,
      updatedAt:new Date()
    });

    return dealer.$query().withGraphFetched('[sucursales]')
  }
};
