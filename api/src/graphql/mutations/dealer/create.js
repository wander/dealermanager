import models from "../../../models";
import Dealer from "../../types/dealer";
import dealerInput from "../../inputs/dealer";

export default {
  type: Dealer,
  args: {
    dealer: {
      type: dealerInput
    }
  },
  async resolve(source, args) {
    try {


      const dealer = await models.dealer.transaction(async trx => {

        var new_dealer = await models.dealer.query(trx).insertAndFetch({
          nombre: args.dealer.nombre,
          rnc: args.dealer.rnc,
          email: args.dealer.email,
          website: args.dealer.website
        });

        for (const sucursal of args.dealer.sucursales || []) {
          const new_sucursal = await new_dealer.$relatedQuery('sucursales', trx).insert({
            descripcion: sucursal.descripcion,
            direccion: sucursal.direccion,
            telefonos: sucursal.telefonos
          })
        }

        return new_dealer.$query(trx).withGraphFetched('[sucursales]');
      })
      console.log('Great success! Transaction Succeded');
      return dealer;
    } catch (error) {
      console.log(error);
      return error
    }



  }
};
