import models from "../../../models";
import Inventario from "../../types/inventario";
import inventarioInput from "../../inputs/inventario";
import { truncateSync } from "fs";

export default {
    type: Inventario,
    args: {
        inventario: {
            type: inventarioInput
        }
    },
    //TODO HANDLE THE SUCURSAL ASIGNATION
    async resolve(source, args) {

        const p_inventario = args.inventario
        try {
            const inventario = await models.inventario.transaction(async trx => {
                var new_inventario = await models.inventario.query(trx).insertAndFetch({
                    descripcion: p_inventario.descripcion,
                    codigo_subasta: p_inventario.codigo_subasta,
                    vin: p_inventario.vin,
                    year: p_inventario.year,
                    statusinvid: p_inventario.status_inventario.id,
                    vehiculoid: p_inventario.vehiculo.id,
                    colorid: p_inventario.color.id,
                    precio_venta:p_inventario.precio_venta,
                    sucursalid: p_inventario.sucursal ? p_inventario.sucursal.id:null

                })

                for (const gasto in p_inventario.gastos || []) {
                    const new_gasto = await new_inventario.$relatedQuery('gastos', trx).insert({
                        tipogastoid: gasto.tipo_gasto.id,
                        descripcion: gasto.descripcion,
                        monto: gasto.monto,
                        tasacambioid: gasto.tasa_cambio.id,
                        monedaid: gasto.moneda.id
                    })
                }
                return new_inventario.$query(trx).withGraphFetched('[status_inventario,vehiculo,sucursal,color,gastos]')
            
            })
            console.log('Great success! Transaction Succeded')
            return inventario

        } catch (error) {
            console.log(error)
            return error
        }



    }
}
