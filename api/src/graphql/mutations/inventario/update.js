import models from "../../../models";
import Inventario from "../../types/inventario";
import inventarioInput from "../../inputs/inventario";
import { truncateSync } from "fs";

export default {
    type: Inventario,
    args: {
        inventario: {
            type: inventarioInput
        }
    },
    //TODO HANDLE THE SUCURSAL ASIGNATION
    async resolve(source, args) {

        const p_inventario = args.inventario
        try {
            const inventario = await models.inventario.transaction(async trx => {
                var updated_inventario = await models.inventario.query(trx).patchAndFetchById(p_inventario.id, {
                    descripcion: p_inventario.descripcion,
                    codigo_subasta: p_inventario.codigo_subasta,
                    vin: p_inventario.vin,
                    year: p_inventario.year,
                    statusinvid: p_inventario.status_inventario.id,
                    vehiculoid: p_inventario.vehiculo.id,
                    colorid: p_inventario.color.id,
                    precio_venta:p_inventario.precio_venta,
                    sucursalid: p_inventario.sucursal ? p_inventario.sucursal.id:null

                })
                //modificando los gastos
                for (const gasto of p_inventario.gastos || []) {
                    const updated_gasto = await updated_inventario.$relatedQuery('gastos', trx).patchAndFetchById(gasto.id, {
                        tipogastoid: gasto.tipo_gasto.id,
                        descripcion: gasto.descripcion,
                        monto: gasto.monto,
                        tasacambioid: gasto.tasa_cambio ? gasto.tasa_cambio.id : null,
                        monedaid: gasto.moneda ? gasto.moneda.id : null
                    })
                }

                for (const gasto_to_add of p_inventario.gastosToAdd || []) {
                    const new_gasto = await updated_inventario.$relatedQuery('gastos', trx).insert({
                        tipogastoid: gasto_to_add.tipo_gasto.id,
                        descripcion: gasto_to_add.descripcion,
                        monto: gasto_to_add.monto,
                        tasacambioid: gasto_to_add.tasa_cambio.id,
                        monedaid: gasto_to_add.moneda.id
                    })
                }

                for (const gasto_to_remove of p_inventario.gastosToRemove || []) {
                    await updated_inventario.$relatedQuery('gastos', trx).deleteById(gasto_to_remove.id)
                }


                return updated_inventario.$query(trx).withGraphFetched('[status_inventario,vehiculo,sucursal,color,gastos]')

            })
            console.log('Great success! Transaction Succeded')
            return inventario

        } catch (error) {
            console.log(error)
            return error
        }



    }
}
