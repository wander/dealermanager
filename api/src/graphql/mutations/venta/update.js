import models from "../../../models";
import Venta from "../../types/venta";
import ventaInput from "../../inputs/venta";
import {StatusInv} from '../../../constants/statusInv'

export default{
    type:Venta,
    args:{
        venta:{
            type:ventaInput
        }
    },

    async resolve(source,args){
       const vendidoid= (await models.status_inventario.query().where('descripcion',StatusInv.Vendido).first()).id
        const venta=args.venta
        const updated_venta=await models.venta.transaction(async trx => {
        
       const v_venta= await models.venta.query().patchAndFetchById(venta.id, {
            cliente:venta.cliente,
            cedula:venta.cedula,
            monto:venta.monto,
            sucursal_id:venta.sucursal.id,
            inventario_id:venta.inventario.id,
            usuario_id:venta.usuario.id,
            tasacambio_id:venta.tasa_cambio.id,
            moneda_id:venta.moneda.id,
            updatedAt:new Date()
        })

       await v_venta.$relatedQuery('inventario',trx).patchAndFetchById(venta.inventario?.id,{
            statusinvid:vendidoid
        })

        return v_venta
    })

        return updated_venta.$query()
        //.withGraphFetched('[sucursal,inventario,usuario,tasa_cambio,moneda')
    }
}