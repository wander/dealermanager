import models from "../../../models";
import Venta from "../../types/venta";
import ventaInput from "../../inputs/venta";;
import { orderId } from '../../../utils'
import {StatusInv} from '../../../constants/statusInv'

export default {
    type: Venta,
    args: {
        venta: {
            type: ventaInput
        }
    },

    async resolve(source, args,ctx) {
        const venta = args.venta
        const orderid = orderId().generate()
        let usuarioid= await ctx.user.then(user => {
            return user.id
          })
      
        const vendidoid = (await models.status_inventario.query().where('descripcion', StatusInv.Vendido).first()).id
        const created_venta = await models.venta.transaction(async trx => {
            const new_venta = await models.venta.query().insertAndFetch({
                codigofactura:orderid,
                cliente: venta.cliente,
                cedula: venta.cedula,
                monto: venta.monto,
                sucursal_id: venta.sucursal.id,
                inventario_id: venta.inventario.id,
                usuario_id: usuarioid,
                tasacambio_id: venta.tasa_cambio.id,
                moneda_id: venta.moneda.id,

            })

            await new_venta.$relatedQuery('inventario', trx).patchAndFetchById(venta.inventario?.id, {
                statusinvid: vendidoid
            })

            return new_venta
        })
        return created_venta.$query()
        //.withGraphFetched('[sucursal,inventario,usuario,tasa_cambio,moneda')
    }
}