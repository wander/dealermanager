import models from "../../../models";
import Usuario from "../../types/usuario";
import usuariolInput from "../../inputs/usuario";
const bcrypt = require('bcrypt')
export default {
    type: Usuario,
    args: {
        usuario: {
            type: usuariolInput
        }
    },
    async resolve(source, args) {
        var user = args.usuario
        try {

            const updated_user = await models.usuario.transaction(async trx => {
                var updated_user = await models.usuario.query(trx).updateAndFetchById(args.usuario.id, {
                    nombreusuario: args.usuario.nombreusuario,
                    //clave: await bcrypt.hash(args.usuario.clave, 10),
                    email: user.email,
                    nombre: user.nombre,
                    activo: user.activo
                })

                for (const rol of user.rolesToAdd || []) {
                    await updated_user.$relatedQuery('roles', trx).relate({
                        id: rol.id,
                        updatedAt: new Date()
                    })
                }

                for (const rol of user.rolesToRemove || []) {
                    await updated_user.$relatedQuery('roles', trx).unrelate().where('id', rol.id)
                }

                for (const sucursal of user.sucursalesToAdd || []) {
                    await updated_user.$relatedQuery('sucursales', trx).relate({
                        id: sucursal.id,
                        updatedAt: new Date()
                    })
                }

                for (const sucursal of user.sucursalesToRemove || []) {
                    await updated_user.$relatedQuery('sucursales', trx).unrelate().where('id', sucursal.id)
                }

                return updated_user.$query(trx).withGraphFetched('[roles,sucursales]')

            })

            
            console.log('Great success! Transaction Succeded')
            return updated_user;

        } catch (error) {
            console.log(error)
            return error
        }
    }
}

