import models from "../../../models";
import Usuario from "../../types/usuario";
import usuariolInput from "../../inputs/usuario";
const bcrypt = require('bcrypt')
export default {
  type: Usuario,
  args: {
    usuario: {
      type: usuariolInput
    }
  },
  async resolve(source, args) {  
      
    try {

        const user = await models.usuario.transaction(async trx => {
            var updated_user = await models.usuario.query(trx).updateAndFetchById(args.usuario.id,{
                clave: await bcrypt.hash(args.usuario.clave, 10)
            })
            
            return updated_user.$query(trx).withGraphFetched('[roles,sucursales]')

        })
        console.log('Great success! Transaction Succeded')
        return user;
        
    } catch (error) {
        console.log(error)
        return error
    }
  }
}

