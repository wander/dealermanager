import models from "../../../models";
import Usuario from "../../types/usuario";
import usuariolInput from "../../inputs/usuario";
const bcrypt = require('bcrypt')
export default {
  type: Usuario,
  args: {
    usuario: {
      type: usuariolInput
    }
  },
  async resolve(source, args) {  
    var user=args.usuario
    try {
        

        const created_user = await models.usuario.transaction(async trx => {
            var created_user = await models.usuario.query(trx).insertAndFetch({
                nombreusuario: user.nombreusuario,
                clave: await bcrypt.hash(user.clave, 10),
                email: user.email,
                nombre:user.nombre,
                activo:user.activo
            })

            for (const rol of user.roles || []) {
                await created_user.$relatedQuery('roles',trx).relate({
                    id: rol.id,
                    createdAt: new Date(),
                    updatedAt: new Date()
                })
            }

            for (const sucursal of user.sucursales || []) {
             await created_user.$relatedQuery('sucursales',trx).relate({
                    id:sucursal.id,
                    createdAt: new Date(),
                    updatedAt: new Date()
                })
            }

            return created_user.$query(trx).withGraphFetched('[roles,sucursales]')

        })
        
        console.log('Great success! Transaction Succeded')
        return created_user;
        
    } catch (error) {
        console.log(error)
        return error
    }
  }
}

