import models from "../../../models";
import Vehiculo from "../../types/vehiculo";
import vehiculoInput from "../../inputs/vehiculo";
import { resolve } from "path";

export default{

    type:Vehiculo,
    args:{
        vehiculo:{
            type:vehiculoInput
        }
    },
    async resolve(source,args){
        const new_vehiculo=await models.vehiculo.query().updateAndFetchById(args.vehiculo.id,{
            modeloid:args.vehiculo.modelo.id,
            tipocombustibleid:args.vehiculo.tipo_combustible.id,
            tipomotorid:args.vehiculo.tipo_motor.id,
            cilindroid:args.vehiculo.cilindro.id,
            tipotransmisionid:args.vehiculo.tipo_transmision.id,
            tipovehiculoid:args.vehiculo.tipo_vehiculo.id,
            tipotrainid:args.vehiculo.tipo_train.id
        });

        return new_vehiculo.$query().withGraphFetched('[modelo,tipo_combustible,tipo_motor,cilindro,tipo_transmision,tipo_vehiculo,tipo_train]')
    }
}