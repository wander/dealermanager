import { GraphQLObjectType, GraphQLInt, GraphQLString } from "graphql";

export default new GraphQLObjectType({
  name: "status_inventario",
  description: "This represent a a status_inventario",
  fields: () => {
    return {
      id: {
        type: GraphQLInt,
        resolve(status_inventario) {
          return status_inventario.id;
        }
      },
      descripcion: {
        type: GraphQLString,
        resolve(status_inventario) {
          return status_inventario.descripcion;
        }
      }
    };
  }
});
