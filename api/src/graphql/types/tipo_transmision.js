import { GraphQLObjectType, GraphQLInt, GraphQLString } from "graphql";

export default new GraphQLObjectType({
  name: "tipo_transmision",
  description: "This represent a a tipo_transmision",
  fields: () => {
    return {
      id: {
        type: GraphQLInt,
        resolve(tipo_transmision) {
          return tipo_transmision.id;
        }
      },
      descripcion: {
        type: GraphQLString,
        resolve(tipo_transmision) {
          return tipo_transmision.descripcion;
        }
      }
    };
  }
});
