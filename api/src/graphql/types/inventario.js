//TODO: AGREGAR PLACA Y TITULO DE VEHICULO

import { GraphQLObjectType, GraphQLInt, GraphQLString, GraphQLList, GraphQLFloat, GraphQLScalarType } from "graphql";
import Vehiculo from './vehiculo'
import StatusInventario from './status_inventario'
import Gasto from './gasto'
import Color from './color'
import Sucursal from './sucursal'
import { raw, fn, ref } from 'objection'
import models from '../../models'
var GraphQLDate = require('graphql-date')

export default new GraphQLObjectType({
  name: "inventario",
  description: "This represent a a inventario",
  fields: () => {
    return {
      id: {
        type: GraphQLInt,
        resolve(inventario) {
          return inventario.id;
        }
      },
      descripcion: {
        type: GraphQLString,
        resolve(inventario) {
          return inventario.descripcion;
        }
      },
      codigosubasta: {
        type: GraphQLString,
        resolve(inventario) {
          return inventario.codigosubasta;
        }
      },
      vin: {
        type: GraphQLString,
        resolve(inventario) {
          return inventario.vin;
        }
      },
      year: {
        type: GraphQLString,
        resolve(inventario) {
          return inventario.year;
        }
      },
      status_inventario: {
        type: StatusInventario,
        resolve(inventario) {
          if (inventario.hasOwnProperty("status_inventario")) {
            return inventario.status_inventario;
          }
          return inventario.$query().withGraphFetched('status_inventario').then(inventario => {
            return inventario.status_inventario;
          });
        }
      },
      precio_venta:{
        type:GraphQLFloat,
        resolve(inventario){
          return inventario.precio_venta
        }
      },
      gastos: {
        type: new GraphQLList(Gasto),
        resolve(inventario) {
          if (inventario.hasOwnProperty("gastos")) {
            return inventario.gastos;
          }
          return inventario.$query().withGraphFetched('gastos').then(inventario => {
            return inventario.gastos;
          });
        }
      },
      total_gastos: {

        type: GraphQLFloat,
        async resolve(inventario) {
          if (inventario.hasOwnProperty("gastos")) {
            return inventario.total_gastos?.total_gastos
          }
          const total_gastos =

            models.gasto.query()
            .joinRelated('[tasa_cambio]',{

              aliases:{
              tasacambio:'tc'
              }
            })
            .select(raw('ifnull(sum(gasto.monto*tasa_cambio.valor),0) as total_gastos'))              
            .as('total_gastos')
            .where('gasto.inventarioid', inventario.id).then(total_gastos => { 
                return total_gastos[0]['total_gastos']
                //['ifnull(sum(`monto`),0)']
              })


          /*    inventario.$query().withGraphFetched('gastos').then(inventario => {
               return inventario.gastos.reduce?.((currenttotal,item)=>{
                 return item.monto+currenttotal
               })
             }); */
          //  await inventario.$relatedQuery('gastos').alias('g')
          //.select(raw('sum(ifnull(g.monto,0))').as('total_gastos')).then(total_gastos=>{return total_gastos})
          ///.sum(raw('ifnull(g.monto,0)'))           
          //.as('total_gastos')
          //.then(total_gastos=>{return total_gastos})
          //const final=total_gastos.then(results=>{return results})
          return total_gastos? total_gastos: 0

        }
      },


      vehiculo: {
        type: Vehiculo,
        resolve(inventario) {
          if (inventario.hasOwnProperty("vehiculo")) {
            return inventario.vehiculo;
          }
          return inventario.$query().withGraphFetched('vehiculo').then(inventario => {
            return inventario.vehiculo;
          });
        }
      },
      sucursal: {
        type: Sucursal,
        resolve(inventario) {
          if (inventario.hasOwnProperty("sucursal")) {
            return inventario.sucursal;
          }
          return inventario.$query().withGraphFetched('sucursal').then(inventario => {
            return inventario.sucursal;
          });
        }
      },
      color: {
        type: Color,
        resolve(inventario) {
          if (inventario.hasOwnProperty("color")) {
            return inventario.color;
          }
          return inventario.$query().withGraphFetched('color').then(inventario => {
            return inventario.color;
          });
        }
      },
      createdat: {
        type: GraphQLDate,
        resolve(inventario) {
          return inventario.createdat;
        }
      },
      updatedat: {
        type: GraphQLDate,
        resolve(inventario) {
          return inventario.updatedat;
        }
      },
    };
  }
});
