import { GraphQLObjectType, GraphQLInt, GraphQLString, GraphQLList,GraphQLBoolean } from "graphql";
import Rol from './rol'
import Sucursal from './sucursal'
export default new GraphQLObjectType({
  name: "usuario",
  description: "This represent a a rol",
  fields: () => {
    return {
      id: {
        type: GraphQLInt,
        resolve(usuario) {
          return usuario.id;
        }
      },
      nombreusuario: {
        type: GraphQLString,
        resolve(usuario) {
          return usuario.nombreusuario;
        }
      },
      clave: {
        type: GraphQLString,
        resolve(usuario) {
          return usuario.clave;
        }
      },
      nombre: {
        type: GraphQLString,
        resolve(usuario) {
          return usuario.nombre;
        }
      },
      email: {
        type: GraphQLString,
        resolve(usuario) {
          return usuario.email;
        }
      },
      activo: {
        type: GraphQLBoolean,
        resolve(usuario) {
          return usuario.activo;
        }
      },
      roles: {
        type: new GraphQLList(Rol),
        resolve(usuario) {
          if (usuario.hasOwnProperty('roles'))
            return usuario.roles

          return usuario.$query().withGraphFetched('roles').then(usuario => {
            return usuario.roles
          })
        }
      },
      sucursales:{
        type:new GraphQLList(Sucursal),
        resolve(usuario) {
          if (usuario.hasOwnProperty('sucursales'))
            return usuario.sucursales

          return usuario.$query().withGraphFetched('sucursales').then(usuario => {
            return usuario.sucursales
          })
        }
      }
    };
  }
});
