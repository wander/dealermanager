import { GraphQLObjectType, GraphQLInt, GraphQLString } from "graphql";
import Marca from './marca'
export default new GraphQLObjectType({
  name: "modelo",
  description: "This represent a a modelo",
  fields: () => {
    return {
      id: {
        type: GraphQLInt,
        resolve(modelo) {
          return modelo.id;
        }
      },
      descripcion: {
        type: GraphQLString,
        resolve(modelo) {
          return modelo.descripcion;
        }
      },
      marca: {
        type:Marca,
        resolve(modelo) {
          if (modelo.hasOwnProperty("marca")) 
            return modelo.marca;
          
          return modelo.$query().withGraphFetched('marca').then(modelo=> { 
            //console.log(almacen.tramos);
          return modelo.marca;
        });
  
        }
      },
    };
  }
});
