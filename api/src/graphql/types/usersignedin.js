import { GraphQLObjectType, GraphQLString } from "graphql";
import Usuario from './usuario'
export default new GraphQLObjectType({
  name: "usersignedin",
  description: "This represent a a user signed in",
  fields: () => {
    return {
      usuario: {
        type: Usuario,
        resolve(usersignedin) {
          return usersignedin.usuario;
        }
      },
      token: {
        type: GraphQLString,
        resolve(usersignedin) {
          return usersignedin.token;
        }
      }
    };
  }
});
