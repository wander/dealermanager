//import Db from "../../models";
import Inventario from './inventario'
import Usuario from './usuario'
import Sucursal from './sucursal'
import Tasa_cambio from './tasa_cambio'
import Moneda from './moneda'
//import empresaLoader from '../../loaders/empresasLoader';
//import almacenByventaLoader from '../../loaders/almacenesByventaLoader';
import { GraphQLObjectType, GraphQLInt, GraphQLString, graphqldata, GraphQLFloat } from "graphql";
var GraphQLDate = require('graphql-date')

export default new GraphQLObjectType({
    name: "venta",
    description: "This represent a venta",
    fields: () => {
        return {
            id: {
                type: GraphQLInt,
                resolve(venta) {
                    return venta.id;
                }
            },
            codigofactura: {
                type: GraphQLString,
                resolve(venta) {
                    return venta.codigofactura
                }
            },

            cliente: {
                type: GraphQLString,
                resolve(venta) {
                    return venta.cliente
                }
            },
            cedula: {
                type: GraphQLString,
                resolve(venta) {
                    return venta.cedula
                }
            },
            monto: {
                type: GraphQLFloat,
                resolve(venta) {
                    return venta.monto
                }
            },


            inventario: {
                type: Inventario,
                resolve(venta) {
                    if (venta.hasOwnProperty("inventario"))
                        return venta.inventario;
                    return venta.$query().withGraphFetched('inventario').then(venta => {
                        return venta.inventario;
                    })
                }
            },
            sucursal: {
                type: Sucursal,
                resolve(venta) {
                    if (venta.hasOwnProperty("sucursal"))
                        return venta.sucursal;
                    return venta.$query().withGraphFetched('sucursal').then(venta => {
                        return venta.sucursal;
                    })
                }
            },
            usuario: {
                type: Usuario,
                resolve(venta) {
                    if (venta.hasOwnProperty("usuario"))
                        return venta.usuario;
                    return venta.$query().withGraphFetched('usuario').then(venta => {
                        return venta.usuario;
                    })
                }
            },
            tasa_cambio: {
                type: Tasa_cambio,
                resolve(venta) {
                    if (venta.hasOwnProperty("tasa_cambio"))
                        return venta.tasa_cambio;
                    return venta.$query().withGraphFetched('tasa_cambio').then(venta => {
                        return venta.tasa_cambio;
                    })
                }
            },
            moneda: {
                type: Moneda,
                resolve(venta) {
                    if (venta.hasOwnProperty("moneda"))
                        return venta.moneda;
                    return venta.$query().withGraphFetched('moneda').then(venta => {
                        return venta.moneda;
                    })
                }
            },
            createdat: {
                type: GraphQLDate,
                resolve(inventario) {
                    return inventario.createdat;
                }
            },
            updatedat: {
                type: GraphQLDate,
                resolve(inventario) {
                    return inventario.updatedat;
                }
            },

        }

    }
});
