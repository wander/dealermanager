import { GraphQLObjectType, GraphQLInt,GraphQLFloat, GraphQLBoolean } from "graphql";

import Moneda from './moneda'
var GraphQLDate= require('graphql-date')

export default new GraphQLObjectType({
  name: "tasa_cambio",
  description: "This represent a a tasa_cambio",
  fields: () => {
    return {
      id: {
        type: GraphQLInt,
        resolve(tasa_cambio) {
          return tasa_cambio.id;
        }
      },   
        valor: {
        type: GraphQLFloat,
        resolve(tasa_cambio) {
          return tasa_cambio.valor;
        }
      },
      prederminada:{
        type:GraphQLBoolean,
        resolve(tasa_cambio){
          return tasa_cambio.prederminada
        }
      },
      moneda_predeterminada:{
          type:Moneda,
          resolve(tasa_cambio){
            if (tasa_cambio.hasOwnProperty("moneda_predeterminada")) {
                return tasa_cambio.moneda_predeterminada;
              }
             return tasa_cambio.$query().withGraphFetched('moneda_predeterminada').then(tasa_cambio=>{
               return tasa_cambio.moneda_predeterminada;
             });
          }
      },
      moneda_alterna:{
        type:Moneda,
        resolve(tasa_cambio){
          if (tasa_cambio.hasOwnProperty("moneda_alterna")) {
              return tasa_cambio.moneda_alterna;
            }
           return tasa_cambio.$query().withGraphFetched('moneda_alterna').then(tasa_cambio=>{
             return tasa_cambio.moneda_alterna;
           });
        }
    },
      createdat: {
        type: GraphQLDate,
        resolve(tasa_cambio) {
          return tasa_cambio.createdat;
        }
      },
      updatedat: {
        type: GraphQLDate,
        resolve(tasa_cambio) {
          return tasa_cambio.updatedat;
        }
      },
    };
  }
});
