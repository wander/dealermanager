//import Db from "../../models";
import Modelo from './modelo'
import TipoCombustible from './tipo_combustible'
import TipoMotor from './tipo_motor'
import Cilindro from './cilindro'
import TipoTransmision from './tipo_transmision'
import TipoVehiculo from './tipo_vehiculo'
import TipoTrain from './tipo_train'
//import empresaLoader from '../../loaders/empresasLoader';
//import almacenByvehiculoLoader from '../../loaders/almacenesByvehiculoLoader';
import { GraphQLObjectType, GraphQLInt, GraphQLString, GraphQLBoolean } from "graphql";


export default new GraphQLObjectType({
    name: "vehiculo",
    description: "This represent a vehiculo",
    fields: () => {
        return {
            id: {
                type: GraphQLInt,
                resolve(vehiculo) {
                    return vehiculo.id;
                }
            },


            modelo: {
                type: Modelo,
                resolve(vehiculo) {
                    if (vehiculo.hasOwnProperty("modelo"))
                        return vehiculo.modelo;
                    return vehiculo.$query().withGraphFetched('modelo').then(vehiculo => {
                        return vehiculo.modelo;
                    })
                }
            },
            tipo_combustible: {
                type: TipoCombustible,
                resolve(vehiculo) {
                    if (vehiculo.hasOwnProperty("tipo_combustible"))
                        return vehiculo.tipo_combustible;
                    return vehiculo.$query().withGraphFetched('tipo_combustible').then(vehiculo => {
                        return vehiculo.tipo_combustible;
                    })
                }
            },
            tipo_motor: {
                type: TipoMotor,
                resolve(vehiculo) {
                    if (vehiculo.hasOwnProperty("tipo_motor"))
                        return vehiculo.tipo_motor;
                    return vehiculo.$query().withGraphFetched('tipo_motor').then(vehiculo => {
                        return vehiculo.tipo_motor;
                    })
                }
            },
            cilindro: {
                type: Cilindro,
                resolve(vehiculo) {
                    if (vehiculo.hasOwnProperty("cilindro"))
                        return vehiculo.cilindro;
                    return vehiculo.$query().withGraphFetched('cilindro').then(vehiculo => {
                        return vehiculo.cilindro;
                    })
                }
            },
            tipo_transmision: {
                type: TipoTransmision,
                resolve(vehiculo) {
                    if (vehiculo.hasOwnProperty("tipo_transmision"))
                        return vehiculo.tipo_transmision;
                    return vehiculo.$query().withGraphFetched('tipo_transmision').then(vehiculo => {
                        return vehiculo.tipo_transmision;
                    })
                }
            },
            tipo_vehiculo: {
                type: TipoVehiculo,
                resolve(vehiculo) {
                    if (vehiculo.hasOwnProperty("tipo_vehiculo"))
                        return vehiculo.tipo_vehiculo;
                    return vehiculo.$query().withGraphFetched('tipo_vehiculo').then(vehiculo => {
                        return vehiculo.tipo_vehiculo;
                    })
                }
            },
            tipo_train: {
                type: TipoTrain,
                resolve(vehiculo) {
                    if (vehiculo.hasOwnProperty("tipo_train"))
                        return vehiculo.tipo_train;
                    return vehiculo.$query().withGraphFetched('tipo_train').then(vehiculo => {
                        return vehiculo.tipo_train;
                    })
                }
            },
        }

    }
});
