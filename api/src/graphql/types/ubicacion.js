import { GraphQLObjectType, GraphQLInt, GraphQLString } from "graphql";

export default new GraphQLObjectType({
  name: "ubicacion",
  description: "This represent a a ubicacion",
  fields: () => {
    return {
      id: {
        type: GraphQLInt,
        resolve(ubicacion) {
          return ubicacion.id;
        }
      },
      descripcion: {
        type: GraphQLString,
        resolve(ubicacion) {
          return ubicacion.descripcion;
        }
      }
    };
  }
});
