
import cilindro from './cilindro'
import color from './color'
import dealer from './dealer'
import gasto from './gasto'
import inventario from './inventario'
import marca from './marca'
import modelo from './modelo'
import moneda from './moneda'
import rol from './rol'
import status from './status'
import status_inventario from './status_inventario'
import sucursal from './sucursal'
import tipo_combustible from './tipo_combustible'
import tipo_gasto from './tipo_gasto'
import tipo_motor from './tipo_motor'
import tipo_train from './tipo_train'
import tipo_transmision from './tipo_transmision'
import usuario from './usuario'
import vehiculo from './vehiculo'
import tasa_cambio from './tasa_cambio'
import usersignedin from './usersignedin'
import venta from './venta'
export default {
  cilindro,
  color,
  dealer,
  gasto,
  inventario,
  marca,
  modelo,
  moneda,
  rol,
  status,
  status_inventario,
  sucursal,
  tipo_combustible,
  tipo_gasto,
  tipo_motor,
  tipo_train,
  tipo_transmision,
  usuario,
  vehiculo,
  tasa_cambio,
  usersignedin,
  venta

}