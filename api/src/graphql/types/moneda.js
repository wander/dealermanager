import { GraphQLObjectType, GraphQLInt, GraphQLString,GraphQLBoolean } from "graphql";

export default new GraphQLObjectType({
  name: "moneda",
  description: "This represent a a moneda",
  fields: () => {
    return {
      id: {
        type: GraphQLInt,
        resolve(moneda) {
          return moneda.id;
        }
      },
      descripcion: {
        type: GraphQLString,
        resolve(moneda) {
          return moneda.descripcion;
        }
      },
      predeterminada:{
        type:GraphQLBoolean,
        resolve(moneda){
          return moneda.predeterminada
        }
      },
      nomenclatura:{
        type:GraphQLString,
        resolve(moneda){
          return moneda.nomenclatura
        }
      }
    };
  }
});
