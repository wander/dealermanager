import { GraphQLObjectType, GraphQLInt, GraphQLString } from "graphql";

export default new GraphQLObjectType({
  name: "tipo_train",
  description: "This represent a a tipo_train",
  fields: () => {
    return {
      id: {
        type: GraphQLInt,
        resolve(tipo_train) {
          return tipo_train.id;
        }
      },
      descripcion: {
        type: GraphQLString,
        resolve(tipo_train) {
          return tipo_train.descripcion;
        }
      }
    };
  }
});
