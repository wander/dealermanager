import { GraphQLObjectType, GraphQLInt, GraphQLString,GraphQLList } from "graphql";
import Sucursal from './sucursal'
export default new GraphQLObjectType({
  name: "dealer",
  description: "This represent a a dealer",
  fields: () => {
    return {
      id: {
        type: GraphQLInt,
        resolve(dealer) {
          return dealer.id;
        }
      },
      rnc: {
        type: GraphQLString,
        resolve(dealer) {
          return dealer.rnc;
        }
      },
      nombre: {
        type: GraphQLString,
        resolve(dealer) {
          return dealer.nombre;
        }
      },
      pathlogo: {
        type: GraphQLString,
        resolve(dealer) {
          return dealer.pathlogo;
        }
      },
      email: {
        type: GraphQLString,
        resolve(dealer) {
          return dealer.email;
        }
      },
      website: {
        type: GraphQLString,
        resolve(dealer) {
          return dealer.website;
        }
      },
      sucursales:{
        type: new GraphQLList(Sucursal),
        resolve(dealer){
           if (dealer.hasOwnProperty('sucursales'))
               return dealer.sucursales
            
            return dealer.$query().withGraphFetched('sucursales').then(dealer=>{
                return dealer.sucursales
            })
            // return sucursalesByEmpresaIds.load(empresa.id).then(sucursales=>{
            //     return sucursales;
            // });
            
          //  return sucursalModel.query().where('empresaId',empresa.id);
           
        }
    }
    };
  }
});
