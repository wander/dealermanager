import { GraphQLObjectType, GraphQLInt, GraphQLString } from "graphql";

export default new GraphQLObjectType({
  name: "color",
  description: "This represent a a color",
  fields: () => {
    return {
      id: {
        type: GraphQLInt,
        resolve(color) {
          return color.id;
        }
      },
      descripcion: {
        type: GraphQLString,
        resolve(color) {
          return color.descripcion;
        }
      },
      class: {
        type: GraphQLString,
        resolve(color) {
          return color.class;
        }
      }
    };
  }
});
