import { GraphQLObjectType, GraphQLInt, GraphQLString } from "graphql";

export default new GraphQLObjectType({
  name: "status",
  description: "This represent a a status",
  fields: () => {
    return {
      id: {
        type: GraphQLInt,
        resolve(status) {
          return status.id;
        }
      },
      descripcion: {
        type: GraphQLString,
        resolve(status) {
          return status.descripcion;
        }
      }
    };
  }
});
