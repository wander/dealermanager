import { GraphQLObjectType, GraphQLInt, GraphQLString,GraphQLList } from "graphql";
import Modelos from './modelo'
export default new GraphQLObjectType({
  name: "marca",
  description: "This represent a a marca",
  fields: () => {
    return {
      id: {
        type: GraphQLInt,
        resolve(marca) {
          return marca.id;
        }
      },
      descripcion: {
        type: GraphQLString,
        resolve(marca) {
          return marca.descripcion;
        }
      },
      modelos: {
        type: new GraphQLList(Modelos),
        resolve(marca) {
          if (marca.hasOwnProperty("modelos")) 
            return marca.modelos;
          
          return marca.$query().withGraphFetched('modelos').then(marca=> { 
            //console.log(almacen.tramos);
          return marca.modelos;
        });
          // return tramosByAlmacen.load(almacen.id).then(tramos=>{
          //     return tramos;
          //   });
         // console.log(almacen.$query().withGraphFetched('tramos').tramos);
         // return almacen.$query().withGraphFetched('tramos');
         // return almacenModel.relatedQuery('tramos');
          // return tramoModel.query().where('almacenId', almacen.id);
        }
      },
    };
  }
});
