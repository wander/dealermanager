import { GraphQLObjectType, GraphQLInt, GraphQLString } from "graphql";

export default new GraphQLObjectType({
  name: "cilindro",
  description: "This represent a a cilindro",
  fields: () => {
    return {
      id: {
        type: GraphQLInt,
        resolve(cilindro) {
          return cilindro.id;
        }
      },
      descripcion: {
        type: GraphQLString,
        resolve(cilindro) {
          return cilindro.descripcion;
        }
      }
    };
  }
});
