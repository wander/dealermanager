import { GraphQLObjectType, GraphQLInt, GraphQLString } from "graphql";

export default new GraphQLObjectType({
  name: "tipo_gasto",
  description: "This represent a a tipo_gasto",
  fields: () => {
    return {
      id: {
        type: GraphQLInt,
        resolve(tipo_gasto) {
          return tipo_gasto.id;
        }
      },
      descripcion: {
        type: GraphQLString,
        resolve(tipo_gasto) {
          return tipo_gasto.descripcion;
        }
      }
    };
  }
});
