import { GraphQLObjectType, GraphQLInt, GraphQLString,GraphQLFloat } from "graphql";
import tipoGasto from './tipo_gasto'
import Inventario from './inventario'
import TasaCambio from './tasa_cambio'
import Ubicacion from './ubicacion'
import Moneda from './moneda'
var GraphQLDate= require('graphql-date')

export default new GraphQLObjectType({
  name: "gasto",
  description: "This represent a a gasto",
  fields: () => {
    return {
      id: {
        type: GraphQLInt,
        resolve(gasto) {
          return gasto.id;
        }
      },
      descripcion: {
        type: GraphQLString,
        resolve(gasto) {
          return gasto.descripcion;
        }
      },
      tipo_gasto: {
        type: tipoGasto,
        resolve(gasto) {
            if (gasto.hasOwnProperty("tipo_gasto")) {
                return gasto.tipo_gasto;
              }
             return gasto.$query().withGraphFetched('tipo_gasto').then(gasto=>{
               return gasto.tipo_gasto;
             });
        }
      },
      monto: {
        type: GraphQLFloat,
        resolve(gasto) {
          return gasto.monto;
        }
      },
      inventario: {
        type: Inventario,
        resolve(gasto) {
            if (gasto.hasOwnProperty("inventario")) {
                return gasto.inventario;
              }
             return gasto.$query().withGraphFetched('inventario').then(gasto=>{
               return gasto.inventario;
             });
        }
      },
      moneda: {
        type: Moneda,
        resolve(gasto) {
            if (gasto.hasOwnProperty("moneda")) {
                return gasto.moneda;
              }
             return gasto.$query().withGraphFetched('moneda').then(gasto=>{
               return gasto.moneda;
             });
        }
      },
      tasa_cambio: {
        type: TasaCambio,
        resolve(gasto) {
            if (gasto.hasOwnProperty("tasa_cambio")) {
                return gasto.tasa_cambio;
              }
             return gasto.$query().withGraphFetched('tasa_cambio').then(gasto=>{
               return gasto.tasa_cambio;
             });
        }
      },
      ubicacion: {
        type: Ubicacion,
        resolve(gasto) {
            if (gasto.hasOwnProperty("ubicacion")) {
                return gasto.ubicacion;
              }
             return gasto.$query().withGraphFetched('ubicacion').then(gasto=>{
               return gasto.ubicacion;
             });
        }
      },
      createdat: {
        type: GraphQLDate,
        resolve(gasto) {
          return gasto.createdat;
        }
      },
      updatedat: {
        type: GraphQLDate,
        resolve(gasto) {
          return gasto.updatedat;
        }
      },
    };
  }
});
