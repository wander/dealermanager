import { GraphQLObjectType, GraphQLInt, GraphQLString } from "graphql";

export default new GraphQLObjectType({
  name: "tipo_vehiculo",
  description: "This represent a a tipo_vehiculo",
  fields: () => {
    return {
      id: {
        type: GraphQLInt,
        resolve(tipo_vehiculo) {
          return tipo_vehiculo.id;
        }
      },
      descripcion: {
        type: GraphQLString,
        resolve(tipo_vehiculo) {
          return tipo_vehiculo.descripcion;
        }
      }
    };
  }
});
