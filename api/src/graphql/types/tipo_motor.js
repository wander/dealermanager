import { GraphQLObjectType, GraphQLInt, GraphQLString } from "graphql";

export default new GraphQLObjectType({
  name: "tipo_motor",
  description: "This represent a a tipo_motor",
  fields: () => {
    return {
      id: {
        type: GraphQLInt,
        resolve(tipo_motor) {
          return tipo_motor.id;
        }
      },
      descripcion: {
        type: GraphQLString,
        resolve(tipo_motor) {
          return tipo_motor.descripcion;
        }
      }
    };
  }
});
