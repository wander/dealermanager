import { GraphQLObjectType, GraphQLInt, GraphQLString } from "graphql";

export default new GraphQLObjectType({
  name: "tipoCombustible",
  description: "This represent a a tipo Combustible",
  fields: () => {
    return {
      id: {
        type: GraphQLInt,
        resolve(tipoCombustible) {
          return tipoCombustible.id;
        }
      },
      descripcion: {
        type: GraphQLString,
        resolve(tipoCombustible) {
          return tipoCombustible.descripcion;
        }
      }
    };
  }
});
