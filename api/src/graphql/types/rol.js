import { GraphQLObjectType, GraphQLInt, GraphQLString } from "graphql";

export default new GraphQLObjectType({
  name: "rol",
  description: "This represent a a rol",
  fields: () => {
    return {
      id: {
        type: GraphQLInt,
        resolve(rol) {
          return rol.id;
        }
      },
      descripcion: {
        type: GraphQLString,
        resolve(rol) {
          return rol.descripcion;
        }
      }
    };
  }
});
