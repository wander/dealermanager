//import Db from "../../models";
import Dealer from './dealer';

//import empresaLoader from '../../loaders/empresasLoader';
//import almacenBySucursalLoader from '../../loaders/almacenesBySucursalLoader';
import { GraphQLObjectType, GraphQLInt, GraphQLString, GraphQLBoolean } from "graphql";


export default new GraphQLObjectType({
  name: "sucursal",
  description: "This represent a sucursal",
  fields: () => {
    return {
      id: {
        type: GraphQLInt,
        resolve(sucursal) {
          return sucursal.id;
        }
      },
    
      descripcion: {
        type: GraphQLString,
        resolve(sucursal) {
          return sucursal.descripcion;
        }
      },
      direccion: {
        type: GraphQLString,
        resolve(sucursal) {
          return sucursal.direccion;
        }
      },
      telefonos: {
        type: GraphQLString,
        resolve(sucursal) {
          return sucursal.telefonos;
        }
      },
     
      dealer: {
        type: Dealer,
        resolve(sucursal) {
          if (sucursal.hasOwnProperty("dealer"))
            return sucursal.dealer;
        return sucursal.$query().withGraphFetched('dealer').then(sucursal=>{
          return sucursal.dealer;
        })
        }
      }
    };
  }
});
