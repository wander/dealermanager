import { GraphQLObjectType, GraphQLSchema } from 'graphql';
import { rule, shield, and, or, not } from 'graphql-shield'
import { applyMiddleware } from 'graphql-middleware';
import queries from './queries';
import mutations from './mutations';
import { validateJwt } from "../utils";
import {Role} from '../constants/role'
var schema = new GraphQLSchema({
  query: new GraphQLObjectType({
    name: "Query",
    fields: queries
  }),
  mutation: new GraphQLObjectType({
    name: "Mutation",
    fields: mutations
  }),
  types: [

  ]
});
const isAuthenticated = rule({ cache: 'contextual' })(
  async (parent, args, ctx, info) => {
  return validateJwt(ctx.req);
  // return ctx.user !== null;
});
const allow = rule({ cache: 'contextual' })(
  async (parent, args, ctx, info) => {
 return true
});

const isAdmin = rule({ cache: 'contextual' })(
  async (parent, args, ctx, info) => {
    const userroles=await ctx.user.then(user=>{
      return user.roles
    })
    var validrule = userroles.some(item => item.descripcion === Role.Admin);
    //console.log(isAuthenticatedadmin);
    return validrule
  }
);
const isSuperAdmin = rule({ cache: 'contextual' })(
  async (parent, args, ctx, info) => {
    const userroles=await ctx.user.then(user=>{
      return user.roles
    })
    var validrule = userroles.some(item => item.descripcion ===Role.SuperAdmin);
    //console.log(isAuthenticatedadmin);
    return validrule
  //isAuthenticatedadmin;
});
const permissions = shield({
  Query: {
    cilindro: isAuthenticated,
    cilindros: isAuthenticated,

    color: isAuthenticated,
    colors: isAuthenticated,

    dealer: isAuthenticated,
    dealers: and(isAuthenticated,isSuperAdmin),
    dealerValExists: isAuthenticated,
    dealersByUsuario:and(isAuthenticated,isAdmin),

    marca: isAuthenticated,
    marcas: isAuthenticated,

    modelo: isAuthenticated,
    modelos: isAuthenticated,

    status: isAuthenticated,
    statuses: isAuthenticated,

    status_inv: isAuthenticated,
    statuses_inv: isAuthenticated,

    tipo_comb: isAuthenticated,
    tipo_combs: isAuthenticated,

    tipo_gasto: isAuthenticated,
    tipo_gastos: isAuthenticated,

    tipo_motor: isAuthenticated,
    tipo_motores: isAuthenticated,

    tipo_train: isAuthenticated,
    tipo_trains: isAuthenticated,

    tipo_tran: isAuthenticated,
    tipo_trans: isAuthenticated,

    tipo_vehiculo: isAuthenticated,
    tipo_vehiculos: isAuthenticated,

    gasto: isAuthenticated,
    gastos: isAuthenticated,

    inventario: isAuthenticated,
    inventarios: and(isAuthenticated,isSuperAdmin),
    inventariosByDealer:isAuthenticated,

    sucursal: isAuthenticated,
    sucursales: and(isAuthenticated,isSuperAdmin),
    sucursalValExists: isAuthenticated,
    sucursalesv2:and(isAuthenticated,or(isAdmin,isSuperAdmin)),

    vehiculo: isAuthenticated,
    vehiculos: isAuthenticated,

    ubicacion: isAuthenticated,
    ubicaciones: isAuthenticated,

    moneda: isAuthenticated,
    monedas: isAuthenticated,

    tasa_cambio: isAuthenticated,
    tasa_cambios: isAuthenticated,

    rol:isAuthenticated,
    roles:isAuthenticated,

    usuario:isAuthenticated,
    usuarios:and(isAuthenticated,isSuperAdmin),
    usuariosbyDealer:and(isAuthenticated,isAdmin),
    validateClave:isAuthenticated,
    validateUserName:allow,

    venta:isAuthenticated,
    ventas:isAuthenticated
    
  },
  Mutation: {
    createDealer: isAuthenticated,
    updateDealer: isAuthenticated,

    createSucursal: isAuthenticated,
    updateSucursal: isAuthenticated,

    createVehiculo: isAuthenticated,
    updateVehiculo: isAuthenticated,

    createInventario: isAuthenticated,
    updateInventario: isAuthenticated,

    createGasto: isAuthenticated,
    updateGasto: isAuthenticated,
    deleteGasto: isAuthenticated,

    createUsuario:and(isAuthenticated,or( isAdmin,isSuperAdmin)),
    updateUsuario:and(isAuthenticated,or(isAdmin,isSuperAdmin)),
    updateUsuarioPerfil:isAuthenticated,
    updateUsuarioPass:and(isAuthenticated,or(isAdmin,isSuperAdmin)),

    createVenta:isAuthenticated,
    updateVenta:isAuthenticated,

    login:not(isAuthenticated),
    signup:not(isAuthenticated)
  },


});

schema = applyMiddleware(schema, permissions);
export default schema
//module.exports = { schema, permissions };