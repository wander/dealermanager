import models from '../../../models'
import Venta from '../../types/venta'
import { GraphQLString, GraphQLList, GraphQLBoolean } from 'graphql'
import { sucursalesIds } from '../queryutils'
var GraphQLDate = require('graphql-date')

export default {
    type: GraphQLList(Venta),
    args: {
        param: {
            type: GraphQLString
        },
        startDate: {
            type: GraphQLDate
        },
        endDate: {
            type: GraphQLDate
        },
        withDetails: {
            type: GraphQLBoolean,

        }

    },
    async resolve(root, args, ctx) {

        args.param = args.param ?? '';
        //if(args.param==null)args.param=''
        const details =''
        // args.withDetails ? '[sucursal,usuario,inventario,tasa_cambio,moneda]' : ''
        const whereClause =(args.param || args.param!='') ?
            `MATCH(codigofactura,cliente,cedula) AGAINST(concat(:param,' * ') in BOOLEAN MODE) AND
         CAST(createdAt AS DATE) between CAST(IFNULL(:p_startDate,createdat) AS DATE)
         and CAST(IFNULL(:p_endDate,createdat) AS DATE)`
         :
         `CAST(createdAt AS DATE) between CAST(IFNULL(:p_startDate,createdat) AS DATE)
         and CAST(IFNULL(:p_endDate,createdat) AS DATE)`
        const params=(args.param || args.param!='') ? 
        { param: args.param, p_startDate: args.startDate, p_endDate: args.endDate }:
        { p_startDate: args.startDate, p_endDate: args.endDate }
      //  const whereDateOnly = 

        //"MATCH(codigofactura,cliente,cedula) AGAINST(concat(:param,' * ') in BOOLEAN MODE)"
        const sIds = await sucursalesIds(ctx).then((results) => { return results })


        const ventas = (args.param !== '') ?
            await models.venta.query()
                .whereIn('sucursal_id', sIds)
                .andWhereRaw(whereClause,
                    params)
                .withGraphFetched(details)
            :
            await models.venta.query()
                .whereIn('sucursal_id', sIds)
                .andWhereRaw(whereClause,
                   params)
                .withGraphFetched(details)
        return ventas
    }
}

