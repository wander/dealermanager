import models from '../../../models'
import Venta from '../../types/venta'
import { GraphQLInt } from 'graphql'


export default {
    type: Venta,
    args: {
        id: {
            type: GraphQLInt
        },

    },
    async resolve(root, args) {

        return await models.venta.query().findById(args.id)
            .withGraphFetched('[sucursal,usuario,inventario,tasa_cambio,moneda]')
    }
}

