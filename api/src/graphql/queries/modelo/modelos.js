import { GraphQLList, GraphQLString, GraphQLInt } from "graphql";


import Modelo from "../../types/modelo";
import models from '../../../models';
export default {
  type: new GraphQLList(Modelo),
  args: {
    param: {
      type: GraphQLString
    },
    marcaid:{
      type:GraphQLInt
    }
  },
  async resolve(root, args) {
    if(args.param==undefined)
      return await models.modelo.query().withGraphFetched('[marca]');
      
    return await models.modelo.query().whereRaw('descripcion  like :term'
      , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' }).andWhere('marcaid',args.marcaid).withGraphFetched('[marca]');
  }
};
