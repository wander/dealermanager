import models from '../../../models'
import Modelo from '../../types/modelo'
import { GraphQLInt } from 'graphql';

export default {
    type: Modelo,
    args: {
      id: {
        type: GraphQLInt
      }
    },
   async resolve(root, args) {
     return await models.modelo.query().findById(args.id).withGraphFetched('[marca]');
      // return models.almacen.findByPk(args.id);
    }
  };