import { GraphQLList, GraphQLString, GraphQLBoolean } from "graphql";


import Marca from "../../types/marca";
import models from '../../../models';
export default {
  type: new GraphQLList(Marca),
  args: {
    param: {
      type: GraphQLString
    },
    withModelos:{
      type:GraphQLBoolean
    }
  },
  async resolve(root, args) {

    if(args.param==undefined)
      return await models.marca.query();
      
    if(args.withModelos)
      return await models.marca.query().whereRaw('CONVERT(id,CHAR(50)) LIKE :term or descripcion  like :term'
      , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' }).withGraphFetched('[modelos]');

    return await models.marca.query().whereRaw('CONVERT(id,CHAR(50)) LIKE :term or descripcion  like :term'
      , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' });
  }
};
