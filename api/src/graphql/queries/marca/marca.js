import models from '../../../models'
import Marca from '../../types/marca'
import { GraphQLInt } from 'graphql';

export default {
    type: Marca,
    args: {
      id: {
        type: GraphQLInt
      }
    },
    async resolve(root, args) {
     return await models.marca.query().findById(args.id).withGraphFetched('[modelos]');
      // return models.almacen.findByPk(args.id);
    }
  };