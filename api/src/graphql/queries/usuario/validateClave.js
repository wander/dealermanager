import models from '../../../models'
import { GraphQLBoolean, GraphQLString } from 'graphql';
const bcrypt = require('bcrypt')
export default {
  type: GraphQLBoolean,
  args: {
    clave: {
      type: GraphQLString
    },
  },
    //si retorna true la clave es valida
    async resolve(root, args,ctx) {

      let userid = await ctx.user.then(user => {
        return user.id
      }) 

      var data = await models.usuario.query().findById(userid).select('clave')
      console.log(data)
      const valid = data===undefined ? false: await bcrypt.compare(args.clave, data.clave)
      return valid

      // return models.almacen.findByPk(args.id);
    }
  
}