import models from '../../../models'
import { GraphQLBoolean, GraphQLString } from 'graphql';
const bcrypt = require('bcrypt')
export default {
  type: GraphQLBoolean,
  args: {
    nombreusuario: {
      type: GraphQLString
    },
  },
//si retorna true ya el nombre de usuario existe
    async resolve(root, args) {


      const user = await models.usuario.query().findOne('nombreusuario', '=', args.nombreusuario)
      return user ? user.id>0:false
      // return models.almacen.findByPk(args.id);
    }
  }
