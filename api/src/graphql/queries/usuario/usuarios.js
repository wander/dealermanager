import models from '../../../models'
import Usuario from '../../types/usuario'
import { GraphQLString,GraphQLList } from 'graphql';

export default {
    type: GraphQLList(Usuario),
    args: {
        param: {
            type: GraphQLString
          },
    },
   async  resolve(root, args) {
    if(args.param===undefined) return await models.usuario.query()
      // return models.almacen.findByPk(args.id);

            
      return await models.usuario.query().whereRaw('CONVERT(id,CHAR(50)) LIKE :term or nombreusuario  like :term or nombre like :term or email like :term'
      , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' }).withGraphFetched('[roles,sucursales]');
    }
  };