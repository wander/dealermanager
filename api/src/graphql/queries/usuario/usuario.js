import models from '../../../models'
import Usuario from '../../types/usuario'
import { GraphQLInt } from 'graphql';

export default {
    type: Usuario,
    args: {
      id: {
        type: GraphQLInt
      }
    },
   async  resolve(root, args) {
     return await models.usuario.query().findById(args.id).withGraphFetched('[roles,sucursales]');
      // return models.almacen.findByPk(args.id);
    }
  };