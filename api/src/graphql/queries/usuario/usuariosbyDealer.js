import models from '../../../models'
import Usuario from '../../types/usuario'
import { GraphQLString,GraphQLList } from 'graphql';

export default {
    type: GraphQLList(Usuario),
    args: {
        param: {
            type: GraphQLString
          },
    },
   async  resolve(root, args) {
       
    let dealer_id= await ctx.user.then(user => {
        return user.sucursales[0].dealer_id
      })
    if(args.param===undefined) return await models.usuario.query()
      // return models.almacen.findByPk(args.id);

            
      return await models.usuario.query().alias('u').joinRelated('sucursales.[dealer]',{
         aliases:{
             sucursales:'s',
             dealer:'d'
         } 
      })
      
      .whereRaw("`s:d`.id=:dealerid AND LIKE :term or `u`.nombreusuario  like :term or `u`.nombre like :term or `u`.email like term"
      , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' },  { dealerid: dealer_id }).withGraphFetched('[roles,sucursales]');
    }
  };