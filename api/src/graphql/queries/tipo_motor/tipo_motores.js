import { GraphQLList, GraphQLString } from "graphql";


import Tipo_Motor from '../../types/tipo_motor'
import models from '../../../models';
export default {
  type: new GraphQLList(Tipo_Motor),
  args: {
    param: {
      type: GraphQLString
    }
  },
  async resolve(root, args) {
    if(args.param==undefined)
      return await models.tipo_motor.query();
      
    return await models.tipo_motor.query().whereRaw('CONVERT(id,CHAR(50)) LIKE :term or descripcion  like :term'
      , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' });
  }
};
