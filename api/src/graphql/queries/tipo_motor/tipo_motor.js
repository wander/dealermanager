import models from '../../../models'
import Tipo_Motor from '../../types/tipo_motor'
import { GraphQLInt } from 'graphql';

export default {
    type: Tipo_Motor,
    args: {
      id: {
        type: GraphQLInt
      }
    },
    async resolve(root, args) {
     return await models.tipo_motor.query().findById(args.id);
      // return models.almacen.findByPk(args.id);
    }
  };