import { GraphQLList, GraphQLString } from "graphql";

import Tipo_Vehiculo from '../../types/tipo_vehiculo'
import models from '../../../models';
export default {
  type: new GraphQLList(Tipo_Vehiculo),
  args: {
    param: {
      type: GraphQLString
    }
  },
  async resolve(root, args) {
    if(args.param==undefined)
      return await models.tipo_vehiculo.query();
      
    return await models.tipo_vehiculo.query().whereRaw('CONVERT(id,CHAR(50)) LIKE :term or descripcion  like :term'
      , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' });
  }
};
