import models from '../../../models'
import Tipo_Vehiculo from '../../types/tipo_vehiculo'
import { GraphQLInt } from 'graphql';

export default {
    type: Tipo_Vehiculo,
    args: {
      id: {
        type: GraphQLInt
      }
    },
   async  resolve(root, args) {
     return await models.tipo_vehiculo.query().findById(args.id);
      // return models.almacen.findByPk(args.id);
    }
  };