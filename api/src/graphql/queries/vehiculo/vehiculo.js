import models from '../../../models'
import Vehiculo from '../../types/vehiculo'
import { GraphQLInt, GraphQLBoolean } from 'graphql'


export default{
    type:Vehiculo,
    args:{
        id:{
            type:GraphQLInt
        },
        withDetails:{
            type:GraphQLBoolean
        }
    },
   async  resolve(root,args){
        let vehiculo= await models.vehiculo.query().findById(args.id)

        if (args.withDetails) return vehiculo.$query().withGraphFetched('[modelo,tipo_combustible,tipo_motor,cilindro,tipo_transmision,tipo_vehiculo,tipo_train]')

        return vehiculo
    }

}