import models from '../../../models'
import Vehiculo from '../../types/vehiculo'
import { GraphQLString, GraphQLBoolean,GraphQLList } from 'graphql'


export default {
    type: new GraphQLList(Vehiculo),
    args: {
        param: {
            type: GraphQLString
        },
        withDetails: {
            type: GraphQLBoolean
        }
    },
    async resolve(root, args) {
        if (args.param == undefined && args.withDetails)
        return await models.vehiculo.query()
        .withGraphFetched('[modelo]')
        //.withGraphFetched('[modelo,tipo_combustible,tipo_motor,cilindro,tipo_transmision,tipo_vehiculo,tipo_train]');

        if (args.param == undefined)
            return await models.vehiculo.query()
            //.withGraphFetched('[modelo]')

        if (args.withDetails)
            return await models.vehiculo.query().joinRelated('[modelo.[marca],tipo_combustible,tipo_motor,cilindro,tipo_transmision,tipo_vehiculo,tipo_train]', {
                aliases: {
                    modelo: 'm',
                    marca:'ma',
                    tipo_combustible: 'tc',
                    tipo_motor: 'tm',
                    cilindro: 'c',
                    tipo_transmision: 'tt',
                    tipo_vehiculo: 'tv',
                    tipo_train: 'ttr'

                }
            }).whereRaw('m.descripcion  like :term OR `m:ma`.descripcion like :term OR tc.descripcion  like :term OR tm.descripcion  like :term OR c.descripcion  like :term OR  tt.descripcion  like :term OR tv.descripcion  like :term OR ttr.descripcion  like :term'
                , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' })
                //.withGraphFetched('[modelo,tipo_combustible,tipo_motor,cilindro,tipo_transmision,tipo_vehiculo,tipo_train]');


        return await models.vehiculo.query().joinRelated('[modelo]', {
            aliases: {
                modelo: 'm',
            }
        }).whereRaw(`m.descripcion  like :term`
            , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' })
          //  .withGraphFetched('[modelo]');



    }

}