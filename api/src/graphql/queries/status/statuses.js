import { GraphQLList, GraphQLString } from "graphql";


import Status from "../../types/status";
import models from '../../../models';
export default {
  type: new GraphQLList(Status),
  args: {
    param: {
      type: GraphQLString
    }
  },
 async resolve(root, args) {
    if(args.param==undefined)
      return await models.status.query();
      
    return await models.status.query().whereRaw('CONVERT(id,CHAR(50)) LIKE :term or descripcion  like :term'
      , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' });
  }
};
