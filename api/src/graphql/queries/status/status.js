import models from '../../../models'
import Status from '../../types/status'
import { GraphQLInt } from 'graphql';

export default {
    type: Status,
    args: {
      id: {
        type: GraphQLInt
      }
    },
    async resolve(root, args) {
     return await models.status.query().findById(args.id);
      // return models.almacen.findByPk(args.id);
    }
  };