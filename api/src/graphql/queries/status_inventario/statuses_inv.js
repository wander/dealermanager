import { GraphQLList, GraphQLString } from "graphql";


import Status_Inv from "../../types/status_inventario";
import models from '../../../models';
export default {
  type: new GraphQLList(Status_Inv),
  args: {
    param: {
      type: GraphQLString
    }
  },
  async resolve(root, args) {
    if(args.param==undefined)
      return await models.status_inventario.query();
      
    return await models.status_inventario.query().whereRaw('CONVERT(id,CHAR(50)) LIKE :term or descripcion  like :term'
      , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' });
  }
};
