import models from '../../../models'
import Status_Inv from '../../types/status_inventario'
import { GraphQLInt } from 'graphql';

export default {
    type: Status_Inv,
    args: {
      id: {
        type: GraphQLInt
      }
    },
    async resolve(root, args) {
     return await models.status_inventario.query().findById(args.id);
      // return models.almacen.findByPk(args.id);
    }
  };