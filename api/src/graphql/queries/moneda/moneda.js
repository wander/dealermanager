import models from '../../../models'
import Moneda from '../../types/moneda'
import { GraphQLInt } from 'graphql';

export default {
    type: Moneda,
    args: {
      id: {
        type: GraphQLInt
      }
    },
   async  resolve(root, args) {
     return await models.moneda.query().findById(args.id);
      // return models.almacen.findByPk(args.id);
    }
  };