import models from '../../../models'
import Moneda from '../../types/moneda'
import { GraphQLString,GraphQLList } from 'graphql';

export default {
    type: new GraphQLList(Moneda),
    args: {
        param: {
            type: GraphQLString
          },
    },
    async resolve(root, args) {
        if(args.param==undefined)
          return await models.moneda.query();
          
        return await models.moneda.query().whereRaw('CONVERT(id,CHAR(50)) LIKE :term or descripcion  like :term'
          , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' });
      }
  };