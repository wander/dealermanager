import models from '../../../models'
import Gasto from '../../types/gasto'
import { GraphQLInt } from 'graphql';

export default {
    type: Gasto,
    args: {
      id: {
        type: GraphQLInt
      }
    },
    async resolve(root, args) {
     return await models.gasto.query().findById(args.id).withGraphFetched('[tipo_gasto,inventario,moneda,tasa_cambio]');
      // return models.almacen.findByPk(args.id);
    }
  };