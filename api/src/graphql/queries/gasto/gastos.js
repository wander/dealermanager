import { GraphQLList, GraphQLString, GraphQLBoolean, GraphQLInt } from "graphql";


import Gasto from "../../types/gasto";
import models from '../../../models';
export default {
  type: new GraphQLList(Gasto),
  args: {
    param: {
      type: GraphQLString
    },   
    inventarioid: {
      type: GraphQLInt
    },
    withDetails: {
      type: GraphQLBoolean
    }
  },
  async resolve(root, args) {
    var gastos
    
    if (args.param == undefined || args.param == '') gastos = await models.gasto.query().where('inventarioid',args.inventarioid).withGraphFetched('[tipo_gasto,moneda,tasa_cambio,ubicacion]');

    else gastos = await models.gasto.query().alias('g').joinRelated('[tipo_gasto]', {
      aliases: {
        tipo_gasto: 'tg'
      }
    })
      .whereRaw('g.descripcion LIKE :term or tg.descripcion  like :term'
        , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' }).andWhere('inventarioid', args.inventarioid).withGraphFetched('[tipo_gasto,moneda,tasa_cambio,ubicacion]');


    if (args.withDetails) return await gastos.$query().withGraphFetched('[tipo_gasto,inventario,moneda,tasa_cambio,ubicacion]');

    return gastos



  }

};
