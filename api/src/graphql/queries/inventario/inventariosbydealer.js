import { GraphQLList, GraphQLString, GraphQLBoolean, GraphQLInt } from "graphql";


import Inventario from "../../types/inventario";
import models from '../../../models';
export default {
  type: new GraphQLList(Inventario),
  args: {
    param: {
      type: GraphQLString
    },
    withDetails: {
      type: GraphQLBoolean
    }
  },
  async resolve(root, args, ctx) {

    let dealer_id= await ctx.user.then(user => {
      return user.sucursales[0].dealer_id
    })

    if (args.param == undefined)
      return await models.inventario.query().joinRelated('sucursal.[dealer]',{
        aliases: {
        dealer: 'd',
        sucursal: 's'
        }
      }).whereRaw("`s:d`.id=:dealerid",{ dealerid:dealer_id } ).withGraphFetched('[status_inventario,vehiculo,color]');

    if (args.withDetails)
      return await models.inventario.query().alias('i').joinRelated('vehiculo.[modelo.[marca]],sucursal.[dealer]]', {
        aliases: {
          vehiculo: 'v',
          modelo: 'm',
          marca: 'ma',
          dealer: 'd',
          sucursal: 's'
        }
      })
        .whereRaw("`s:d`.id=:dealerid `v:m`.descripcion  like :term OR `v:m:ma`.descripcion like :term OR MATCH(`i`.`codigosubasta`,`i`.`vin`,`i`.`descripcion`,`i`.`year`) AGAINST(concat(:param,'*') in BOOLEAN MODE)"
          , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%', param: args.param },
          { dealerid: dealer_id }).withGraphFetched('[status_inventario,vehiculo,sucursal,color,gastos]');


    return await models.inventario.query().alias('i').joinRelated('vehiculo.[modelo.[marca]],sucursal.[dealer]', {
      aliases: {
        vehiculo: 'v',
        modelo: 'm',
        marca: 'ma',
        dealer: 'd',
        sucursal: 's'
      }
    })
      .whereRaw("`s:d`.id=:dealerid  `v:m`.descripcion  like :term OR `v:m:ma`.descripcion like :term OR MATCH(`i`.`codigosubasta`,`i`.`vin`,`i`.`descripcion`,`i`.`year`) AGAINST(concat(:param,'*') in BOOLEAN MODE)"
        , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%', param: args.param }, { dealerid: dealer_id })
      .withGraphFetched('[status_inventario,vehiculo,sucursal,color]');
  }

};
