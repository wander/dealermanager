import { GraphQLList, GraphQLString, GraphQLBoolean } from "graphql";


import Inventario from "../../types/inventario";
import models from '../../../models';
export default {
  type: new GraphQLList(Inventario),
  args: {
    param: {
      type: GraphQLString
    },
    withDetails: {
      type: GraphQLBoolean
    }
  },
  async resolve(root, args) {
    if (args.param == undefined)
      return await models.inventario.query()
      //.withGraphFetched('[status_inventario,vehiculo,sucursal,color]');

    if (args.withDetails)
      return await models.inventario.query().alias('i').joinRelated('vehiculo.[modelo.[marca]]', {
        aliases: {
          vehiculo: 'v',
          modelo: 'm',
          marca: 'ma'

        }
      })
        .whereRaw("`v:m`.descripcion  like :term OR `v:m:ma`.descripcion like :term OR MATCH(`i`.`codigosubasta`,`i`.`vin`,`i`.`descripcion`,`i`.`year`) AGAINST(concat(:param,'*') in BOOLEAN MODE)"
          , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%', param: args.param })
          //.withGraphFetched('[status_inventario,vehiculo,sucursal,color,gastos]');


    return await models.inventario.query().alias('i').joinRelated('vehiculo.[modelo.[marca]]', {
      aliases: {
        vehiculo: 'v',
        modelo: 'm',
        marca: 'ma'

      }
    })
      .whereRaw("`v:m`.descripcion  like :term OR `v:m:ma`.descripcion like :term    OR MATCH(`i`.`codigosubasta`,`i`.`vin`,`i`.`descripcion`,`i`.`year`) AGAINST(concat(:param,'*') in BOOLEAN MODE)"
        , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%', param: args.param })
        //.withGraphFetched('[status_inventario,vehiculo,sucursal,color]');
  }

};
