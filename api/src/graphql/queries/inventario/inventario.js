import models from '../../../models'
import Inventario from '../../types/inventario'
import { GraphQLInt } from 'graphql';

export default {
    type: Inventario,
    args: {
      id: {
        type: GraphQLInt
      }
    },
    async resolve(root, args) {
     return await models.inventario.query().findById(args.id).withGraphFetched('[status_inventario,vehiculo,sucursal,color,gastos]');
      // return models.almacen.findByPk(args.id);
    }
  };