
let dealersIds = async(ctx) => {
    const user_sucursales = await ctx.user.then(user => {
        return user.sucursales
    })
    let dealerids = []

    if (user_sucursales) {
        //verificar si es un array o un objecto en caso de que solo tenga una sucursal
        if (user_sucursales.length === 1)
            dealerids.push(user_sucursales[0].dealer_id)
        else
            _.forEach(user_sucursales, function (sucursal) {
                dealerids.push(sucursal.dealer_id)
            })
        return dealerids
    }
}

let sucursalesIds = async (ctx) => {
    const user_sucursales = await ctx.user.then(user => {
        return user.sucursales
    })
    let sucursalesids = []

    if (user_sucursales) {
        //verificar si es un array o un objecto en caso de que solo tenga una sucursal
        if (user_sucursales.length === 1)
            sucursalesids.push(user_sucursales[0].id)
        else
            _.forEach(user_sucursales, function (sucursal) {
                sucursalesids.push(sucursal.id)
            })
        return sucursalesids
    }
}



module.exports= {
    dealersIds,
    sucursalesIds
}



