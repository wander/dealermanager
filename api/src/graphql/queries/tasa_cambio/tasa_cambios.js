import models from '../../../models'
import TasaCambio from '../../types/tasa_cambio'
import { GraphQLString,GraphQLList } from 'graphql';

export default {
    type: new GraphQLList(TasaCambio),
    args: {
        param: {
            type: GraphQLString
          },
    },
    async resolve(root, args) {
        if(args.param==undefined)
          return await models.tasa_cambio.query();
          
        return await models.tasa_cambio.query().whereRaw('CONVERT(id,CHAR(50)) LIKE :term or descripcion  like :term'
          , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' });
      }
  };