import models from '../../../models'
import TasaCambio from '../../types/tasa_cambio'
import { GraphQLInt } from 'graphql';

export default {
    type: TasaCambio,
    args: {
      id: {
        type: GraphQLInt
      }
    },
    async resolve(root, args) {
     return await models.tasa_cambio.query().findById(args.id).withGraphFetched('[moneda_alterna,moneda_predeterminada]');
      // return models.almacen.findByPk(args.id);
    }
  };