import models from '../../../models'
import Cilindro from '../../types/cilindro'
import { GraphQLInt } from 'graphql';

export default {
    type: Cilindro,
    args: {
      id: {
        type: GraphQLInt
      }
    },
    async resolve(root, args,context) {
     return await models.cilindro.query().findById(args.id);
      // return models.almacen.findByPk(args.id);
    }
  };