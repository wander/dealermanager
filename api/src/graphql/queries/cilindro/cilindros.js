import { GraphQLList, GraphQLString } from "graphql";


import Cilindro from "../../types/cilindro";
import models from '../../../models';
export default {
  type: new GraphQLList(Cilindro),
  args: {
    param: {
      type: GraphQLString
    }
  },
async resolve(root, args) {
    if(args.param==undefined)
      return await models.cilindro.query();
      
    return await models.cilindro.query().whereRaw('CONVERT(id,CHAR(50)) LIKE :term or descripcion  like :term'
      , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' });
  }
};
