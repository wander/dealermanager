import { GraphQLInt, GraphQLBoolean } from "graphql";

//import models from "../../../models";
import Dealer from "../../types/dealer";
import models from '../../../models';
export default {
  type: Dealer,
  args: {
    id: {
      type: GraphQLInt
    },
    withDetails: {
      type: GraphQLBoolean
    }
  },
  async resolve(root, args) {
    var dealer = await models.dealer.query().findById(args.id);
    if (args.withDetails) return await dealer.$query().withGraphFetched('[sucursales]')
    return dealer;


  }
};
