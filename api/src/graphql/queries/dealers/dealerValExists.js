import { GraphQLList, GraphQLString } from "graphql";

import Dealer from "../../types/dealer";
import models from '../../../models';
export default {
  type: new GraphQLList(Dealer),
  args: {
    param: {
      type: GraphQLString,
    },
    field: {
      type: GraphQLString
    }
  },
  async resolve(root, args) {
    return await models.dealer.query().where(args.field, '=', args.param)
  }
};
