import { GraphQLList, GraphQLInt, GraphQLString, GraphQLBoolean } from "graphql";

//import models from "../../../models";
import Dealer from "../../types/dealer";
import models from '../../../models';


export default {
  type: new GraphQLList(Dealer),
  args: {

    param: {
      type: GraphQLString
    },
    withDetails: {
      type: GraphQLBoolean

    }
  },
  async resolve(root, args) {
    if (args.param === undefined || args.param === '')
      return await models.dealer.query();

      
    if (args.withDetails) return await models.dealer.query().whereRaw('nombre like :term or rnc like :term or CONVERT(id, CHAR(50)) LIKE :term',
    { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' }).withGraphFetched('[sucursales]')

   return  await models.dealer.query()
    .whereRaw('nombre like :term or rnc like :term or CONVERT(id, CHAR(50)) LIKE :term',
      { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' })


  
  }
}
