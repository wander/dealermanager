import { GraphQLList, GraphQLInt, GraphQLString, GraphQLBoolean } from "graphql";

//import models from "../../../models";
import Dealer from "../../types/dealer";
import models from '../../../models';
import dealer from "../../types/dealer";


export default {
  type: new GraphQLList(Dealer),
  args: {

    param: {
      type: GraphQLString
    },
    withDetails: {
      type: GraphQLBoolean

    }
  },
  async resolve(root, args, ctx) {

    const user_sucursales = await ctx.user.then(user => {
      return user.sucursales
    })
    let dealerids = []

    if (user_sucursales) {
      //verificar si es un array o un objecto en caso de que solo tenga una sucursal
      if (user_sucursales.length === 1) 
        dealerids.push(user_sucursales[0].dealer_id)
      else
      _.forEach(user_sucursales, function (sucursal) {
        dealerids.push(sucursal.dealer_id)
      })


    }

    if (args.param == undefined || args.param == '')
      return await models.dealer.query().whereIn('id',dealerids);

    var dealers = await models.dealer.query().whereRaw('nombre like :term or rnc like :term or CONVERT(id, CHAR(50)) LIKE :term',
      { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' }).whereIn('id', dealerids)

    if (args.withDetails) return dealers.$query().withGraphFetched('[sucursales]')

    return dealers;
  }
}
