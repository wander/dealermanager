import { GraphQLInt, GraphQLBoolean } from "graphql";

//import models from "../../../models";
import Sucursal from "../../types/sucursal";
import models from '../../../models';
export default {
  type: Sucursal,
  args: {
    id: {
      type: GraphQLInt
    },
    withDealer:{
      type:GraphQLBoolean
    }
  },
  async resolve(root, args) {
    var sucursal= await models.sucursal.query().findById(args.id);
    if(args.withDealer) return await sucursal.$query().withGraphFetched('[dealer]')
    return sucursal;
  }
};
