import { GraphQLList, GraphQLInt, GraphQLString, GraphQLBoolean } from "graphql";

//import models from "../../../models";
import Sucursal from "../../types/sucursal";
import models from '../../../models';

export default {
  type: new GraphQLList(Sucursal),
  args: {
  
    param: {
      type: GraphQLString
    }
  },
  async resolve(root, args) {
    var sucursales;
    if (args.param == undefined || args.param=='') 
    sucursales=await models.sucursal.query().withGraphFetched('[dealer]');

    else sucursales= await models.sucursal.query()
    .whereRaw("MATCH(descripcion,direccion,telefonos) AGAINST(concat(:param,' * ') in BOOLEAN MODE)",
    { param: args.param });

   // if(args.withDealer) return await sucursales.$query()
    
    return sucursales;
  }
};
