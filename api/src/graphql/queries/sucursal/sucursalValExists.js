import { GraphQLList, GraphQLString } from "graphql";

import Sucursal from "../../types/sucursal";
import models from '../../../models';
export default {
  type: new GraphQLList(Sucursal),
  args: {
    param: {
      type: GraphQLString,
    },
    field:{
        type:GraphQLString
    }
  },
 async  resolve(root, args) {
    return await models.sucursal.query().where(args.field, '=', args.param)
   }
};
