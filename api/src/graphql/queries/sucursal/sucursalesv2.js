import { GraphQLList, GraphQLInt, GraphQLString, GraphQLBoolean } from "graphql";

//import models from "../../../models";
import Sucursal from "../../types/sucursal";
import models from '../../../models';

export default {
  type: new GraphQLList(Sucursal),
  args: {

    param: {
      type: GraphQLString
    }
  },
  async resolve(root, args) {
    var sucursales;

    const user_sucursales = await ctx.user.then(user => {
      return user.sucursales
    })
    let dealerids = []

    if (user_sucursales) {
      //verificar si es un array o un objecto en caso de que solo tenga una sucursal
      if (user_sucursales.length === 1)
        dealerids.push(user_sucursales[0].dealer_id)
      else
        _.forEach(user_sucursales, function (sucursal) {
          if (dealerids.indexOf(sucursal.dealer_id) < 0)
            dealerids.push(sucursal.dealer_id)
        })


    }
    if (args.param == undefined || args.param == '')
      sucursales = await models.sucursal.query().whereIn('dealer_id', dealerids).withGraphFetched('[dealer]');

    else sucursales = await models.sucursal.query()
      .whereIn('dealer_id', dealerids)
      .andWhereRaw("MATCH(descripcion,direccion,telefonos) AGAINST(concat(:param,' * ') in BOOLEAN MODE)",
        { param: args.param }).withGraphFetched('[dealer]')



    return sucursales;
  }
};
