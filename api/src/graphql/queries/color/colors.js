import { GraphQLList, GraphQLString } from "graphql";


import Color from "../../types/color";
import models from '../../../models';
export default {
  type: new GraphQLList(Color),
  args: {
    param: {
      type: GraphQLString
    }
  },
  async resolve(root, args) {
    if(args.param==undefined)
      return await models.color.query();
      
    return await models.color.query().whereRaw('CONVERT(id,CHAR(50)) LIKE :term or descripcion  like :term'
      , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' });
  }
};
