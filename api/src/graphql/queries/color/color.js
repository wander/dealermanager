import models from '../../../models'
import Color from '../../types/color'
import { GraphQLInt } from 'graphql';

export default {
    type: Color,
    args: {
      id: {
        type: GraphQLInt
      }
    },
    async resolve(root, args) {
     return  await models.color.query().findById(args.id);
      // return models.almacen.findByPk(args.id);
    }
  };