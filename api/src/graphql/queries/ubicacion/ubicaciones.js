import { GraphQLList, GraphQLString } from "graphql";


import Ubicacion from "../../types/ubicacion";
import models from '../../../models';
export default {
  type: new GraphQLList(Ubicacion),
  args: {
    param: {
      type: GraphQLString
    }
  },
async resolve(root, args) {
    if(args.param==undefined)
      return await models.ubicacion.query();
      
    return await models.ubicacion.query().whereRaw('CONVERT(id,CHAR(50)) LIKE :term or descripcion  like :term'
      , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' });
  }
};
