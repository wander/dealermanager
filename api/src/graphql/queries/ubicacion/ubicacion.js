import models from '../../../models'
import Ubicacion from '../../types/ubicacion'
import { GraphQLInt } from 'graphql';

export default {
    type: Ubicacion,
    args: {
      id: {
        type: GraphQLInt
      }
    },
    async resolve(root, args) {
     return await models.ubicacion.query().findById(args.id);
      // return models.almacen.findByPk(args.id);
    }
  };