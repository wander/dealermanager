import { GraphQLList, GraphQLString } from "graphql";


import Tipo_Gasto from '../../types/tipo_gasto'
import models from '../../../models';
export default {
  type: new GraphQLList(Tipo_Gasto),
  args: {
    param: {
      type: GraphQLString
    }
  },
  async resolve(root, args) {
    if(args.param==undefined)
      return await models.tipo_gasto.query();
      
    return await models.tipo_gasto.query().whereRaw('CONVERT(id,CHAR(50)) LIKE :term or descripcion  like :term'
      , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' });
  }
};
