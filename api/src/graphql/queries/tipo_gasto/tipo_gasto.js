import models from '../../../models'
import Tipo_Gasto from '../../types/tipo_gasto'
import { GraphQLInt } from 'graphql';

export default {
    type: Tipo_Gasto,
    args: {
      id: {
        type: GraphQLInt
      }
    },
   async  resolve(root, args) {
     return await models.tipo_gasto.query().findById(args.id);
      // return models.almacen.findByPk(args.id);
    }
  };