import models from '../../../models'
import Tipo_Train from '../../types/tipo_train'
import { GraphQLInt } from 'graphql';

export default {
    type: Tipo_Train,
    args: {
      id: {
        type: GraphQLInt
      }
    },
    async resolve(root, args) {
     return await models.tipo_train.query().findById(args.id);
      // return models.almacen.findByPk(args.id);
    }
  };