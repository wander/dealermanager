import { GraphQLList, GraphQLString } from "graphql";


import Tipo_Train from '../../types/tipo_train'
import models from '../../../models';
export default {
  type: new GraphQLList(Tipo_Train),
  args: {
    param: {
      type: GraphQLString
    }
  },
  async resolve(root, args) {
    if(args.param==undefined)
      return await models.tipo_train.query();
      
    return await models.tipo_train.query().whereRaw('CONVERT(id,CHAR(50)) LIKE :term or descripcion  like :term'
      , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' });
  }
};
