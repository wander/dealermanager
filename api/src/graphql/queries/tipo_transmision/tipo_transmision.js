import models from '../../../models'
import Tipo_Trans from '../../types/tipo_transmision'
import { GraphQLInt } from 'graphql';

export default {
    type: Tipo_Trans,
    args: {
      id: {
        type: GraphQLInt
      }
    },
    async resolve(root, args) {
     return await models.tipo_transmision.query().findById(args.id);
      // return models.almacen.findByPk(args.id);
    }
  };