import { GraphQLList, GraphQLString } from "graphql";

import Tipo_Trans from '../../types/tipo_transmision'
import models from '../../../models';
export default {
  type: new GraphQLList(Tipo_Trans),
  args: {
    param: {
      type: GraphQLString
    }
  },
  async resolve(root, args) {
    if(args.param==undefined)
      return await models.tipo_transmision.query();
      
    return await models.tipo_transmision.query().whereRaw('CONVERT(id,CHAR(50)) LIKE :term or descripcion  like :term'
      , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' });
  }
};
