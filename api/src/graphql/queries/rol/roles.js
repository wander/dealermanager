import models from '../../../models'
import { GraphQLString, GraphQLList } from 'graphql';
import { Role } from '../../../constants/role';
import Rol from '../../types/rol'
export default {
  type: GraphQLList(Rol),
  args: {
    param: {
      type: GraphQLString
    },
  },
  async resolve(root,args,ctx) {

    let isSuperAdmin = await ctx.user.then(user => {
      const isSuperAdmin = user.roles.some((rolitem) => {
        return rolitem.descripcion === Role.SuperAdmin
      })
      return isSuperAdmin
    })
    let queryresult;

    if (isSuperAdmin) {
      queryresult = args.param === undefined ? await models.rol.query() :
        await models.rol.query()
        .whereRaw('CONVERT(id,CHAR(50)) LIKE :term or descripcion  like :term'
          , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' });
      return queryresult
    }
    queryresult = args.param === undefined ? 
    await models.rol.query()
    .whereNotIn('descripcion', Role.SuperAdmin) :

    await models.rol.query()
        .whereNotIn('descripcion', Role.SuperAdmin)
        .whereRaw('CONVERT(id,CHAR(50)) LIKE :term or descripcion  like :term'
          , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' });

    return queryresult

  }



};