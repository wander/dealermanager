import models from '../../../models'
import Rol from '../../types/rol'
import { GraphQLInt } from 'graphql';

export default {
    type: Rol,
    args: {
      id: {
        type: GraphQLInt
      }
    },
   async  resolve(root, args) {
     return await models.rol.query().findById(args.id);
      // return models.almacen.findByPk(args.id);
    }
  };