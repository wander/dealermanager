import cilindro from './cilindro/cilindro'
import cilindros from './cilindro/cilindros'

import color from './color/color'
import colors from './color/colors'

import dealer from './dealers/dealer'
import dealers from './dealers/dealers'
import dealerValExists from './dealers/dealerValExists'
import dealersByUsuario from './dealers/dealersbyusuario'

import marcas from './marca/marcas'
import marca from './marca/marca'

import modelo from './modelo/modelo'
import modelos from './modelo/modelos'

import status from './status/status'
import statuses from './status/statuses'

import status_inv from './status_inventario/status_inv'
import statuses_inv from './status_inventario/statuses_inv'

import tipo_comb from './tipo_combustible/tipo_combustible'
import tipo_combs from './tipo_combustible/tipo_combustibles'

import tipo_gasto from './tipo_gasto/tipo_gasto'
import tipo_gastos from './tipo_gasto/tipo_gastos'

import tipo_motor from './tipo_motor/tipo_motor'
import tipo_motores from './tipo_motor/tipo_motores'

import tipo_train from './tipo_train/tipo_train'
import tipo_trains from './tipo_train/tipo_trains'

import tipo_tran from './tipo_transmision/tipo_transmision'
import tipo_trans from './tipo_transmision/tipo_transmisiones'

import tipo_vehiculo from './tipo_vehiculo/tipo_vehiculo'
import tipo_vehiculos from './tipo_vehiculo/tipo_vehiculos'

import gasto from './gasto/gasto'
import gastos from './gasto/gastos'

import inventario from './inventario/inventario'
import inventarios from './inventario/inventarios'
import inventariosByDealer from './inventario/inventariosbydealer'

import sucursal from './sucursal/sucursal'
import sucursales from './sucursal/sucursales'
import sucursalValExists from './sucursal/sucursalValExists'
import sucursalesv2 from './sucursal/sucursalesv2'

import vehiculo from './vehiculo/vehiculo'
import vehiculos from './vehiculo/vehiculos'

import ubicacion from './ubicacion/ubicacion'
import ubicaciones from './ubicacion/ubicaciones'

import moneda from './moneda/moneda'
import monedas from './moneda/monedas'

import tasa_cambio from './tasa_cambio/tasa_cambio'
import tasa_cambios from './tasa_cambio/tasa_cambios'


import rol from './rol/rol'
import roles from './rol/roles'

import usuario from './usuario/usuario'
import usuarios from './usuario/usuarios'
import usuariosbyDealer from './usuario/usuariosbyDealer'
import validateClave from './usuario/validateClave'
import validateUserName from './usuario/validateUserName'

import  ventas from './venta/ventas'
import venta from './venta/venta'

export default{
    cilindro,
    cilindros,

    color,
    colors,

    dealer,
    dealers,
    dealerValExists,
    dealersByUsuario,

    marca,
    marcas,

    modelo,
    modelos,

    status,
    statuses,

    status_inv,
    statuses_inv,

    tipo_comb,
    tipo_combs,

    tipo_gasto,
    tipo_gastos,

    tipo_motor,
    tipo_motores,

    tipo_train,
    tipo_trains,

    tipo_tran,
    tipo_trans,

    tipo_vehiculo,
    tipo_vehiculos,

    gasto,
    gastos,

    inventario,
    inventarios,
    inventariosByDealer,

    sucursal,
    sucursales,
    sucursalValExists,
    sucursalesv2,

    vehiculo,
    vehiculos,

    ubicacion,
    ubicaciones,

    moneda,
    monedas,

    tasa_cambio,
    tasa_cambios,

    roles,
    rol,

    usuario,
    usuarios,
    usuariosbyDealer,
    validateClave,
    validateUserName,

    venta,
    ventas

};