import models from '../../../models'
import Tipo_Com from '../../types/tipo_combustible'
import { GraphQLInt } from 'graphql';

export default {
    type: Tipo_Com,
    args: {
      id: {
        type: GraphQLInt
      }
    },
    async resolve(root, args) {
     return await models.tipo_combustible.query().findById(args.id);
      // return models.almacen.findByPk(args.id);
    }
  };