import { GraphQLList, GraphQLString } from "graphql";


import Tipo_Com from '../../types/tipo_combustible'
import models from '../../../models';
export default {
  type: new GraphQLList(Tipo_Com),
  args: {
    param: {
      type: GraphQLString
    }
  },
  async resolve(root, args) {
    if(args.param==undefined)
      return await models.tipo_combustible.query();
      
    return await models.tipo_combustible.query().whereRaw('CONVERT(id,CHAR(50)) LIKE :term or descripcion  like :term'
      , { term: (args.param !== undefined) ? '%' + args.param + '%' : '%%' });
  }
};
