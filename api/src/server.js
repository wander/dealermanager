//import Express from "express";
const express = require('express');
//import { graphqlHTTP } from "express-graphql";
//const { GraphQLServer } = require('graphql-yoga') 

import schema from "./graphql";
//const {schema,permissions}=require('./graphql')
import jwt from "express-jwt";
import jsontoken from "jsonwebtoken"
//import cors from 'cors';
require("dotenv").config();
const { Model } = require('objection');
const knex = require('./knex/knex')
import { AuthenticationError } from "apollo-server-core"
//const APP_PORT = 3000;
const app = express();
const { ApolloServer} = require('apollo-server-express')
//const knex=Knex(knexConfig.development);
knex.on('query', console.log);

Model.knex(knex);


const hostname = process.env.HOST;
const port = process.env.PORT || 3000;

//auth middleware
const auth = jwt({
  secret: process.env.JWT_SECRET,
  credentialsRequired: false,
  algorithms: ['HS256']
});


// const authenticate = async (resolve, root, args, context, info) => {
//   let token;
//   try {
//     token = jsontoken.verify(context.req.get("Authorization"), process.env.JWT_SECRET);
//   } catch (e) {
//     return new AuthenticationError("Not authorized!!!");
//   }
//   const result = await resolve(root, args, context, info);
//   return result;
// };


// const options = {
//   port: port,
//   endpoint: '/graphql',
//   subscriptions: '/subscriptions',
//   playground: '/playground',
// }

const getUser= async (req) =>{
  let token;
  let finaltoken;
  try {
    if (req.query && req.query.hasOwnProperty("access_token")) {
      token = req.query.access_token;
    } else if (
      req.headers.authorization &&
      req.headers.authorization.includes("Bearer")
    ) {
      token = req.headers.authorization.split(" ")[1];
    }
    finaltoken = jsontoken.verify(token, process.env.JWT_SECRET);
  } catch (e) {
    return null;
  }
  return finaltoken;
}


// The GraphQL endpoint
const server = new ApolloServer({
  
  schema:schema,
  context: (req) => ({
    req: req.req,
    user: getUser(req.req)
  }),
})
const path='/graphql'
server.applyMiddleware({app,path})
app.listen(port, () => {
  console.log(`App listening on port ${port}...`);
});






/* 
app.use('/graphql', bodyParser.json(), graphqlExpress({
  schema: schema,
  context: (req) => ({
    req: req.request,
    user: getUser(req.request)
  }),
}));
app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }));
app.listen(APP_PORT, () => {
  console.log(`App listening on port ${APP_PORT}...`);
}); */
// GraphiQL, a visual editor for queries
/*

const server = new GraphQLServer({
  schema:schema,
    context: (req) => ({
      req:req.request,
      user:getUser(req.request)
    }),


})



server.start(options, ({ port }) =>
  console.log(
    `Server started, listening on port ${port} for incoming requests.`,
  ),
)*/

//app.use(auth)

/* app.use(
  "/graphql",
  bodyParser.json(),
  GraphHTTP({
    schema: Schema,
    models:models,
    pretty: true,
    graphiql: true,
    postsLoader: new Dataloader(keys=>batchPosts(keys,models))
  })
); */

/* function getUser(context) {
  const auth = context.get('Authorization')
  if (users[auth]) {
    return users[auth]
  } else {
    return null
  }
}
app.use(cors())

app.use(auth)

app.use("/graphql", graphqlHTTP(request => ({
      schema: Schema,
      context: {
        user: request.user,
        req: request,
        //postLoader= new Dataloader(keys=>batchPosts(keys,models)),
      },

      pretty: true,
      graphiql: true,
      customFormatErrorFn: (error) => ({
        message: error.message,
        locations: error.locations,
        stack: error.stack ? error.stack.split('\n') : [],
        path: error.path,
      })
    })
  )
);

app.listen(APP_PORT, () => {
  console.log(`App listening on port ${APP_PORT}...`);
}); */
