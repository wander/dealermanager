export const StatusInv={
    Comprado:'COMPRADO',
    EnGrua:'EN GRUA',
    RecibidoTaller:'RECIBIDO TALLER',
    Embaracado:'EMBARCADO',
    RecibidoMuelle:'RECIBIDO MUELLE',
    RecibidoDealer:'RECIBIDO DEALER',
    Disponible:'DISPONIBLE',
    Vendido:'VENDIDO'
}