
exports.up = function(knex) {
    return knex.schema.createTable('sucursal',function(table){
    table.increments();
    table.string('descripcion',255).notNullable();
    table.string('direccion',400);
    table.string('telefonos');
    table.integer('dealerid').notNullable();
    table.integer('active')
    table.datetime('createdat',{precision:6}).defaultTo(knex.fn.now(6));
    table.datetime('updatedat',{precision:6}).defaultTo(knex.fn.now(6));
})}

exports.down = function(knex) {
  
};
