
exports.up = function(knex) {
    return knex.schema.table('gasto', function (table) {
        table.integer('tipogastoid').unsigned().alter()
        table.integer('inventarioid').unsigned().alter()
        table.integer('monedaid').unsigned().alter()
        table.foreign('tipogastoid').references('id').inTable('tipo_gasto');
        table.foreign('inventarioid').references('id').inTable('inventario');
        table.foreign('monedaid').references('id').inTable('moneda');
      });
};

exports.down = function(knex) {
  
};
