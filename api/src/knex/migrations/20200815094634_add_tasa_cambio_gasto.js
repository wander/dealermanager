
exports.up = function (knex) {
    return knex.schema.table('tasa_cambio', function (table) {
        table.increments();
    }).table('gasto', function (table) {
        table.integer('tasacambioid').unsigned().notNullable();

        table.foreign('tasacambioid').references('id').inTable('tasa_cambio');

    });
};

exports.down = function (knex) {

};
