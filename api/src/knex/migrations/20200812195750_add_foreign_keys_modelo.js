
exports.up = function(knex) {
  
    return knex.schema.table('modelo', function (table) {
        table.integer('marcaid').unsigned().notNullable().alter();

        table.foreign('marcaid').references('id').inTable('marca');
  
      });
};
exports.down = function(knex) {
  
};
