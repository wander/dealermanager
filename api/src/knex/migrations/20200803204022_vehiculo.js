
exports.up = function(knex) {
    return knex.schema.createTable('vehiculo',function(table){
    table.increments();
    table.integer('modeloid');
    table.integer('tipocombustibleid');
    table.integer('tipotransaccionid');
    table.integer('tipotransmisionid');
    table.integer('tipomotorid');
    table.integer('cilindroid');
    table.datetime('createdat',{precision:6}).defaultTo(knex.fn.now(6));
    table.datetime('updatedat',{precision:6}).defaultTo(knex.fn.now(6));

})};

exports.down = function(knex) {
  
};
