
exports.up = function(knex) {
    return knex.schema.createTable('usuario_rol_mapping',function(table){

        table.integer('usuarioid').unsigned()
        table.integer('rolid').unsigned()
        table.datetime('createdat',{precision:6}).defaultTo(knex.fn.now(6));
        table.datetime('updatedat',{precision:6}).defaultTo(knex.fn.now(6));
        
    })}
    
exports.down = function(knex) {
  
};
