
exports.up = function(knex) {
    return knex.schema.createTable('usuario_sucursal_mapping',function(table){

        table.integer('usuarioid').unsigned()
        table.integer('sucursalid').unsigned()
        table.datetime('createdat',{precision:6}).defaultTo(knex.fn.now(6));
        table.datetime('updatedat',{precision:6}).defaultTo(knex.fn.now(6));
        
    })}
    
exports.down = function(knex) {
  
};
