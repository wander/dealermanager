
exports.up = function(knex) {
    return knex.schema.createTable('inventario',function(table){
        table.increments();
        table.integer('vehiculoid').notNullable();
        table.integer('statusinvid',255).notNullable();
        table.string('codigosubasta',400);
        table.string('vin',400);
        table.string('descripcion',200);
        table.datetime('createdat',{precision:6}).defaultTo(knex.fn.now(6));
        table.datetime('updatedat',{precision:6}).defaultTo(knex.fn.now(6));

    });
  
};

exports.down = function(knex) {
  
};
