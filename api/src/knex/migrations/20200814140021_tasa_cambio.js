
exports.up = function(knex) {
    return knex.schema.createTable('tasa_cambio',function(table){

        table.integer('default_moneda_id').unsigned()
        table.integer('moneda_id').unsigned()
        table.decimal('valor',18,2);
        table.datetime('createdat',{precision:6}).defaultTo(knex.fn.now(6));
        table.datetime('updatedat',{precision:6}).defaultTo(knex.fn.now(6));
        
    }).table('tasa_cambio',function(table){
        table.foreign('default_moneda_id').references('id').inTable('moneda')
        table.foreign('moneda_id').references('id').inTable('moneda')
    })};

exports.down = function(knex) {
  
};
