
exports.up = function(knex) {
    return knex.schema.createTable('moneda',function(table){
    table.increments();
    table.string('descripcion',300); 
    table.string('nomenclatura',50);
    table.boolean('predeterminada');
    table.datetime('createdat',{precision:6}).defaultTo(knex.fn.now(6));
    table.datetime('updatedat',{precision:6}).defaultTo(knex.fn.now(6));
    
})};

exports.down = function(knex) {
  
};
