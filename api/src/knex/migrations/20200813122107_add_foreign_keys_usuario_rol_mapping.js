
exports.up = function(knex) {
    return knex.schema.table('usuario_rol_mapping',function(table){

        table.foreign('usuarioid').references('id').inTable('usuario')
        table.foreign('rolid').references('id').inTable('rol')
        
    })}

exports.down = function(knex) {
  
};
