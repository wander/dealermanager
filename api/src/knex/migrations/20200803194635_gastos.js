
exports.up = function(knex) {
    return knex.schema.createTable('gasto',function(table){
        table.increments();
        table.integer('tipogastoid').notNullable();
        table.string('descripcion',255).notNullable();
        table.string('inventarioid',400).notNullable();;
        table.decimal('monto',18,2).notNullable();;
        table.integer('monedaid').notNullable();
        table.datetime('createdat',{precision:6}).defaultTo(knex.fn.now(6));
        table.datetime('updatedat',{precision:6}).defaultTo(knex.fn.now(6));

    });
};

exports.down = function(knex) {
  
};
