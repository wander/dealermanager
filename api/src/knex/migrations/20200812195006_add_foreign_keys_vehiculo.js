
exports.up = function(knex) {
    return knex.schema.table('vehiculo', function (table) {
        table.integer('modeloid').unsigned().alter()
        table.integer('tipocombustibleid').unsigned().alter()
        table.integer('tipomotorid').unsigned().alter()
        table.integer('cilindroid').unsigned().alter()
        table.integer('tipotransmisionid').unsigned().alter()

        table.foreign('modeloid').references('id').inTable('modelo');
        table.foreign('tipocombustibleid').references('id').inTable('tipo_combustible');
        table.foreign('tipotransmisionid').references('id').inTable('tipo_transmision');
        table.foreign('tipomotorid').references('id').inTable('tipo_motor');
        table.foreign('cilindroid').references('id').inTable('cilindro');
      });
};

exports.down = function(knex) {
  
};
