
exports.up = function(knex) {
    return knex.schema.createTable('dealer',function(table){
        table.increments();
        table.string('rnc',255);
        table.string('nombre',255).notNullable();
        table.string('website',400);
        table.string('pathlogo',400);
        table.string('email',200);
        table.datetime('createdat',{precision:6}).defaultTo(knex.fn.now(6));
        table.datetime('updatedat',{precision:6}).defaultTo(knex.fn.now(6));

    });
  
};

exports.down = function(knex) {
  
};
