
exports.up = function(knex) {
    return knex.schema.table('inventario', function (table) {
        table.integer('vehiculoid').unsigned().alter()
        table.integer('statusinvid').unsigned().alter()
        table.foreign('vehiculoid').references('id').inTable('vehiculo');
        table.foreign('statusinvid').references('id').inTable('status_inventario');
    })
};

exports.down = function(knex) {
  
};
