
exports.up = function(knex) {
    return knex.schema.createTable('venta',function(table){
        table.increments();
        table.string('codigofactura',255);
        table.string('cliente',255).notNullable()
        table.string('cedula',20);
        table.integer('vehiculo_id').unsigned()
        table.decimal('monto',18,2);
        table.integer('sucursal_id').unsigned()
        table.integer('moneda_id').unsigned()
        table.integer('usuario_id').unsigned()
        table.integer('tasacambio_id').unsigned()
        table.datetime('createdat',{precision:6}).defaultTo(knex.fn.now(6));
        table.datetime('updatedat',{precision:6}).defaultTo(knex.fn.now(6));

    }).table('venta',(table)=>{
        table.foreign('vehiculo_id').references('id').inTable('vehiculo')
        table.foreign('moneda_id').references('id').inTable('moneda')
        table.foreign('tasacambio_id').references('id').inTable('tasa_cambio')
        table.foreign('sucursal_id').references('id').inTable('sucursal')
        table.foreign('usuario_id').references('id').inTable('usuario')
    });
  
};

exports.down = function(knex) {
  
};
