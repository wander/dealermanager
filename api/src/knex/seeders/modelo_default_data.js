
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('modelo').del()
    .then(function () {
      // Inserts seed entries
      return knex('modelo').insert([
        {descripcion: 'CRV', marcaid:5},
        {descripcion: 'CIVIC EX',marcaid:5},
        {descripcion: 'CIVIC EXL',marcaid:5},
        {descripcion: 'ACCORD EX',marcaid:5},
        {descripcion: 'COROLLA S',marcaid:6},
        {descripcion: 'RAV4 LIMITED',marcaid:6},
      ]);
    });
};
