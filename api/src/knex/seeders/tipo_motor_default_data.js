
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('tipo_motor').del()
    .then(function () {
      // Inserts seed entries
      return knex('tipo_motor').insert([
        {descripcion: '1.3 L'},
        {descripcion: '1.5 L TURBO'},
        {descripcion: '1.8 L'},
        {descripcion: '2.0 L'},
        {descripcion: '2.4 L'},
        {descripcion: '3.0 L'},
        {descripcion: '3.3 L'},
        {descripcion: '3.6 L'},
    
      ]);
    });
};
