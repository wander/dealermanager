
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('tipo_vehiculo').del()
    .then(function () {
      // Inserts seed entries
      return knex('tipo_vehiculo').insert([
        {descripcion: 'Automovil'},
        {descripcion: 'Auto bus'},
        {descripcion: 'Jeepeta'}
      ]);
    });
};
