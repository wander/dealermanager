
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('tipo_gasto').del()
    .then(function () {
      // Inserts seed entries
      return knex('tipo_gasto').insert([
        {descripcion: 'Compra Subasta'},
        {descripcion: 'Grua'},
        {descripcion: 'Reparacion'},
        {descripcion: 'Mantenimiento'},
        {descripcion: 'Impuesto'},
      ]);
    });
};
