
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('color').del()
    .then(function () {
      // Inserts seed entries
      return knex('color').insert([
        {descripcion: 'Azul',class:'bg-primary'},
        {descripcion: 'Negro',class:'bg-dark'},
        {descripcion: 'Blanco',class:'bg-grey-1'},
        {descripcion: 'Rojo',class:'bg-red'},
        {descripcion: 'Verde',class:'bg-green'},
        {descripcion: 'Gris',class:'bg-grey'},
        {descripcion: 'Amarillo',class:'bg-yellow-7'},
        {descripcion: 'Naranja',class:'bg-orange'},
        {descripcion: 'Marron',class:'bg-brown'},
        {descripcion: 'Vino',class:'pink-10'},
      ]);
    });
};
