
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('sucursal').del()
    .then(function () {
      // Inserts seed entries
      return knex('sucursal').insert([
        {id: 1, descripcion: 'Uceta te Monta 1',direccion:'villa Juana',telefonos:'8096830606',active:1,dealer_id:1,dealerid:1},
        {id: 2, descripcion: 'Guzman AI 1',direccion:'Villa Faro',telefonos:'8092342943',active:1,dealer_id:2,dealerid:2},
      
      ]);
    });
};
