
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('vehiculo').del()
    .then(function () {
      // Inserts seed entries
      return knex('vehiculo').insert([
        {modeloid:7,tipocombustibleid:1,tipotransmisionid:1,tipomotorid:4,tipotrainid:1,cilindroid:4,tipovehiculoid:1},
        {modeloid:8,tipocombustibleid:1,tipotransmisionid:1,tipomotorid:4,tipotrainid:1,cilindroid:4,tipovehiculoid:1},
        {modeloid:10,tipocombustibleid:1,tipotransmisionid:1,tipomotorid:4,tipotrainid:1,cilindroid:4,tipovehiculoid:1},
        {modeloid:11,tipocombustibleid:1,tipotransmisionid:1,tipomotorid:4,tipotrainid:1,cilindroid:4,tipovehiculoid:1},
        {modeloid:12,tipocombustibleid:1,tipotransmisionid:1,tipomotorid:4,tipotrainid:1,cilindroid:4,tipovehiculoid:1},
      ]);
    });
};
 