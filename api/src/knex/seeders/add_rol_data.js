
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('rol').del()
    .then(function () {
      // Inserts seed entries
      return knex('rol').insert([
        { descripcion: 'Usuario'},
        { descripcion: 'Admin'},
        { descripcion: 'SuperAdmin'}
      ]);
    });
};
