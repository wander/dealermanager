
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('moneda').del()
    .then(function () {
      // Inserts seed entries
      return knex('moneda').insert([
        {descripcion: 'Pesos',predeterminada:true},
        {descripcion: 'Dolares',predeterminada:false},
        {descripcion: 'Euro', predeterminada:false},
       
      ]);
    });
};
