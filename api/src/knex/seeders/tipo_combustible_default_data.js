
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('tipo_combustible').del()
    .then(function () {
      // Inserts seed entries
      return knex('tipo_combustible').insert([
        {descripcion: 'Gasolina'},
        {descripcion: 'Gas'},
        {descripcion: 'Gasoil'},
        {descripcion: 'Gas Natural'},
        {descripcion: 'Electrico'},
      ]);
    });
};
