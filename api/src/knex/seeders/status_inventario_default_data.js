
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('status_inventario').del()
    .then(function () {
      // Inserts seed entries
      return knex('status_inventario').insert([
        {descripcion: 'COMPRADO'},
        {descripcion: 'EN GRUA'},
        {descripcion: 'RECIBIDO TALLER'},
        {descripcion: 'EMBARCADO'},
        {descripcion: 'RECIBIDO MUELLE'},
        {descripcion: 'RECIBIDO DEALER'},
        {descripcion: 'DISPONIBLE'},
     
      ]);
    });
};
