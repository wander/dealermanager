

exports.seed = function(knex) {


  // Deletes ALL existing entries
  return knex('venta').del()
    .then(function () {
      // Inserts seed entries
      return knex('venta').insert([
        {codigofactura:'1111111111',cliente:'Juan Perez',cedula:'222222',monto:25000,
        sucursal_id:1,moneda_id:2,
      tasacambio_id:3,inventario_id:1},
      {codigofactura:'2222',cliente:'Juaniquito acosta',cedula:'222222',monto:25000,
      sucursal_id:1,moneda_id:2,
    tasacambio_id:3,inventario_id:2}
      ]);
    });
};
