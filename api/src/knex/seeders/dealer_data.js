
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('dealer').del()
    .then(function () {
      // Inserts seed entries
      return knex('dealer').insert([
        {id: 1, rnc: '2',nombre:'Uceta te Monta',website:'ucetatemonta@gmail.com',email:'ucetatemonta@gmail.com'},
        {id: 2, rnc: '3',nombre:'Guzman Auto Import',website:'www.guzmanai.do',email:'guzmanautoimport@gmail.com'},
      ]);
    });
};
