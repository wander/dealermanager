
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('tasa_cambio').del()
    .then(function () {
      // Inserts seed entries
      return knex('tasa_cambio').insert([
        {default_moneda_id:1,moneda_id:2,valor:58.50,predeterminada:false},
        {default_moneda_id:1,moneda_id:1,valor:1,predeterminada:true},
      ]);
    });
};
