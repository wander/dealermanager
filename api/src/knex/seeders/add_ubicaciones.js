
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('ubicacion').del()
    .then(function () {
      // Inserts seed entries
      return knex('ubicacion').insert([
        {descripcion: 'MIAMI'},
        {descripcion: 'SANTO DOMINGO'}
      ]);
    });
};
