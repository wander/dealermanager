
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('cilindro').del()
    .then(function () {
      // Inserts seed entries
      return knex('cilindro').insert([
        {descripcion: '4 clindros'},
        {descripcion: '6 clindros'},
        {descripcion: '8 clindros'},
      ]);
    });
};
