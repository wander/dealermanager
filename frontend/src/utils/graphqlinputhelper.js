export default {

    generateInventarioRow: (productoInventario) => {
        return {
            id: productoInventario.id,
            loteSerie: productoInventario.loteSerie,
            codigoBarra: productoInventario.codigoBarra,
            existencia: productoInventario.existencia,
            disponible: productoInventario.disponible,
            tramos: productoInventario.tramos,
            tramosToAdd: [],
            tramosToRemove: [],
            productoPresentacionPrecio: productoInventario.productoPresentacionPrecio,
            insert: false
        }
    }
}