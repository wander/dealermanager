
module.exports = {
  producto: {
    id: 0,
    descripcion: "",
    nombre: "",
    codigo: "",
    status: false,
    marca: null,
    tipoProducto: null,
    tipoLote: null,
    impuestos: [
      {
        id: 0,
        impuesto: "",
        valor: 0,
      },
    ],
  },
  //definicion de producto inventario
  productoInventarios: []
}
