


import { AUTH_TOKEN, USER, USER_ID } from '../constants/settings'
import { LocalStorage } from "quasar";
import { Role } from '../constants/role'




//TODO: va para el vuerouter en beforeEnter: requireAuth import { requireAuth } from '../../utils/auth';
class Auth {
  constructor() {

  }

  static User = this.getUser()

  static logout(app) {
    this.clearAccessToken();
    this.clearUser();
    this.clearUserId();

  }
  static checkRole(roles) {
    const validroles = roles.some(r => this.User.roles.some(r2 => r2.descripcion === r))
    return validroles
  }
  static requireAuth(to, from, next) {
    if (!this.$Auth.isLoggedIn()) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      });
    } else {
      next();
    }
  }

  static setAccessToken(token) {
    LocalStorage.set(AUTH_TOKEN, token)
  }
  static setUserId(userId) {
    LocalStorage.set(USER_ID, userId)
  }

  static setUser(user) {
    LocalStorage.set(USER, user)
  }

  static setData(user, token) {
    this.setUser(user)
    this.setAccessToken(token)
    this.setUserId(user.id)
  }


  static getUserId() {
    return LocalStorage.getItem(USER_ID);
  }

  static getAccessToken() {
    return LocalStorage.getItem(AUTH_TOKEN);
  }

  static getUser() {
    return LocalStorage.getItem(USER)
  }

  static getSucursales(){
   const sucursales = this.getUser().sucursales ?? []
   return sucursales

  }



  static clearUserId() {
    LocalStorage.remove(USER_ID);
  }
  static clearAccessToken() {
    LocalStorage.remove(AUTH_TOKEN);
  }
  static clearUser() {
    LocalStorage.remove(USER)
  }

  // Helper function that will allow us to extract the access_token and id_token

  static isLoggedIn() {
    const idToken = this.getUserId();
    return idToken != null;
  }

  //getting roles information
  static isAdmin() {
    const user = this.getUser()
    return user.roles.some(item => item.descripcion === Role.Admin)
  }

  static isSuperAdmin() {
    const user = this.getUser()
    return user.roles.some(item => item.descripcion === Role.SuperAdmin)

  }

}
export default Auth
