import { date } from 'quasar'


class MyUtils {

  constructor() {

  }
  static router = {}

  static formatCurrency(val) {
    return `$ ${parseFloat(val ? val:0).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,').toString()}`;
  }


  static convertDate(date) {
    var datearray = date.split("/");
    var newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
    return newdate;

  }

  static formatDate(value) {
    const da = value
    return date.formatDate(value, 'DD/MM/YYYY hh:mm:ss A')
  }
  static returnTo(to_return) {
    //console.log(router.)
    this.router.push({ name: to_return })
  }
  static redirectTo(to_redirect={}){
    this.router.push(to_redirect);
  }
 
}
export default MyUtils