import Errors from "./Errors";
import { apolloProvider } from "../boot/apollo";
import { Notify } from "quasar";
import i18n from 'src/i18n/i18n';

class Form {

  constructor(data) {


    this.originalData = data;
    // console.log(this.$t('success'))
    // console.log(this.$i18n)
    for (let field in data) {
      this[field] = data[field];
    }
    this.errors = new Errors();
  }

  reset() {
    for (let field in this.originalData) {
      this[field] = "";
    }
    this.errors.clear();
  }

  post(url) {
    return this.submit("post", url);
  }

  submit(mutation, mutationVariables) {
    return new Promise((resolve, reject) => {
      apolloProvider.defaultClient
        .mutate({
          mutation: mutation,
          variables: mutationVariables
        })
        .then(response => {
          this.onSuccess(response.data);
          resolve(response.data);
        })
        .catch(error => {
          this.onFail(error);
          reject(error);
        });
    });
  }

  data() {
    let data = {};

    for (let property in this.originalData) {
      data[property] = this[property];
    }

    return data;

    // delete this.errors;
  }

  onSuccess(data) {
    //  alert(data.valoresafectados);
    Notify.create({
      message: i18n.t('success'),
      color: "positive",
      icon: "fa fa-info",
      /*  actions: [
         {
           label: "Regresar",
           color:"white",
           handler: () => {
             //router.go(-1);
             window.history.go(-1);
             console.log("dismissed");
           }
         }
       ] */
    });

    console.log(data);
    //this.reset();

    // document.getElementById("name").focus();
  }

  onFail(errors) {
    Notify.create({
      message: i18n.t('failed'),
      type: "negative",

    });
    this.errors.record(errors);
    //console.log(errors);
  }
}

export default Form;
