import {  apolloProvider } from 'src/boot/apollo'
import { FIND_BY_ID, FIND_BY_PARAM,FIND_BY_USER_DEALER } from './queries'
export default {
    findByParam: (param,with_details=false) => {
        return apolloProvider.defaultClient.query({
            query: FIND_BY_PARAM,
            variables: {
                param: param,
                withDetails:with_details
            },
            fetchPolicy:'network-only'
        }).then((response) => {
            return response;
        }).catch((error) => {
            return error.response;
        })
    },
    findByParamAndDealer: (param,with_details=false) => {
        return apolloProvider.defaultClient.query({
            query: FIND_BY_USER_DEALER,
            variables: {
                param: param,
                withDetails:with_details
            },
            fetchPolicy:'network-only'
        }).then((response) => {
            return response;
        }).catch((error) => {
            return error.response;
        })
    },
    findById: (id) => {
        return apolloProvider.defaultClient.query({
            query: FIND_BY_ID,
            variables: {
                id: id
            },
            fetchPolicy:'network-only'
        }).then((response) => {
            return response;
        }).catch((error) => {
            return error.response;
        })
    }
}