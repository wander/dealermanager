import gpl from "graphql-tag";
const fields=`
id
descripcion
codigosubasta
precio_venta
vin
year
color{
    id
    descripcion
    class
}
sucursal{
    id
    descripcion
    dealer{
        id
        nombre
    }
}
status_inventario{
    id
    descripcion
}
gastos{
    id
    descripcion
    tipo_gasto{
        id
        descripcion
    }
    monto
    tasa_cambio{
        id
        valor
        moneda_predeterminada{
            id
            descripcion
        nomenclatura

        }
        moneda_alterna{
            id
            descripcion
        nomenclatura
        }

    }
}
vehiculo{
    id
    modelo{
        id
        descripcion
        marca{
            id
            descripcion
        }
    }
    tipo_combustible{
        id
        descripcion
    }
    tipo_motor{
        id
        descripcion
    }
    cilindro{
        id
        descripcion
    }
    tipo_transmision{
        id
        descripcion
    }
    tipo_vehiculo{
        id
        descripcion
    }
    tipo_train{
        id
        descripcion
    }
}

`
const INSERT=gpl`mutation($inventario: inventarioinput!) {
    createInventario(inventario: $inventario) {
        ${fields}
    }
  }`;
  const UPDATE=gpl`mutation($inventario: inventarioinput!) {
    updateInventario(inventario: $inventario) {
    ${fields}
    }
  }`

  export{
      INSERT,
      UPDATE
  }
