import gpl from 'graphql-tag'
const fields=`id
descripcion
predeterminada
nomenclatura
`
const FIND_BY_PARAM=gpl`
query data($param:String)
{
    monedas(param:$param){
        ${fields}
    }
}
`
const FIND_BY_ID=gpl`
query data($id:Int!)
{
    moneda(id:$id){
        ${fields}
    }
}
`

export{FIND_BY_PARAM,FIND_BY_ID}
