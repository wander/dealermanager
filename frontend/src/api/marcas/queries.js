import gpl from 'graphql-tag'
const fields=`id
descripcion
modelos @include(if:$withModelos){
    id
    descripcion
}
`
const FIND_BY_PARAM=gpl`
query data($param:String,$withModelos:Boolean!)
{
    marcas(param:$param,withModelos:$withModelos){
        ${fields}
    }
}
`
const FIND_BY_ID=gpl`
query data($id:Int!)
{
    marca(id:$id){
        ${fields}
    }
}
`

export{FIND_BY_PARAM,FIND_BY_ID}
