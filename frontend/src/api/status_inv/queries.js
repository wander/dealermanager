import gpl from 'graphql-tag'
const fields=`id
descripcion
`
const FIND_BY_PARAM=gpl`
query data($param:String)
{
    statuses_inv(param:$param){
        ${fields}
    }
}
`
const FIND_BY_ID=gpl`
query data($id:Int!)
{
    status_inv(id:$id){
        ${fields}
    }
}
`

export{FIND_BY_PARAM,FIND_BY_ID}
