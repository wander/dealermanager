import gpl from 'graphql-tag'
const fields=`id
descripcion
class
`
const FIND_BY_PARAM=gpl`
query data($param:String)
{
    colors(param:$param){
        ${fields}
    }
}
`
const FIND_BY_ID=gpl`
query data($id:Int!)
{
    color(id:$id){
        ${fields}
    }
}
`

export{FIND_BY_PARAM,FIND_BY_ID}
