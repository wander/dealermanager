import gpl from 'graphql-tag'
const fields=`id
rnc
nombre
pathlogo
email
website
sucursales @include(if:$withDetails)
{
    id
    descripcion
    direccion
    telefonos
}
`
const FIND_BY_PARAM=gpl`
query data($param:String,$withDetails:Boolean!)
{
    dealers(param:$param,withDetails:$withDetails){
        ${fields}
    }
}
`
const FIND_BY_PARAM_SU=gpl`
query data($param:String,$withDetails:Boolean!)
{
    dealersByUsuario(param:$param,withDetails:$withDetails){
        ${fields}
    }
}
`

const FIND_BY_ID=gpl`
query data($id:Int!,$withDetails:Boolean!)
{
    dealer(id:$id,withDetails:$withDetails){
        ${fields}
    }
}
`
const VAL_EXISTS=gpl`
query data($param:String,$field:String)
{
    dealerValExists(param:$param,field:$field){
        id
    }
}
`
export{FIND_BY_PARAM,FIND_BY_ID,VAL_EXISTS,FIND_BY_PARAM_SU}
