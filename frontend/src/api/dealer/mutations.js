import gpl from "graphql-tag";
const fields=`id
rnc
nombre
pathlogo
email
website
sucursales
{
    id
    direccion
    telefonos
}
`
const INSERT=gpl`mutation($dealer: dealerinput!) {
    createDealer(dealer: $dealer) {
        ${fields}
    }
  }`;
  const UPDATE=gpl`mutation($dealer: dealerinput!) {
    updateDealer(dealer: $dealer) {
    ${fields}
    }
  }`

  export{
      INSERT,
      UPDATE
  }
