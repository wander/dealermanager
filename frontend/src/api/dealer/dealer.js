import {  apolloProvider } from 'src/boot/apollo'
import { FIND_BY_ID, FIND_BY_PARAM,VAL_EXISTS,FIND_BY_PARAM_SU } from './queries'
export default {
    findByParam: (param,with_details=false) => {
        return apolloProvider.defaultClient.query({
            query: FIND_BY_PARAM,
            variables: {
                param: param,
                withDetails:with_details
            }
        }).then((response) => {
            return response;
        }).catch((error) => {
            return error.response;
        })
    },

    findByParamAndUser: (param,with_details=false) => {
        return apolloProvider.defaultClient.query({
            query: FIND_BY_PARAM_SU,
            variables: {
                param: param,
                withDetails:with_details
            }
        }).then((response) => {
            return response;
        }).catch((error) => {
            return error.response;
        })
    },
    findById: (id,with_details=true) => {
        return apolloProvider.defaultClient.query({
            query: FIND_BY_ID,
            variables: {
                id: id,
                withDetails:with_details
            }
        }).then((response) => {
            return response;
        }).catch((error) => {
            return error.response;
        })
    },

    val_exists: (param,field) => {
        return apolloProvider.defaultClient.query({
            query: VAL_EXISTS,
            variables: {
                param: param,
                field:field
            }
        }).then((response) => {
            return response;
        }).catch((error) => {
            return error.response;
        })
    },
}