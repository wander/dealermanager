import {  apolloProvider } from 'src/boot/apollo'
import { FIND_BY_ID, FIND_BY_PARAM } from './queries'

export default{
    findByParam: (param,marcaid) => {
        return apolloProvider.defaultClient.query({
            query: FIND_BY_PARAM,
            variables: {
                param: param,
                marcaid:marcaid
            }
        }).then((response) => {
            return response;
        }).catch((error) => {
            return error.response;
        })
    },
    findById: (id) => {
        return apolloProvider.defaultClient.query({
            query: FIND_BY_ID,
            variables: {
                id: id
            }
        }).then((response) => {
            return response;
        }).catch((error) => {
            return error.response;
        })
    },
}