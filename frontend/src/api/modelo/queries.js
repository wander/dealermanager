import gpl from 'graphql-tag'
const fields=`id
descripcion
marca{
    id
    descripcion
    modelos{
        id
        descripcion
    }
}
`
const FIND_BY_PARAM=gpl`
query data($param:String,$marcaid:Int!)
{
    modelos(param:$param,marcaid:$marcaid){
        ${fields}
    }
}
`
const FIND_BY_ID=gpl`
query data($id:Int!)
{
    modelo(id:$id){
        ${fields}
    }
}
`

export{FIND_BY_PARAM,FIND_BY_ID}
