import dealer from './dealer/dealer'
import sucursal from './sucursales/sucursal'
import marca from './marcas/marca'
import cilindro from './cilindros/cilindro'
import modelo from './modelo/modelo'
import tipo_combustible from './tipo_combustible/tipo_combustible'
import tipo_motor from './tipo_motor/tipo_motor'
import tipo_train from './tipo_train/tipo_train'
import tipo_transmision from './tipo_transmision/tipo_transmision'
import tipo_vehiculo from './tipo_vehiculo/tipo_vehiculo'
import vehiculo from './vehiculos/vehiculo'
import inventario from './inventario/inventario'
import statusinv from './status_inv/statusinv'
import color from './color/color'
import gasto from './gasto/gasto'
import moneda from './moneda/moneda'
import ubicacion from './ubicacion/ubicacion'
import tipo_gasto from './tipo_gasto/tipo_gasto'
import tasa_cambio from './tasa_cambio/tasa_cambio'
import usuario from './security/usuario/usuario'
import rol from './security/rol/rol'
import venta from './venta/venta'
export default{
    dealer,
    sucursal,
    marca,
    cilindro,
    tipo_combustible,
    tipo_motor,
    tipo_train,
    tipo_transmision,
    tipo_vehiculo,
    modelo,
    vehiculo,
    inventario,
    statusinv,
    color,
    gasto,
    tipo_gasto,
    ubicacion,
    moneda,
    tasa_cambio,
    usuario,
    rol,
    venta

}