import gpl from 'graphql-tag'
const fields=`id
              modelo{
                  id
                  descripcion
                  marca{
                      id
                      descripcion
                      modelos{
                          id
                          descripcion
                      }
                      
                  }        
              }
              tipo_combustible @include(if:$withDetails){
                  id
                  descripcion
              }
              tipo_motor @include(if:$withDetails){
                id
                descripcion
            }
            cilindro @include(if:$withDetails){
                id
                descripcion
            }
            tipo_transmision @include(if:$withDetails){
                id
                descripcion
            }
            tipo_vehiculo @include(if:$withDetails){
                id
                descripcion
            }
            tipo_train @include(if:$withDetails){
                id
                descripcion
            }`
const FIND_BY_PARAM=gpl`
query data($param:String,$withDetails:Boolean!)
{
    vehiculos(param:$param,withDetails:$withDetails){
        ${fields}
    }
}
`
const FIND_BY_ID=gpl`
query data($id:Int!,$withDetails:Boolean!)
{
    vehiculo(id:$id,withDetails:$withDetails){
        ${fields}
    }
}
`

export{FIND_BY_PARAM,FIND_BY_ID}
