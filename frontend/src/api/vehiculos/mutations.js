import gpl from "graphql-tag";
const fields = `id
modelo{
    id
    descripcion
    marca{
        id
        descripcion
        modelos{
            id
            descripcion
        }
        
    }        
}
tipo_combustible{
    id
    descripcion
}
tipo_motor{
id
descripcion
}
cilindro{
id
descripcion
}
tipo_transmision{
id
descripcion
}
tipo_vehiculo{
id
descripcion
}
tipo_train{
id
descripcion
}`

const INSERT = gpl`mutation($vehiculo: vehiculoinput!) {
    createVehiculo(vehiculo: $vehiculo) {
        ${fields}
    }
  }`;
const UPDATE = gpl`mutation($vehiculo: vehiculoinput!) {
    updateVehiculo(vehiculo: $vehiculo) {
    ${fields}
    }
  }`

export {
    INSERT,
    UPDATE
}
