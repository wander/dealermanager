import { apolloProvider } from 'src/boot/apollo'
import { FIND_BY_ID, FIND_BY_PARAM } from './queries'

export default {
    findByParam: (param,withDetails=false) => {
        return apolloProvider.defaultClient.query({
            query: FIND_BY_PARAM,
            variables: {
                param: param,
                withDetails:withDetails
            }
        }).then((response) => {
            return response;
        }).catch((error) => {
            return error.response;
        })
    },
    findById: (id,withDetails=true) => {
        return apolloProvider.defaultClient.query({
            query: FIND_BY_ID,
            variables: {
                id: id,
                withDetails:withDetails
            }
        }).then((response) => {
            return response;
        }).catch((error) => {
            return error.response;
        })
    },
}