import gpl from "graphql-tag";
const fields=`
id
descripcion
createdat
tipo_gasto{
  id
  descripcion
}
monto
inventario{
  id
}
ubicacion{
  id
  descripcion
}
moneda{
  id
  descripcion
  nomenclatura
  predeterminada
}
tasa_cambio{
  id
  valor
  moneda_predeterminada{
    id
    descripcion
    nomenclatura
  }
  moneda_alterna{
    id
    descripcion
    nomenclatura
  }
}
`
const FIND_BY_PARAM = gpl`
query data($param:String,$inventarioid:Int!,$withDetails:Boolean!) {
  gastos(param:$param,inventarioid:$inventarioid,withDetails:$withDetails){
    ${fields}
  }
}
`;
const FIND_BY_ID = gpl`
query data($id:Int!) {
  gasto(id:$id){
    ${fields} 
  }
}
`;



export{FIND_BY_ID,FIND_BY_PARAM}