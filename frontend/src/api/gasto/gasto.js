import { apolloProvider } from "src/boot/apollo";
import {
  FIND_BY_PARAM,
  FIND_BY_ID
} from "./queries";

import { DELETE }
  from './mutations'

export default {
  get_by_param: (param, inventarioid, withDetails = false) => {
    return apolloProvider.defaultClient
      .query({
        query: FIND_BY_PARAM,
        variables: {
          param: param,
          inventarioid: inventarioid,
          withDetails: withDetails

        },
        fetchPolicy:'network-only'
      })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error.response;
      });
  },
  delete_by_id: (gasto) => {
    return apolloProvider.defaultClient
      .query({
        query: DELETE,
        variables: {
        gasto:gasto

        },
        fetchPolicy:'network-only'
      })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error.response;
      });

  },
  get_by_id: (id) => {
    return apolloProvider.defaultClient
      .query({
        query: FIND_BY_ID,
        variables: {
          id: id
        },
        
        fetchPolicy:'network-only'
      })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error.response;
      });
  }
}