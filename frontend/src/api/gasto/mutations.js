import gpl from "graphql-tag";
const fields=`
id
descripcion
createdat
tipo_gasto{
  id
  descripcion
}
monto
inventario{
  id
}
ubicacion{
  id
  descripcion
}
moneda{
  id
  descripcion
  nomenclatura
}
tasa_cambio{
  id
  valor
  moneda_predeterminada{
    id
    descripcion
    nomenclatura
  }
  moneda_alterna{
    id
    descripcion
    nomenclatura
  }
}
`
const INSERT=gpl`mutation($gasto: gastoinput!) {
    createGasto(gasto: $gasto) {
        ${fields}
    }
  }`;
  const UPDATE=gpl`mutation($gasto: gastoinput!) {
    updateGasto(gasto: $gasto){
    ${fields}
    }
  }`
;
const DELETE=gpl`mutation($gasto: gastoinput!) {
  deleteGasto(gasto: $gasto){
  ${fields}
  }
}`

  export{
      INSERT,
      UPDATE,
      DELETE
  }
