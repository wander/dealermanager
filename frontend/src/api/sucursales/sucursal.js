import { apolloProvider } from "src/boot/apollo";
import {
  FIND_BY_PARAM,
  FIND_BY_PARAM2,
  FIND_BY_ID,
  VAL_EXISTS
} from "./queries";

export default {
  get_by_param: (param) => {
    return apolloProvider.defaultClient
      .query({
        query: FIND_BY_PARAM,
        variables: {
          param: param,

        }
      })
      .then(response => {  
        return response;
      })
      .catch(error => {
        return error.response;
      });
  },

  get_by_param2: (param) => {
    return apolloProvider.defaultClient
      .query({
        query: FIND_BY_PARAM2,
        variables: {
          param: param,

        }
      })
      .then(response => {  
        return response;
      })
      .catch(error => {
        return error.response;
      });
  },
  get_by_id: (id, withDealer) => {
    return apolloProvider.defaultClient
      .query({
        query: FIND_BY_ID,
        variables: {  
            id:id,
            withDealer:withDealer
        }
      })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error.response;
      });
  },
  getSucursalById: id => {
    return apolloProvider.defaultClient
      .query({
        query: FIND_BY_ID,
        variables: {
          id: id
        }
      })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error.response;
      });
  },

  valSucursalDataExists: (param, field) => {
    return apolloProvider.defaultClient
      .query({
        query: VAL_EXISTS,
        variables:{
          param: param,
          field:field
        } 
      })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error.response;
      });
  }
};
