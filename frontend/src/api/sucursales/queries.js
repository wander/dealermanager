import gpl from "graphql-tag";
const fields=`id
descripcion
direccion
telefonos
dealer{
 id
 rnc
 nombre
 website
 email
}`
const FIND_BY_PARAM = gpl`
query data($param:String) {
  sucursales(param:$param){
    ${fields}
  }
}
`;

const FIND_BY_PARAM2 = gpl`
query data($param:String) {
  sucursalesv2(param:$param){
    ${fields}
  }
}
`;
const FIND_BY_ID = gpl`
query data($id:Int!,$withDealer:Boolean!) {
  sucursal(id:$id,withDealer:$withDealer){
    ${fields} 
  }
}
`;

const VAL_EXISTS=gpl`
query data($param:String,$field:String)
{
    sucursalValExists(param:$param,field:$field){
        id
    }
}`;

export{FIND_BY_ID,FIND_BY_PARAM,FIND_BY_PARAM2,VAL_EXISTS}