import gpl from "graphql-tag";
const fields=`
    id
    direccion
    telefonos
`
const INSERT=gpl`mutation($sucursal: sucursalinput!) {
    createSucursal(sucursal: $sucursal) {
        ${fields}
    }
  }`;
  const UPDATE=gpl`mutation($sucursal: sucursalinput!) {
    updateSucursal(sucursal: $sucursal){
    ${fields}
    }
  }`

  export{
      INSERT,
      UPDATE
  }
