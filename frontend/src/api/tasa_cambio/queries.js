import gpl from 'graphql-tag'
const fields = `id
valor
moneda_predeterminada{
descripcion
predeterminada
nomenclatura
}
moneda_alterna{
descripcion
predeterminada
nomenclatura   
}
`
const FIND_BY_PARAM = gpl`
query data($param:String)
{
    tasa_cambios(param:$param){
        ${fields}
    }
}
`
const FIND_BY_ID = gpl`
query data($id:Int!)
{
    tasa_cambio(id:$id){
        ${fields}
    }
}
`

export { FIND_BY_PARAM, FIND_BY_ID }
