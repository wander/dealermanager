import gpl from 'graphql-tag'
const fields=`id
descripcion
`
const FIND_BY_PARAM=gpl`
query data($param:String)
{
    ubicaciones(param:$param){
        ${fields}
    }
}
`
const FIND_BY_ID=gpl`
query data($id:Int!)
{
    ubicacion(id:$id){
        ${fields}
    }
}
`

export{FIND_BY_PARAM,FIND_BY_ID}
