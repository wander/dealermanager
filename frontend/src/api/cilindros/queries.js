import gpl from 'graphql-tag'
const fields=`id
descripcion
`
const FIND_BY_PARAM=gpl`
query data($param:String)
{
    cilindros(param:$param){
        ${fields}
    }
}
`
const FIND_BY_ID=gpl`
query data($id:Int!)
{
    cilindro(id:$id){
        ${fields}
    }
}
`

export{FIND_BY_PARAM,FIND_BY_ID}
