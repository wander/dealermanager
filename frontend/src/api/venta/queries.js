import gpl from 'graphql-tag'
import {fields, fields_report} from './fields'
//TODO OPTIMIZAR FIELD SELECTION


const FIND_BY_PARAM = gpl`
query data($param:String,$startDate:Date,$endDate:Date,$withDetails:Boolean){
    ventas(param:$param,startDate:$startDate,endDate:$endDate,withDetails:$withDetails){
        ${fields}
    }
}
`
const FIND_BY_ID = gpl`
query data($id:Int!){
    venta(id:$id){
        ${fields}
    }
}`

const FIND_BY_PARAM_REPORT = gpl`
query data($param:String,$startDate:Date,$endDate:Date,$withDetails:Boolean){
    ventas(param:$param,startDate:$startDate,endDate:$endDate,withDetails:$withDetails){
        ${fields_report}
    }
}
`
export { FIND_BY_ID, FIND_BY_PARAM,FIND_BY_PARAM_REPORT }