import gpl from "graphql-tag";
import { fields_mod } from './fields'
const INSERT = gpl`mutation($venta: ventainput!) {
    createVenta(venta: $venta) {
        ${fields_mod}
    }
  }`;
const UPDATE = gpl`mutation($venta: ventainput!) {
    updateVenta(venta: $venta) {
    ${fields_mod}
    }
  }`

export {
    INSERT,
    UPDATE
}
