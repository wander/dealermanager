import { apolloProvider } from 'src/boot/apollo'
import { FIND_BY_ID, FIND_BY_PARAM, FIND_BY_PARAM_REPORT } from './queries'

export default {
    findByParam: async (param, startDate = undefined, endDate = undefined, withDetails = false) => {
        return await apolloProvider.defaultClient.query({
            query: FIND_BY_PARAM,
            variables: {
                param: param,
                withDetails: withDetails,
                startDate: startDate,
                endDate: endDate

            }

            , fetchPolicy: 'network-only'
        },


        ).then((response) => {
            return response;
        }).catch((error) => {
            return error.response;
        })
    },

    findByParamReport:async (param, startDate = undefined, endDate = undefined, withDetails = false) => {
        return await apolloProvider.defaultClient.query({
            query: FIND_BY_PARAM_REPORT,
            variables: {
                param: param,
                withDetails: withDetails,
                startDate: startDate,
                endDate: endDate

            }

            , fetchPolicy: 'network-only'
        },


        ).then((response) => {
            return response;
        }).catch((error) => {
            return error.response;
        })
    },


    findById: (id, withDetails = true) => {
        return apolloProvider.defaultClient.query({
            query: FIND_BY_ID,
            variables: {
                id: id,
                withDetails: withDetails
            }
        }).then((response) => {
            return response;
        }).catch((error) => {
            return error.response;
        })
    },
}