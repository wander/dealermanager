import { apolloProvider } from 'src/boot/apollo'
import { FIND_BY_ID, FIND_BY_PARAM,FIND_BY_DEALER,VAL_USERNAME,VAL_CLAVE } from './queries'

export default {
    findByParam: (param) => {
        return apolloProvider.defaultClient.query({
            query: FIND_BY_PARAM,
            variables: {
                param: param
            }
        }).then((response) => {
            return response;
        }).catch((error) => {
            return error.response;
        })
    },
    findById: (id) => {
        return apolloProvider.defaultClient.query({
            query: FIND_BY_ID,
            variables: {
                id: id
            }
        }).then((response) => {
            return response;
        }).catch((error) => {
            return error.response;
        })
    },
    findByDealer: (param) => {
        return apolloProvider.defaultClient.query({
            query: FIND_BY_ID,
            variables: {
                param: param
            }
        }).then((response) => {
            return response;
        }).catch((error) => {
            return error.response;
        })
    },

    val_username: (nombreusuario) => {
        return apolloProvider.defaultClient.query({
            query: VAL_USERNAME,
            variables: {
               nombreusuario :nombreusuario
            }
        }).then((response) => {
            return response;
        }).catch((error) => {
            return error.response;
        })
    },
    val_clave: (clave) => {
        return apolloProvider.defaultClient.query({
            query: VAL_CLAVE,
            variables: {
               clave :clave
            },
            fetchPolicy:'network-only'
        }).then((response) => {
            return response;
        }).catch((error) => {
            return error.response;
        })
    },
}