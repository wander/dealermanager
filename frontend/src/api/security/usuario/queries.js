import gpl from 'graphql-tag'
const fields=` 

  id
  nombreusuario
  clave
  nombre
  email
  activo
  sucursales{
    id
    descripcion
    direccion
    telefonos
    dealer{
        id
        nombre
    }
  }
  roles{
    id
    descripcion
  }    

`
const FIND_BY_PARAM=gpl`
query data($param:String)
{
    usuarios(param:$param){
        ${fields}
    }
}
`
const FIND_BY_ID=gpl`
query data($id:Int!)
{
    usuario(id:$id ){
        ${fields}
    }
}
`
const VAL_USERNAME=gpl`
query data($nombreusuario:String){
  validateUserName(nombreusuario:$nombreusuario)
}`

const VAL_CLAVE=gpl`query data($clave:String){
  validateClave(clave:$clave)
}`

const FIND_BY_DEALER=gpl`
query data($param:String){
    usuariosbyDealer(param:$param ){
        ${fields}
    }
}
`

export{ FIND_BY_PARAM,FIND_BY_ID,FIND_BY_DEALER,VAL_USERNAME,VAL_CLAVE}
 