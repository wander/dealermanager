import gpl from "graphql-tag";
const fields=` 
    id
    nombreusuario
    clave
    email
    nombre
    activo
    sucursales{
      id
      descripcion
      direccion
      telefonos
      dealer{
          id
          nombre
      }
    }
    roles{
      id
      descripcion
    }        
`
const LOGIN=gpl`mutation($usuario: usuarioinput!) {
    login(usuario: $usuario){
      usuario{${fields}},
      token
    }
  }`;
  const SIGNUP=gpl`mutation($usuario: usuarioinput!) {
    signup(usuario: $usuario) 
  }`
  const INSERT = gpl`mutation($usuario: usuarioinput!) {
    createUsuario(usuario: $usuario) {
        ${fields}
    }
  }`;
const UPDATE = gpl`mutation($usuario: usuarioinput!) {
    updateUsuario(usuario: $usuario) {
    ${fields}
    }
  }`;

  const UPDATE_PERFIL = gpl`mutation($usuario: usuarioinput!) {
    updateUsuarioPerfil(usuario: $usuario) {
    ${fields}
    }
  }`

  const UPDATE_PASS= gpl`mutation($usuario: usuarioinput!) {
    updateUsuarioPass(usuario: $usuario) {
    ${fields}
    }
  }`

  export{
     LOGIN,
     SIGNUP,
     INSERT,
     UPDATE,
     UPDATE_PERFIL,
     UPDATE_PASS
  }
