import { apolloProvider } from 'src/boot/apollo'
import {  FIND_BY_PARAM } from './queries'

export default {
    findByParam: (param) => {
        return apolloProvider.defaultClient.query({
            query: FIND_BY_PARAM,
            variables: {
                param: param
            }
        }).then((response) => {
            return response;
        }).catch((error) => {
            return error.response;
        })
    },
}