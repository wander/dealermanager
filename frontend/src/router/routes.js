import cmDealers from '../pages/dealers/cmDealers.vue'
import mDealers from '../pages/dealers/mantDealers.vue'

import mSucursales from '../pages/sucursales/mantsucursales.vue'

import cmVehiculos from '../pages/vehiculos/cmVehiculos.vue'
import mVehiculos from '../pages/vehiculos/mantVehiculos.vue'


import cmInventarios from '../pages/inventarios/cmInventarios.vue'
import mInventarios from '../pages/inventarios/mantInventarios.vue'

import mGastos from '../pages/gastos/mantGastos.vue'

import login from '../pages/security/login.vue'

import settings from '../pages/configuration/settings.vue'

import mProfile from '../pages/configuration/profile.vue'

import mPrintTest from '../pages/pruebas/printtest.vue'

import mantuser from '../pages/configuration/mantuser.vue'

import cmVentas from '../pages/venta/cmVenta.vue'

import mantventas from '../pages/venta/mantVenta.vue'

//reportes
import rptganancias from '../pages/reportes/rptVentasGanancias.vue'

import { Role } from '../constants/role'
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  },
  {
    path: '/unauthorized',
    component: () => import('pages/Error401.vue')
  },

  {
    path: "/printtest",
    component: () => import("layouts/MainLayout.vue"),
    children: [{ path: "", name: "printtest", component: mPrintTest }]
  },
  //security
  {
    path: "/login",
    component: () => import("layouts/MainLayout.vue"),
    children: [{ path: "", name: "login", component: login }]
  },
  //dealers
  {
    path: "/dealers",
    component: () => import("layouts/MainLayout.vue"),
    meta: { authorize: [Role.Admin, Role.SuperAdmin] },
    children: [{ path: "", name: "dealers", component: cmDealers }]
  },
  //settings
  {
    path: "/settings",
    component: () => import("layouts/MainLayout.vue"),
    meta: { authorize: [Role.Admin, Role.SuperAdmin] },
    children: [{ path: "", name: "settings", component: settings }]
  },
  {
    path: "/mantdealers/:id?",
    component: () => import("layouts/MainLayout.vue"),
    meta: { authorize: [Role.Admin, Role.SuperAdmin] },
    children: [{ path: "", name: "mantdealers", component: mDealers }]
  },
  //sucursales
  {
    path: "/mantsucursales/:id?/:dealerid?",
    component: () => import("layouts/MainLayout.vue"),
    meta: { authorize: [Role.Admin, Role.SuperAdmin] },
    children: [{ path: "", name: "mantsucursales", component: mSucursales }]
  },
  //vehiculos
  {
    path: "/vehiculos",
    component: () => import("layouts/MainLayout.vue"),
    meta: { authorize: [] },
    children: [{ path: "", name: "vehiculos", component: cmVehiculos }]
  }, {
    path: "/mantvehiculos/:id?",
    component: () => import("layouts/MainLayout.vue"),
    meta: { authorize: [] },
    children: [{ path: "", name: "mantvehiculos", component: mVehiculos }]
  },
  //inventarios
  {
    path: "/inventarios",
    component: () => import("layouts/MainLayout.vue"),
    meta: { authorize: [] },
    children: [{ path: "", name: "inventarios", component: cmInventarios }]
  }, {
    path: "/mantinventarios/:id?",
    component: () => import("layouts/MainLayout.vue"),
    meta: { authorize: [] },
    children: [{ path: "", name: "mantinventarios", component: mInventarios }]
  },
  //gastos
  {
    path: "/mantgastos/:id?/:inventarioid?",
    component: () => import("layouts/MainLayout.vue"),
    meta: { authorize: [] },
    children: [{ path: "", name: "mantgastos", component: mGastos }]
  },

  //security and configuration
  //gastos
  {
    path: "/profile/:id?/",
    component: () => import("layouts/MainLayout.vue"),
    meta: { authorize: [] },
    children: [{ path: "", name: "profile", component: mProfile }]
  },
  //TODO:USER ADMIN PAGE
  {
    path: "/mantuser/:id?",
    component: () => import("layouts/MainLayout.vue"),
    meta: { authorize: [Role.Admin, Role.SuperAdmin] },
    children: [{ path: "", name: "mantuser", component: mantuser }]
  },
  //ventas
  {
    path: '/ventas',
    component: () => import("layouts/MainLayout.vue"),
    meta: { authorize: [] },
    children: [{ path: "", name: "ventas", component: cmVentas }]
  },
  {
    path: "/mantventa/:id?",
    component: () => import("layouts/MainLayout.vue"),
    meta: { authorize: [] },
    children: [{ path: "", name: "mantventa", component: mantventas }]
  },
  //reportes
  {
    path: '/reporteganancias',
    component: () => import("layouts/MainLayout.vue"),
    meta: { authorize: [] },
    children: [{ path: "", name: "reporteganancias", component: rptganancias }]
  }

]

export default routes
