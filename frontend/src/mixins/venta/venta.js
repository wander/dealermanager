import api from '../../api'

export default {
  data() {
    return {
      ventas: [],
      page: 1,
      search_ventas: "",
      startDate: undefined,
      endDate: undefined,
      loading: false,
      columns: [

        { label: "Factura", field: row => row.codigofactura ? row.codigofactura : "No Disponible", width: "80px", sort: true, },
        { label: "Cliente", field: row => row.cliente ? row.cliente : "No Disponible", width: "80px", sort: true,  },
        { label: "Cedula", field: row => row.cedula ? row.cedula : "No Disponible", width: "80px", sort: true,  },
        { label: "Vehiculo", field: row => row.inventario ? row.inventario.vehiculo.modelo.descripcion : "No Disponible", width: "80px", sort: true, },
        { label: "Monto", field: row => row.monto ? row.monto : "No Disponible", width: "80px", sort: true, format: (val, row) => `${this.$MyUtils.formatCurrency(val)}`, },
        { label: "Moneda", field: row => row.moneda ? row.moneda.descripcion : "No Disponible", width: "80px", sort: true, },
        { label: "Fecha", field: row => row.createdat ? row.createdat : "No Disponible", format: (val, row) => `${this.$MyUtils.formatDate(val)}`, width: "80px", sort: true, headerClasses: '' },
        { label: "Usuario Venta", field: row => row.usuario ? row.usuario.nombreusuario : "No Disponible", width: "50px", sort: true, },
        {
          name: "actions",
          label: "Acciones",
          field: "image_url",
          width: "100px",
          headerClasses: 'print-hide'
        }
      ],
      configs: {
        columnPicker: true,
        title: "Ventas"
      }
    };
  },
  mounted() {
    this.loadData();
  },
  watch: {
    // page() {
    //await this.loadData();
    //}
  },

  methods: {
   
    async loadData() {
      this.loading = true
      // console.log('111111'+this.$apollo);
      this.startDate = this.startDate ? this.$MyUtils.convertDate(this.startDate) : this.startDate
      this.endDate = this.endDate ? this.$MyUtils.convertDate(this.endDate) : this.endDate

       setTimeout(async() => {
      await  api.venta.findByParam(this.search_ventas, new Date(this.startDate), new Date(this.endDate))
          .then(response => {
            
              this.ventas = response?.data?.ventas;
            
          })
          .catch(error => {
            console.log(error);
            // this.loading = false;
          });

        this.loading = false
      }, 500)
    },
  
    clearData() {
      this.startDate = undefined
      this.endDate = undefined
      this.search_ventas = ""
      this.loadData()
    },
  


  
  }
}