import { INSERT, UPDATE } from "../../api/venta//mutations";
import api from "../../api";
import form_mixin from '../formmixin'

export default {
  mixins: [form_mixin],
  mounted() {
    //console.log(this.form);

    if (this.$route.params.id !== undefined)
      this.form.id = parseInt(this.$route.params.id);
    if (this.form.id != 0) this.loadVenta();
  },
  data() {
    return {
      loading: false,
      expanded: false,
      showcard: false,
      showinsert: true,
      insert: INSERT,
      update: UPDATE,
      searchgastos: "",
      form: new this.$Form({
        id: 0,
        codigofactura: '',
        cliente: '',
        cedula: '',
        monto: 0,
        sucursal: this.$Auth.getSucursales() ? this.$Auth.getSucursales()[0] : null,
        inventario: null,
        usuario: null,
        tasa_cambio: null,
        moneda: null

      }),
 
      insert: INSERT,
      update: UPDATE

    };
  },

  methods: {
    handleSubmit() {

      this.submitForm('venta')
    },
    async loadVenta() {
      this.loading = true
      setTimeout(async () => {
         await api.venta
          .findById(this.form.data().id, true)
          .then(response => {
            console.log(response)
            // this.form.id = response.data.inventario.id
            this.form.id = response.data.venta.id
            this.form.codigofactura = response.data.venta.codigofactura
            this.form.cliente = response.data.venta.cliente
            this.form.cedula = response.data.venta.cedula
            this.form.monto = response.data.venta.monto
            this.form.sucursal = response.data.venta.sucursal
            this.form.inventario = response.data.venta.inventario
            this.form.usuario = response.data.venta.usuario
            this.form.tasa_cambio = response.data.venta.tasa_cambio
            this.form.moneda = response.data.venta.moneda
            this.form.precio_venta=response.data.venta.precio_venta

          })
          .catch(error => {
            console.log(error);
          });
        this.loading = false
      }, 500)
    },
   
    
    deleteData(value) {
      this.$q.dialog({
        title: 'Confirmación',
        message: '¿Desea eliminar el gasto?',
        cancel: true,
        persistent: true
      }).onOk(() => {
        // console.log('>>>> OK')    
        // api.gasto.delete_by_id(value).then(response => {
        // console.log(response)
        //this.loadGastos()
        var form = new this.$Form({
          id: value.id,
          inventario: {
            id: value.inventario.id
          }
        })

        try {
          form.submit(DELETE, { gasto: form.data() }).then(
            response => {

              console.log("OK");
              this.form.gastos = []
              console.log(response.data);
              this.loadGastos()


            },
            error => {
              console.log(error);

            }
          )
        } catch (error) {
          console.log(error)

        }

      }).onCancel(() => {
        // console.log('>>>> Cancel')
      }).onDismiss(() => {
        // console.log('I am triggered on both OK and Cancel')
      })

    },
    returnToinventario() {

      this.$router.push({ name: 'inventarios' });

    }

  }
}