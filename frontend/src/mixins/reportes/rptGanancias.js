import api from '../../api'
export default {
  data() {
    return {
      ventas: [],
      page: 1,
      search_ventas: "",
      startDate: undefined,
      endDate: undefined,
      loading: false,
      columns: [

        { label: "Factura", field: row => row.codigofactura ? row.codigofactura : "No Disponible", width: "50px", sort: true},
        { label: "Cliente", field: row => row.cliente ? row.cliente : "No Disponible", width: "50px", sort: true},
        { label: "Cedula", field: row => row.cedula ? row.cedula : "No Disponible", width: "50px", sort: true},
        { label: "Vehiculo", field: row => row.inventario ? row.inventario.vehiculo?.modelo?.descripcion : "No Disponible", width: "50px", sort: true },
        { label: "Monto Venta", field: row => row.monto ? row.monto : 0, width: "50px", sort: true, name: "montoventa", format: (val, row) => `${this.$MyUtils.formatCurrency(row.monto)}` },
        { label: "Total Gastos", field: row => row.inventario ? row.inventario?.total_gastos : 0, width: "50px", sort: true, format: (val, row) => `${this.$MyUtils.formatCurrency(val)}`},
        { label: "Ganancia", name: 'ganancia', field: row => row.inventario ? row.inventario?.total_gastos : 0, width: "50px", sort: true},
        { label: "Moneda", field: row => row.moneda ? row.moneda.descripcion : "No Disponible", width: "50px", sort: true},
        { label: "Fecha", field: row => row.createdat ? row.createdat : "No Disponible", format: (val, row) => `${this.$MyUtils.formatDate(val)}`, width: "50px", sort: true},
        /*   { label: "Usuario Venta", field: row => row.usuario ? row.usuario.nombreusuario : "No Disponible", width: "50px", sort: true, headerClasses: 'bg-blue-grey-8 text-white' }, */

      ],
      configs: {
        columnPicker: true,
        title: "Ventas"
      }
    };
  },
  mounted() {
    this.loadData();
  },
  //watch: {
  //page() {
  //this.loadData();
  //}
  //},

  methods: {
    getGanancias(val) {
      return val.monto * val.tasa_cambio.valor -
        val.inventario.total_gastos
    },
    async loadData() {
      this.loading = true
      // console.log('111111'+this.$apollo);
      this.startDate = this.startDate ? this.$MyUtils.convertDate(this.startDate) : this.startDate
      this.endDate = this.endDate ? this.$MyUtils.convertDate(this.endDate) : this.endDate

       setTimeout(async() => {
       await api.venta.findByParamReport(this.search_ventas, new Date(this.startDate), new Date(this.endDate))
          .then(response => {
            console.log(response);
            //conso le.log(response.statusText);
            // console.log(response.data.data.empresas);
            // this.loading = true;
            if (response)
              this.ventas = response.data.ventas;
            // this.loading = false;
            // console.log(this.empresas);
          })
          .catch(error => {
            console.log(error);
            // this.loading = false;
          });

        this.loading = false
      }, 500)
    },
 
    async clearData() {
      this.startDate = undefined
      this.endDate = undefined
      this.search_ventas = ""
     await this.loadData()
    },
    
  }
}