export default {
    data() {
        return {
            saving: false,
        }
    },
    methods: {
        submitForm(formvar, refsval = []) {

            this.saving = true;
            var submitQuery;
           
            //console.log(this.form.data);
            // console.log(this.form.id);

            try {
                this.form = new this.$Form(this.$omitDeep(this.form, "__typename"))
                delete this.form.vehiculo?.modelo?.marca?.modelos
                delete this.form.inventario?.total_gastos
                var variables = { [formvar]: this.form.data() }
                submitQuery = (this.form.id == 0 || this.form.id == undefined) ? this.insert : this.update
                //check for valitaions
                for (const validator of refsval) {
                    this.$refs[validator].validate === undefined ? this.$refs[validator].$children[0].validate() : this.$refs[validator].validate()
                    if (this.$refs[validator].hasError === undefined ? this.$refs[validator].$children[0].hasError : this.$refs[validator].hasError) {
                        this.$q.notify(this.$i18n.t("review_not"));
                        this.saving = false;
                        return;
                    }
                }


                this.form.submit(submitQuery, variables).then(
                    response => {
                        if (response.statusText == "OK") {
                            console.log("OK");
                            console.log(response.data);
                            return;
                        } else {
                            console.log(response);
                            return;
                        }
                    },
                    error => {

                        console.log(error);
                    }
                );
            } catch (error) {
                console.log(error)
            }
            this.saving = false;

        }

    }
}