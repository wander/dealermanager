import { INSERT, UPDATE, UPDATE_PERFIL } from "../../api/security/usuario/mutations";
import api from '../../api'
import form_mixin from '../formmixin'

export default {
  mixins: [form_mixin],
  mounted() {
    //console.log(this.form);
    if (this.$Auth.getUser().id !== undefined)
      this.form.id = parseInt(this.$Auth.getUser().id);
    if (this.form.id != 0) this.loadData();
  },
  data() {
    return {

      form: new this.$Form({

        id: 0,
        nombreusuario: "",
        email: "",
        nombre: "",
        activo: false,
        clave: "",

      }),
      repeatclave: "",
      newclave: "",
      actualclave: "",
      actualusername: "",
      sucursales: [],
      roles: [],
      insert: INSERT,
      update: (this.$Auth.isAdmin() || this.$Auth.isSuperAdmin()) ? UPDATE : UPDATE_PERFIL,
      loading: false


    };
  },
  computed: {
    compareNewPass() {
      return this.repeatclave === this.newclave
    },
    cp_error_message() {
      return this.$i18n.t("cp_error_message")
    }

  },


  methods: {
   async loadData() {
      this.loading = true
      setTimeout(async () => {


        const find_by_id = api.usuario.findById(this.form.data().id)
        //api.dealer.findByParam(this.search_dealers) 
       await find_by_id.then(response => {
          console.log(response);
          this.form.nombreusuario = response.data.usuario.nombreusuario
          //consigo el usuario actual para evitar la validacion isUnique
          this.actualusername = response.data.usuario.nombreusuario
          // this.form.clave = response.data.usuario.clave
          this.actualclave = this.form.data().clave

        })
          .catch(error => {
            console.log(error);

          });
        this.loading = false
      }, 500)
    },

    isUnique(value) {
      if (value === "" || value === this.actualusername) return true;

      console.log(value);
      console.log(this.actualusername)

      return new Promise((resolve, reject) => {

        setTimeout(() => {
          api.usuario
            .val_username(

              this.form.data().nombreusuario

            )
            .then(response => {
              var validateUserName = response.data.validateUserName


              resolve(validateUserName === false || "El nombre de usuario ya esta registrado");
            }, 1000).catch(error => {
              console.log(error);
              reject(false);
            });
        })

      })
      // })
    },
    validatePassword(value) {
      if (value === "") return true;
      console.log(value);

      return new Promise((resolve, reject) => {

        setTimeout(() => {
          api.usuario
            .val_clave(

              this.form.data().clave
            )
            .then(response => {
              var validateClave = response.data.validateClave


              resolve(validateClave === true || "Usuario o Contraseña Invalida");
            }, 1000).catch(error => {
              console.log(error);
              reject(false);
            });
        })

      })

    },




  }
}