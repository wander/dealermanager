import { INSERT, UPDATE, UPDATE_PERFIL,UPDATE_PASS } from "../../api/security/usuario/mutations";
import api from '../../api'
import form_mixin from '../formmixin'
import {Notify} from 'quasar'
export default {
  mixins: [form_mixin],
  mounted() {
    //console.log(this.form);
    this.form.id = this.$route.params.id === undefined ?
      this.$Auth.getUser().id === undefined ?
        0 : parseInt(this.$Auth.getUser().id)
      : parseInt(this.$route.params.id)

    if (this.form.id != 0) this.loadData();
  },
  data() {
    return {

      form: new this.$Form({

        id: 0,
        nombreusuario: "",
        email: "",
        nombre: "",
        activo: false,
        clave: "",
        roles: [],
        rolesToAdd: [],
        rolesToRemove: [],
        sucursales: [],        
        sucursalesToAdd: [],
        sucursalesToRemove: []


      }),
      repeatclave: "",
      newclave: "",
      actualclave: "",
      actualusername: "",
      sucursales: [],
      roles: [],
      insert: INSERT,
      update: (this.$Auth.isAdmin() || this.$Auth.isSuperAdmin()) ? UPDATE : UPDATE_PERFIL,
      loading: false


    };
  },
  computed: {
    compareNewPass() {
      return this.repeatclave === this.newclave
    },
    cp_error_message() {
      return this.$i18n.t("cp_error_message")
    }

  },


  methods: {
    async loadData() {
      this.loading = true
      setTimeout(async () => {


        const find_by_id = api.usuario.findById(this.form.data().id)
        //api.dealer.findByParam(this.search_dealers) 
       await find_by_id.then(response => {
          console.log(response);
          const userdata = response.data.usuario
          //consigo el usuario actual para evitar la validacion isUnique
          this.actualusername = userdata.nombreusuario
          // this.form.clave = response.data.usuario.clave
          this.actualclave = this.form.data().clave
          if (this.$Auth.isAdmin() || this.$Auth.isSuperAdmin()) {
            this.form.nombreusuario = userdata?.nombreusuario
            this.form.email=userdata?.email
            this.form.nombre = userdata?.nombre
            this.form.activo = userdata?.activo
            this.form.roles = userdata?.roles
            this.form.sucursales = userdata?.sucursales


          }
          else {
            this.form = new this.$Form({
              nombreusuario: userdata?.nombreusuario,
              clave:"",

            })

          }



        })
          .catch(error => {
            console.log(error);

          });
        this.loading = false
      }, 500)
    },

    isUnique(value) {
      if (value === "" || value === this.actualusername) return true;

      console.log(value);
      console.log(this.actualusername)

      return new Promise((resolve, reject) => {

        setTimeout(() => {
          api.usuario
            .val_username(

              this.form.data().nombreusuario

            )
            .then(response => {
              var validateUserName = response.data.validateUserName


              resolve(validateUserName === false || "El nombre de usuario ya esta registrado");
            }, 1000).catch(error => {
              console.log(error);
              reject(false);
            });
        })

      })
      // })
    },
    validatePassword(value) {
      if (value === "") return true;
      console.log(value);

      return new Promise((resolve, reject) => {

        setTimeout(() => {
          api.usuario
            .val_clave(

              this.form.data().clave
            )
            .then(response => {
              var validateClave = response.data.validateClave


              resolve(validateClave === true || "Usuario o Contraseña Invalida");
            }, 1000).catch(error => {
              console.log(error);
              reject(false);
            });
        })

      })

    },
    validatePass(){
      return this.form.id===0 ? this.form.clave ? true:false : true
    },
    updatePass() {
      this.$q.dialog({
        title: 'Confirmación',
        message: '¿Desea cambiar la contraseña?',
        cancel: true,
        persistent: true
      }).onOk(() => {
        // console.log('>>>> OK')    
        // api.gasto.delete_by_id(value).then(response => {
        // console.log(response)
        //this.loadGastos()
        this.form = new this.$Form(this.$omitDeep(this.form, "__typename"))

        try {
          this.form.submit(UPDATE_PASS, { usuario: this.form.data() }).then(
            response => {

              console.log("OK");
              Notify.create({
                message: this.$i18n.t('success'),
                color: "positive",
                icon: "fa fa-info",
                /*  actions: [
                   {
                     label: "Regresar",
                     color:"white",
                     handler: () => {
                       //router.go(-1);
                       window.history.go(-1);
                       console.log("dismissed");
                     }
                   }
                 ] */
              });
            },
            error => {
              console.log(error);
              Notify.create({
                message: this.$i18n.t('failed'),
                type: "negative",
          
              });

            }
          )
        } catch (error) {
          console.log(error)

        }

      }).onCancel(() => {
        // console.log('>>>> Cancel')
      }).onDismiss(() => {
        // console.log('I am triggered on both OK and Cancel')
      })

    }




  }
}