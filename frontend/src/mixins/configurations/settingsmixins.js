import api from '../../api'

export default {
  data() {
    return {
      users: [],
      page: 1,
      search_users: undefined,
      loading: false,
      columns: [
        { label: "E-mail",name:'email', field: row=>row.email ? row.email:"N/A", width: "80px", sort: true },
        { label: "Nombre",name:'nombre', field: row=>row.nombre ? row.nombre:"N/A", width: "50px", sort: true },
        { label: "Nombre Usuario",name:'nombreusuario', field: row=>row.nombreusuario ? row.nombreusuario:"N/A", width: "50px", sort: true },
        { label: "Roles",name:'roles', field: (row)=>row.roles, width: "150px", sort: true},
        { label: "Sucursales",name:'sucursales', field: row=>row.sucursales , width: "150px", sort: true },
        { label: "Activo",name:'activo', field: row=>row.activo ? row.activo:true, width: "150px", sort: true },
        { label: "Fecha Creación", name:'createdat',field: row=>row.createdat ? row.createdat:"N/A", width: "150px", sort: true },
        { label: "Ultima Actividad",name:'lastactivity' ,field: row=>row.lastactivity ? row.lastactivity:"N/A", width: "150px", sort: true },
        {
          name: "actions",
          label: "Acciones",
          field: "image_url",
          width: "100px",
       
        }
      ],
      configs: {
        columnPicker: true,
        title: "Listado de Usuarios"
      }
    };
  },
  mounted() {
    this.loadData();
  },
  watch: {
    page() {
      this.loadData();
    }
  },

  methods: {
   async loadData() {
      // console.log('111111'+this.$apollo);
      this.loading = true
      // console.log('111111'+this.$apollo);
      setTimeout(async () => {
        if (this.search_users === '') this.search_users = undefined

        const methodcall =  api.usuario.findByParam(this.search_users, true) 
        //api.dealer.findByParam(this.search_users)
        await methodcall.then(response => {
          console.log(response);
          //console.log(response.statusText);
          // console.log(response.data.data.empresas);

          if (response)
            this.users = response.data.usuarios

          // console.log(this.empresas);
        })
          .catch(error => {
            console.log(error);

          });
        this.loading = false
      }, 500)
    },
  }
}