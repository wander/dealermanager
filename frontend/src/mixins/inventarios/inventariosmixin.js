import api from '../../api'

export default {
  data() {
    return {
      inventarios: [],
      page: 1,
      search_inventarios: undefined,
      loading: false,
      columns: [
        /*           { label: "Descripción", field: row=>row.descripcion ? row.descripcion : "No Disponible" , width: "80px", sort: true },
                  { label: "Estado", field: row=>row.status_inventario ? row.status_inventario.descripcion : "No Disponible" , width: "80px", sort: true },
                  { label: "Código Subasta", field: row=>row.codigosubasta ? row.codigosubasta : "No Disponible" , width: "80px", sort: true }, */
        { label: "VIN", field: row => row.vin ? row.vin : "No Disponible", width: "80px", sort: true },
        { label: "Precio de Venta", field: 'precio_venta', format: (val, row) => `${this.$MyUtils.formatCurrency(val)}`,width: "80px", sort: true},
        { label: "Año", field: row => row.year ? row.year : "No Disponible", width: "80px", sort: true },
        { label: "Color",name:'color', field: row => row.color.descripcion ? row.color.descripcion : "No Disponible", width: "80px", sort: true },
        { label: "Marca", field: row => row.vehiculo.modelo.marca ? row.vehiculo.modelo.marca.descripcion : "No Disponible", width: "80px", sort: true },
        { label: "Modelo", field: row => row.vehiculo.modelo ? row.vehiculo.modelo.descripcion : "No Disponible", width: "80px", sort: true },
        { label: "Combustible", field: row => row.vehiculo.tipo_combustible ? row.vehiculo.tipo_combustible.descripcion : "No Disponible", width: "80px", sort: true },
        { label: "Motor", field: row => row.vehiculo.tipo_motor ? row.vehiculo.tipo_motor.descripcion : "No Disponible", width: "80px", sort: true },
        { label: "Cilindro", field: row => row.vehiculo.cilindro ? row.vehiculo.cilindro.descripcion : "No Disponible", width: "80px", sort: true },
        { label: "Transmisión", field: row => row.vehiculo.tipo_combustible ? row.vehiculo.tipo_transmision.descripcion : "No Disponible", width: "80px", sort: true },
        { label: "Tipo Vehículo", field: row => row.vehiculo.tipo_vehiculo ? row.vehiculo.tipo_vehiculo.descripcion : "No Disponible", width: "80px", sort: true},
        {
          name: "actions",
          label: "Acciones",
          field: "image_url",
          width: "100px",
        
        }
      ],
      configs: {
        columnPicker: true,
        title: "Listado de Vehiculos en Inventario"
      }
    };
  },
  mounted() {
    this.loadData();
  },
  watch: {
    page() {
      this.loadData();
    }
  },

  methods: {
    async loadData() {
      // console.log('111111'+this.$apollo);
      this.loading = true;
      setTimeout(async () => {
        if (this.search_inventarios == '') this.search_inventarios = undefined
        const methodcall = this.$Auth.isSuperAdmin() ? api.inventario.findByParam(this.search_inventarios, true) : api.inventario.findByParamAndDealer(this.search_inventarios, true)
        //api.inventario.findByParam(this.search_inventarios, true)

       await methodcall.then(response => {
          console.log(response);
          //console.log(response.statusText);
          // console.log(response.data.data.empresas);
          // this.loading = true;
          if (response)
            this.inventarios = response.data.inventarios ? response.data.inventarios : response.data.inventariosByDealer;
          // this.loading = false;
          // console.log(this.empresas);
        })
          .catch(error => {
            console.log(error);
            // this.loading = false;
          });
        this.loading = false;
      }, 500)
    }
  }
}