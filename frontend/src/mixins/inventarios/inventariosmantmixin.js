import { INSERT, UPDATE } from "../../api/inventario/mutations";
import { DELETE } from '../../api/gasto/mutations';
import api from "../../api";
import form_mixin from '../formmixin'

export default {
  mixins: [form_mixin],
  mounted() {
    //console.log(this.form);

    if (this.$route.params.id !== undefined)
      this.form.id = parseInt(this.$route.params.id);
    if (this.form.id != 0) this.loadinventario();
  },
  data() {
    return {
      loading: false,
      expanded: false,
      showcard: false,
      showinsert: true,
      insert: INSERT,
      update: UPDATE,
      searchgastos: "",
      form: new this.$Form({
        id: 0,
        descripcion: "",
        codigosubasta: "",
        vin: "",
        year: "",
        status_inventario: null,
        precio_venta:0,
        vehiculo: null,
        sucursal: this.$Auth.getSucursales() ?this.$Auth.getSucursales()[0] :null,
        color: null,
        gastos: [],
        gastosToAdd: [],
        gastosToRemove: []

      }),
      params: {
        id: 0,
        inventarioid: 0,
      },
      columns: [


        { label: "Tipo Gasto", field: row => row.tipo_gasto ? row.tipo_gasto.descripcion : "No Disponible", width: "50px", sort: true },
        { label: "Descripción", field: "descripcion", width: "150px", sort: true },
        { label: "Monto", field: "monto", width: "150px", sort: true, format: (val, row) => `${this.formatCurrency(val)}`},
        { label: "Moneda", field: row => row.moneda ? row.moneda.descripcion : "No Disponible", width: "150px", sort: true},
        {
          label: "Tasa Cambio", field: row => row.tasa_cambio ? `1 ${row.tasa_cambio.moneda_predeterminada.descripcion} = ${row.tasa_cambio.valor}  ${row.tasa_cambio.moneda_alterna.descripcion}`
            : "No Disponible", width: "150px", sort: true},

        { label: "Ubicación", field: row => row.ubicacion ? row.ubicacion.descripcion : "No Disponible", width: "150px", sort: true},
        {
          name: "actions",
          label: "Acciones",
          field: "image_url",
          width: "100px"
        }
      ],
      configs: {
        columnPicker: true,
        title: "Gastos o Inversión"
      }
      ,
      insert: INSERT,
      update: UPDATE

    };
  },
  computed: {
    montoTotal() {

      const total = this.form.gastos ? this.form.gastos.filter(({ moneda }) => moneda.predeterminada === false)
        .reduce((val, curr) => val + curr.monto, 0) : 0
      return total
      //`$ ${this.formatCurrency(total)}`  
    },
    montoTotalPredeterminado() {

      const total = this.form.gastos ? this.form.gastos.filter(({ moneda }) => moneda.predeterminada === true)
        .reduce((val, curr) => val + curr.monto, 0) : 0
      return total
      //`$ ${this.formatCurrency(total)}`  
    },
    montoTotalGeneral() {
      const total = this.form.gastos ? this.form.gastos.filter(({ moneda }) => moneda.predeterminada === false)
        .reduce((val, curr) => val + (curr.monto * curr.tasa_cambio.valor), 0) + this.montoTotalPredeterminado : 0

      return total
      //`$ ${this.formatCurrency(total)}`  
    }

  },
  methods: {
    handleSubmit() {
       delete this.form.vehiculo?.modelo?.marca;
      this.submitForm('inventario')
    },
    async loadinventario() {
      this.loading = true
      setTimeout(async () => {
        await api.inventario
        .findById(this.form.data().id, true)
        .then(response => {
          console.log(response)
          // this.form.id = response.data.inventario.id
          this.form.descripcion = response.data.inventario.descripcion
          this.form.codigosubasta = response.data.inventario.codigosubasta
          this.form.vin = response.data.inventario.vin
          this.form.year = response.data.inventario.year
          this.form.status_inventario = response.data.inventario.status_inventario
          this.form.vehiculo = response.data.inventario.vehiculo
          this.form.sucursal = response.data.inventario.sucursal? response.data.inventario.sucursal :this.$Auth.getSucursales() ?this.$Auth.getSucursales()[0] :null
          this.form.color = response.data.inventario.color
          //this.form.gastos=response.data.gastos
           this.loadGastos()


        })
        .catch(error => {
          console.log(error);
        });
        this.loading = false
      }, 500)
    },
    formatCurrency(val) {
      return `$ ${parseFloat(val).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,').toString()}`;
    },
    async loadGastos() {

    await  api.gasto.get_by_param(
        this.searchgastos,
        parseInt(this.form.data().id)

      )
        .then(response => {
          this.form.gastos = response.data.gastos;
        })
        .catch(error => {
          console.log(error);
        });
    },
    insertData() {
      this.$router.push({ name: "mantgastos", params: { inventarioid: this.form.id } });
    },

    editData(value) {
      this.$router.push({ name: "mantgastos", params: { id: value, inventarioid: this.form.id } });
    },
    async deleteData(value) {
      this.$q.dialog({
        title: 'Confirmación',
        message: '¿Desea eliminar el gasto?',
        cancel: true,
        persistent: true
      }).onOk(() => {
        // console.log('>>>> OK')    
        // api.gasto.delete_by_id(value).then(response => {
        // console.log(response)
        //this.loadGastos()
        var form = new this.$Form({
          id: value.id,
          inventario: {
            id: value.inventario.id
          }
        })

        try {
           form.submit(DELETE, { gasto: form.data() }).then(
            response => {

              console.log("OK");
              this.form.gastos = []
              console.log(response.data);
              this.loadGastos()


            },
            error => {
              console.log(error);

            }
          )
        } catch (error) {
          console.log(error)

        }

      }).onCancel(() => {
        // console.log('>>>> Cancel')
      }).onDismiss(() => {
        // console.log('I am triggered on both OK and Cancel')
      })

    },
    returnToinventario() {

      this.$router.push({ name: 'inventarios' });

    }

  }
}