import { LOGIN, SIGNUP } from "../../api/security/usuario/mutations";
import form_login_mixin from '../formloginmixin'
export default {
  mixins: [form_login_mixin],
  mounted() {
    //console.log(this.form);

  },
  data() {
    return {

      form: new this.$FormLogin({

        id: 0,
        nombreusuario: "",
        clave: "",
        sucursales: null,
        roles: null

      }),
      login: LOGIN,
      signup: SIGNUP,
      loginuser: true

    };
  },
  computed: {


  },


  methods: {

    logout() {
      this.$Auth.logout()

    }

  }
}