import { INSERT, UPDATE } from "../../api/sucursales/mutations";
import api from "../../api";
import form_mixin from '../formmixin'
export default {
  mixins: [form_mixin],
  mounted() {
    //console.log(this.form);

    if (this.$route.params.id !== undefined) this.form.id = this.$route.params.id;
    if (this.$route.params.dealerid !== undefined) this.form.dealer = { id: this.$route.params.dealerid };

    if (this.form.id != 0) this.loadSucursal();
  },
  data() {
    return {
      loading: false,
      form: new this.$Form({

        descripcion: "",
        direccion: "",
        telefonos: "",
        dealer: {
          id: 0
        },
        id: 0
      }),
      empresa: {},
      insert: INSERT,
      update: UPDATE

    };
  },

  methods: {
    isUnique(value) {
      if (value === "") return true;
      console.log(value);

      return new Promise((resolve, reject) => {

        setTimeout(() => {
          api.sucursal
            .val_exists(

              value, 'descripcion'

            )
            .then(response => {
              console.log(response.data)
              var data = response.data.sucursalValExists.filter(
                item => item.id != this.form.id
              ).length;

              resolve(data <= 0 || "Esta sucursal ya esta registrada");
            }, 1000).catch(error => {
              console.log(error);
              reject(false);
            });
        })

      })
      // })
    },
   async  loadSucursal() {
      this.loading = true
      setTimeout(async () => {
        await api.sucursal
          .get_by_id(this.form.data().id, true)
          .then(response => {
            console.log(response)
            this.form.descripcion = response.data.sucursal.descripcion;
            this.form.direccion = response.data.sucursal.direccion;
            this.form.telefonos = response.data.sucursal.telefonos;

          })
          .catch(error => {
            console.log(error);
          });
        this.loading = false
      }, 500)
    },

 
  }
}