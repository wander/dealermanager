import api from '../../api'

export default {
  data() {
    return {
      vehiculos: [],
      page: 1,
      search_vehiculos: "",
      loading: false,
      columns: [
      // headerClasses: 'bg-blue-grey-8 text-white' 
        { label: "Marca", field: row => row.modelo.marca ? row.modelo.marca.descripcion : "No Disponible", width: "80px", sort: true },
        { label: "Modelo", field: row => row.modelo ? row.modelo.descripcion : "No Disponible", width: "80px", sort: true },
        { label: "Combustible", field: row => row.tipo_combustible ? row.tipo_combustible.descripcion : "No Disponible", width: "80px", sort: true},
        { label: "Motor", field: row => row.tipo_motor ? row.tipo_motor.descripcion : "No Disponible", width: "80px", sort: true},
        { label: "Cilindro", field: row => row.cilindro ? row.cilindro.descripcion : "No Disponible", width: "80px", sort: true},
        { label: "Transmisión", field: row => row.tipo_combustible ? row.tipo_transmision.descripcion : "No Disponible", width: "80px", sort: true},
        { label: "Tipo Vehículo", field: row => row.tipo_vehiculo ? row.tipo_vehiculo.descripcion : "No Disponible", width: "80px", sort: true },
        {
          name: "actions",
          label: "Acciones",
          field: "image_url",
          width: "100px",
      
        }
      ],
      configs: {
        columnPicker: true,
        title: "Listado de Vehiculos"
      }
    };
  },
  mounted() {
    this.loadData();
  },
  watch: {
    page() {
      this.loadData();
    }
  },

  methods: {
   async loadData() {
      this.loading = true
      // console.log('111111'+this.$apollo);
      setTimeout(async () => {
       await api.vehiculo.findByParam(this.search_vehiculos, true)
          .then(response => {
            console.log(response);
            //conso le.log(response.statusText);
            // console.log(response.data.data.empresas);
            // this.loading = true;
            if (response)
              this.vehiculos = response.data.vehiculos;
            // this.loading = false;
            // console.log(this.empresas);
          })
          .catch(error => {
            console.log(error);
            // this.loading = false;
          });

        this.loading = false
      }, 500)
    }
  }
}