import { INSERT, UPDATE } from "../../api/vehiculos/mutations";
import api from "../../api";
import form_mixin from '../formmixin'
export default {
  mixins: [form_mixin],
  mounted() {
    //console.log(this.form);

    if (this.$route.params.id !== undefined) this.form.id = parseInt(this.$route.params.id);
    if (this.form.id != 0) this.loadVehiculo();
  },
  data() {
    return {
      loading: false,
      form: new this.$Form({
        id: 0,
        modelo: null,
        tipo_combustible: null,
        tipo_motor: null,
        cilindro: null,
        tipo_transmision: null,
        tipo_vehiculo: null,
        tipo_train: null,

      }),
      modelos: [],
      modelosunfiltered: [],
      marcas: [],
      marca: null,
      empresa: {},
      insert: INSERT,
      update: UPDATE

    };
  },

  methods: {
    handleSubmit() {
      delete this.form.modelo.marca;
      this.submitForm('vehiculo')
    },
    async loadVehiculo() {
      this.loading = true
      setTimeout(async () => {
       await api.vehiculo
          .findById(this.form.data().id, true)
          .then(response => {
            console.log(response)
            this.form.id = response.data.vehiculo.id;
            this.form.modelo = response.data.vehiculo.modelo;
            this.marca = response.data.vehiculo.modelo.marca;
            this.modelos = this.marca.modelos;
            this.modelosunfiltered = this.marca.modelos;
            this.form.tipo_combustible = response.data.vehiculo.tipo_combustible
            this.form.tipo_motor = response.data.vehiculo.tipo_motor
            this.form.cilindro = response.data.vehiculo.cilindro
            this.form.tipo_transmision = response.data.vehiculo.tipo_transmision
            this.form.tipo_vehiculo = response.data.vehiculo.tipo_vehiculo
            this.form.tipo_train = response.data.vehiculo.tipo_train
          }).then(() => {
            api.marca.findByParam(undefined)
              .then(response => {
                console.log(response)
                this.marcas = response.data.marcas
              }).catch(error => {
                console.log(error);
              });
          })
          .catch(error => {
            console.log(error);
          });
        this.loading = false
      }, 500)
    },
    handleMarcaChanged(marca) {

      this.marca = marca
      this.form.modelo.marca = marca
      this.modelos = marca.modelos
      this.modelosunfiltered = marca.modelos


    },

    returnToVehiculo() {

      this.$router.push({ name: 'vehiculos' });

    }

  }
}