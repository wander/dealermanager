
export default {
    data() {
        return {
            saving: false,
        }
    },
    methods: {
        async submitFormSecurity(formvar, refsval = [], to) {

            this.saving = true;
            var submitQuery;
            //console.log(this.form.data);
            // console.log(this.form.id);

            try {
                this.form = new this.$FormLogin(this.$omitDeep(this.form, "__typename"))
                var variables = { [formvar]: this.form.data() }
                submitQuery = this.loginuser ? this.login : this.signup
                //check for valitaions
                for (const validator of refsval) {
                    this.$refs[validator].validate()
                    if (this.$refs[validator].hasError) {
                        this.$q.notify(this.$i18n.t("review_not"));
                        this.saving = false;
                        return;
                    }
                }


             await this.form.submit(submitQuery, variables).then(
                    response => {
                        //if (response.statusText == "OK") {
                        //   console.log("OK");
                        console.log(response.login);
                        const login_data = response.login
                        this.$Auth.setData(login_data.usuario, login_data.token)
                        this.$router.replace(this.$route.query.returnUrl || '/');
                        return
                        //  return;
                        // } else {
                        // console.log(response);
                        //return;
                        //}
                    },
                    error => {
                        console.log(error);
                    }
                );
            } catch (error) {
                console.log(error)
            }
            this.saving = false;

        }

    }
}