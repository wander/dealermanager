import { INSERT, UPDATE } from "../../api/gasto/mutations";
import api from "../../api";
import form_mixin from '../formmixin'
import { VMoney } from 'v-money'
export default {
  mixins: [form_mixin],
  mounted() {
    //console.log(this.form);

    if (this.$route.params.id !== undefined) this.form.id = parseInt(this.$route.params.id);
    if (this.$route.params.inventarioid !== undefined) this.form.inventario = { id: parseInt(this.$route.params.inventarioid) };

    if (this.form.id != 0) this.loadgasto();
  },
  directives: { money: VMoney },
  data() {
    return {
      loading: false,
      moneyFormatForDirective: {
        decimal: '.',
        thousands: ',',
        prefix: '$ ',
        precision: 2,
        masked: false /* doesn't work with directive */
      },
      form: new this.$Form({

        descripcion: "",
        monto: 0,
        moneda: null,
        tasa_cambio: null,
        tipo_gasto: null,
        ubicacion: null,
        inventario: {
          id: 0
        },
        id: 0
      }),
      inventario: {},
      insert: INSERT,
      update: UPDATE

    };
  },
  computed: {
    montoTasaCambio() {

      if (this.form.data().tasa_cambio)
        return this.form.data().monto * this.form.data().tasa_cambio.valor

      return this.form.data().monto
    },


    labelConverted() {
      if (this.form.data().tasa_cambio)
        return `Monto en ${this.form.data().tasa_cambio.moneda_predeterminada.descripcion} (${this.form.data().tasa_cambio.moneda_predeterminada.nomenclatura})`
      return 'Monto $'
    },
    showTasa() {
      if (this.form.data().moneda)
        return this.form.data().moneda.predeterminada ? false : true
      return true


    }

  },


  methods: {

    async loadgasto() {
      this.loading = true
       setTimeout(async () => {
        await api.gasto
        .get_by_id(this.form.data().id, true)
        .then(response => {
          console.log(response)
          this.form.descripcion = response.data.gasto.descripcion;
          this.form.monto = response.data.gasto.monto
          this.form.moneda = response.data.gasto.moneda
          this.form.tasa_cambio = response.data.gasto.tasa_cambio
          this.form.tipo_gasto = response.data.gasto.tipo_gasto
          this.form.ubicacion = response.data.gasto.ubicacion

        })
        .catch(error => {
          console.log(error);
        });
        this.loading = false
      }, 500)
    },

 /*    returnToInventario() {

      this.$router.push({ name: 'mantinventarios', params: { id: this.form.inventario.id } });

    } */

  }
}