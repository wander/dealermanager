import { INSERT, UPDATE } from "../../api/dealer/mutations";
//import { required } from "vuelidate/lib/validators";
// import tooltipButton from './tooltipButton.vue'
import api from "../../api"
import form_mixin from '../formmixin'
export default {
  mixins: [form_mixin],
  mounted() {
    if (this.$route.params.id !== undefined)
      this.form.id = parseInt(this.$route.params.id);
    if (this.form.id != 0) this.loadDealer();
  },
  data() {
    return {
      loading: false,
      expanded: false,
      searchSucursales: "",
      showcard: false,
      showinsert: true,
      insert: INSERT,
      update: UPDATE,

      form: new this.$Form({
        nombre: "",
        rnc: "",
        website: "",
        email: "",
        id: 0,
        sucursales: []
      }),

      sucursales: [],
      sucursalId: 0,
      params: {
        id: 0,
        dealerid: 0,
      },
      columns: [

        {
          label: "Descripción",
          field: "descripcion",
          width: "110px",
          sort: true,
          headerClasses: 'bg-blue-grey text-white'
        },
        { label: "Dirección", field: "direccion", width: "50px", sort: true, headerClasses: 'bg-blue-grey text-white' },
        { label: "Telefono", field: "telefonos", width: "150px", sort: true, headerClasses: 'bg-blue-grey text-white' },
        {
          name: "actions",
          label: "Acciones",
          field: "image_url",
          width: "100px",
          headerClasses: 'bg-blue-grey text-white'
        }
      ],
      configs: {
        columnPicker: true,
        title: "Listado de Sucursales"
      }
    };
  },
  /*   validations: {
      form: {
        nombre: { required },
        rnc: {
          required,
          isUnique(value) {
            if (value === "") return true;
            console.log(value);
  
            return new Promise((resolve, reject) => {
              api.dealer
                .val_exists(
  
                  value, 'rnc'
  
                )
                .then(response => {
                  console.log(response.data)
                  var data = response.data.dealerValExists.filter(
                    item => item.id != this.form.id
                  ).length;
  
                   setTimeout(() => {
                    resolve(data <= 0);
                  }, 500);
                })
                .catch(error => {
                  console.log(error);
                  reject(false);
                });
            })
            // })
          }
        }
      }
    }, */
  //METODOS USADOS EN LA PAGINA
  methods: {

    isUnique(value) {
      if (value === "") return true;
      console.log(value);

      return new Promise((resolve, reject) => {

        setTimeout(() => {
          api.dealer
            .val_exists(

              value, 'rnc'

            )
            .then(response => {
              console.log(response.data)
              var data = response.data.dealerValExists.filter(
                item => item.id != this.form.id
              ).length;

              resolve(data <= 0 || "Este RNC ya esta registrado");
            }, 1000).catch(error => {
              console.log(error);
              reject(false);
            });
        })

      })
      // })
    },

    async loadDealer() {
      // console.log('111111'+this.$apollo);
      this.loading = true
      // console.log('111111'+this.$apollo);
      setTimeout(async () => {
       await api.dealer.findById(this.form.data().id)
          .then(response => {
            console.log(response);
            this.form.nombre = response.data.dealer.nombre;
            this.form.rnc = response.data.dealer.rnc;
            this.form.website = response.data.dealer.website;
            this.form.email = response.data.dealer.email;
            this.form.sucursales = response.data.dealer.sucursales;
            //this.form.id = response.data.empresa.id;     


            //this.sucursales = this.form.sucursales;
            // this.loadSucursales();
            console.log(response.data.dealer);
          })
          .catch(error => {
            console.log(error);
          });
        this.loading = false
      }, 500)
    },
  
    loadSucursales() {
      api.sucursal.get_by_param(
        this.searchSucursales,
        parseInt(this.form.data().id)

      )
        .then(response => {
          this.form.sucursales = response.data.sucursales;
        })
        .catch(error => {
          console.log(error);
        });
    },
 
    disableData(data) {
      this.$q
        .dialog({
          title: "Confirmación",
          message: this.$i18n.t("disable_text_confirmation", {
            attribute: data.nombre,
          }),
          ok: "Aceptar",
          cancel: "Cancelar",
        })
        .then(() => {
          this.$q.notify(
            this.$i18n.t("success_disable_text", { attribute: data.nombre })
          );
        })
        .catch(() => {
          this.$q.notify("Disagreed...");
        });
    },

    disableSucursal(data) {
      this.$q
        .dialog({
          title: "Confirmación",
          message: `¿Desea desactivar el cliente ${data.nombre}?`,
          ok: "Aceptar",
          cancel: "Cancelar"
        })
        .then(() => {
          this.$q.notify(
            `El cliente ${data.nombre} fue desactivado satisfactoriamente!`
          );
        })
        .catch(() => {
          this.$q.notify("Disagreed...");
        });
    }
  }
};