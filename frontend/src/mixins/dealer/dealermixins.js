import api from '../../api'

export default {
  data() {
    return {
      dealers: [],
      page: 1,
      search_dealers: undefined,
      loading: false,
      columns: [
        { label: "Nombre", field: "nombre", width: "80px", sort: true},
        { label: "RNC o Cedula", field: "rnc", width: "50px", sort: true },
        { label: "E-Mail", field: "email", width: "150px", sort: true },
        {
          name: "actions",
          label: "Acciones",
          field: "image_url",
          width: "100px",
        
        }
      ],
      configs: {
        columnPicker: true,
        title: "Listado de Dealers"
      }
    };
  },
  mounted() {
    this.loadData();
  },
  watch: {
    page() {
      this.loadData();
    }
  },

  methods: {
    async loadData() {
      // console.log('111111'+this.$apollo);
      this.loading = true
      // console.log('111111'+this.$apollo);
      setTimeout(async() => {
        if (this.search_dealers === '') this.search_dealers = undefined

        const methodcall = this.$Auth.isSuperAdmin() ? api.dealer.findByParam(this.search_dealers, true) : api.dealer.findByParamAndUser(this.search_dealers, true)
        //api.dealer.findByParam(this.search_dealers)
        await methodcall.then(response => {
          console.log(response);
          //console.log(response.statusText);
          // console.log(response.data.data.empresas);

          if (response)
            this.dealers = response.data.dealers ? response.data.dealers : response.data.dealersByUsuario;

          // console.log(this.empresas);
        })
          .catch(error => {
            console.log(error);

          });
        this.loading = false
      }, 500)
    }
  }
}