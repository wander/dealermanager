import Vue from 'vue'
import VueI18n from 'vue-i18n'
import locales from '../i18n'

Vue.use(VueI18n)

const i18n = new VueI18n({
  locale: 'en-us',
  fallbackLocale: 'en-us',
  messages: locales
})

export default i18n
