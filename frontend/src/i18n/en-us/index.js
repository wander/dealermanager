// This is just an example,
// so you can safely delete all default props below

export default {
  failed: 'Ha ocurrido un error, intentelo nuevamente o contacte al administrador',
  login_failed: 'Error al iniciar sesión, intentelo nuevamanete.',
  success: 'Operación realizada satisfactoriamente',
  login_success: 'Sesión iniciada satisfactoriamente',

  accept_btn: 'Aceptar',
  cancel_btn: 'Regresar',
  generate_barcode_btn: 'Generar',
  review_not: 'Revise todos los campos y corriga los errores.',
  disable_text_confirmation: '¿Desea desactivar {attribute}?',
  success_disable_text: '{attribute} fue desactivado satisfactoriamente!',
  product_not_saved: 'Debe guardar el producto primero antes de poder agregar inventario.',
  //compare password
  cp_error_message: 'Las claves no coinciden',
  val_messages: {
    required: 'El campo {attribute} es requerido.',
    email: 'El campo {attribute} no es una direccion valida de E-mail.',
    maxLength: 'El campo {attribute} excede la cantidad maxima permitida.',
    minLength: 'El campo {attribute} no tiene la cantidad de caracteres minima.',
    isUnique: 'El {attribute} ya está registrado.',

  }


}
