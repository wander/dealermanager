import { ApolloClient } from 'apollo-client'
import {ApolloLink} from 'apollo-link'
import { InMemoryCache } from 'apollo-cache-inmemory'
import VueApollo from 'vue-apollo'
import fetch from 'node-fetch'
import { createHttpLink } from 'apollo-link-http'
import { AUTH_TOKEN } from '../constants/settings'
import {LocalStorage} from 'quasar'
const httpLink =
createHttpLink({ uri: 'https://dealermanagerapi.azurewebsites.net/graphql', fetch: fetch })
//createHttpLink({ uri:'http://localhost:3000/graphql', fetch: fetch })

const authMiddleware = new ApolloLink((operation, forward) => {
  // add the authorization to the headers
  const token = LocalStorage.getItem(AUTH_TOKEN)
  operation.setContext({
    headers: {
      authorization: token ? `Bearer ${token}` : null
    }
  })

  return forward(operation)
})
// Create the apollo client
const apolloClient = new ApolloClient({
  link:authMiddleware.concat(httpLink),
  cache: new InMemoryCache(),
  connectToDevTools: true,
})


export const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
  errorHandler ({ graphQLErrors, networkError }) {
    if (graphQLErrors) {
      graphQLErrors.map(({ message, locations, path }) =>
        console.log(
          `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
        )
      )
    }
    if (networkError) {
      console.log(`[Network error]: ${networkError}`)
    }
  }
})

export default ({ app, Vue }) => {
  Vue.use(VueApollo)
  app.apolloProvider = apolloProvider
}