import VuelidateErrorExtractor, {templates} from 'vuelidate-error-extractor'
import FormGroup from '../components/FormGroup.vue'
import FormSummary from '../components/FormSummary.vue'
import i18n from 'src/i18n/i18n';
const messages = {
  required: i18n.t('val_messages.required'),
  email: i18n.t('val_messages.email'),
  maxLength: i18n.t('val_messages.maxLength'),
  minLength: i18n.t('val_messages.minLength'),
  isUnique:i18n.t('val_messages.isUnique')
}

export default ({app, router, Vue}) => {
  Vue.use(VuelidateErrorExtractor, {
    messages,
    attributes: {
      nombre: 'Nombre',
      email: 'Email',
      text: 'Text',
      username: 'Username',
      password: 'Password',
      firstName: 'First Name',
      rnc:'Rnc',
      razonsocial:'Razón Social'

    }
  })

  Vue.component('FormGroup', FormGroup)
  Vue.component('FormSummary', FormSummary)
  Vue.component('formWrapper', templates.FormWrapper)
}