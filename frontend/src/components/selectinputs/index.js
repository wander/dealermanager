import marcas_input from './marcasInput'
import cilindro_input from './cilindrosInput'
import modelo_input from './modeloInput'
import tipo_combustible_input from './tipocombustibleInput'
import tipo_motor_input from './tipomotorInput'
import tipo_train_input from './tipotrainInput'
import tipo_transmision_input from './tipotransmisionInput'
import tipo_vehiculo_input from './tipovehiculoInput'
import ubicacion_input from './ubicacioninput'
import status_inv_input from './statusinv'
import vehiculo_input from './vehiculoInput'
import tipo_gasto_input from './tipogastoInput'
import moneda_input  from './monedaInput'
import tasa_cambio_input from  './tasacambioInput'
import sucursal_user_input from './sucursaluser'
export default{
    marcas_input,
    cilindro_input,
    modelo_input,
    tipo_combustible_input,
    tipo_motor_input,
    tipo_train_input,
    tipo_transmision_input,
    tipo_vehiculo_input,
    ubicacion_input,
    status_inv_input,
    vehiculo_input,
    tipo_gasto_input,
    moneda_input,
    tasa_cambio_input,
     sucursal_user_input
}